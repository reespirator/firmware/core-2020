#include <core_cfg.h>
#include <core_pinout.h>
#include <DRE.h>

// --- DRE data structure declaration ---
t_dre dre;
t_diag diag;

// --- DRE flow initialization functions ---



// STATUS_LED flow acquisition
// (setup input disabled for LED type)
// STATUS_LED flow synthesis
// (output disabled for LED type)

  
// DO_STATUS_LED flow acquisition
// (setup input disabled for DO type)
// DO_STATUS_LED flow synthesis
void setup_DO_STATUS_LED_output(void){ 
pinMode(PORT_DO_STATUS_LED, OUTPUT); 
};

  
// dbgLedBlinkTimer flow acquisition
// (setup input disabled for Variable type)
// dbgLedBlinkTimer flow synthesis
// (output disabled for Variable type)

  
// DBG_LED flow acquisition
// (setup input disabled for Variable type)
// DBG_LED flow synthesis
// (output disabled for Variable type)

  
// SDA flow acquisition
// (setup input disabled for Bus type)
// SDA flow synthesis
// (output disabled for Bus type)

  
// SCL flow acquisition
// (setup input disabled for Bus type)
// SCL flow synthesis
// (output disabled for Bus type)

  
// ResetRelay flow acquisition
// (setup input disabled for DO type)
// ResetRelay flow synthesis
void setup_ResetRelay_output(void){ 
pinMode(PORT_ResetRelay, OUTPUT); 
};

  
// LCD_DB7 flow acquisition
// (setup input disabled for DO type)
// LCD_DB7 flow synthesis
void setup_LCD_DB7_output(void){ 
pinMode(PORT_LCD_DB7, OUTPUT); 
};

  
// LCD_DB6 flow acquisition
// (setup input disabled for DO type)
// LCD_DB6 flow synthesis
void setup_LCD_DB6_output(void){ 
pinMode(PORT_LCD_DB6, OUTPUT); 
};

  
// LCD_DB5 flow acquisition
// (setup input disabled for DO type)
// LCD_DB5 flow synthesis
void setup_LCD_DB5_output(void){ 
pinMode(PORT_LCD_DB5, OUTPUT); 
};

  
// LCD_DB4 flow acquisition
// (setup input disabled for DO type)
// LCD_DB4 flow synthesis
void setup_LCD_DB4_output(void){ 
pinMode(PORT_LCD_DB4, OUTPUT); 
};

  
// LCD_DB3 flow acquisition
// (setup input disabled for DO type)
// LCD_DB3 flow synthesis
void setup_LCD_DB3_output(void){ 
pinMode(PORT_LCD_DB3, OUTPUT); 
};

  
// LCD_DB2 flow acquisition
// (setup input disabled for DO type)
// LCD_DB2 flow synthesis
void setup_LCD_DB2_output(void){ 
pinMode(PORT_LCD_DB2, OUTPUT); 
};

  
// LCD_DB1 flow acquisition
// (setup input disabled for DO type)
// LCD_DB1 flow synthesis
void setup_LCD_DB1_output(void){ 
pinMode(PORT_LCD_DB1, OUTPUT); 
};

  
// LCD_DB0 flow acquisition
// (setup input disabled for DO type)
// LCD_DB0 flow synthesis
void setup_LCD_DB0_output(void){ 
pinMode(PORT_LCD_DB0, OUTPUT); 
};

  
// BME_CS2 flow acquisition
// (setup input disabled for Bus type)
// BME_CS2 flow synthesis
// (output disabled for Bus type)

  
// BME_MISO flow acquisition
// (setup input disabled for Bus type)
// BME_MISO flow synthesis
// (output disabled for Bus type)

  
// BME_MOSI flow acquisition
// (setup input disabled for Bus type)
// BME_MOSI flow synthesis
// (output disabled for Bus type)

  
// BME_SCK flow acquisition
// (setup input disabled for Bus type)
// BME_SCK flow synthesis
// (output disabled for Bus type)

  
// BME_CS1 flow acquisition
// (setup input disabled for Bus type)
// BME_CS1 flow synthesis
// (output disabled for Bus type)

  
// _5V flow acquisition
// (setup input disabled for Power type)
// _5V flow synthesis
// (output disabled for Power type)

  
// GND flow acquisition
// (setup input disabled for Power type)
// GND flow synthesis
// (output disabled for Power type)

  
// INT_FS flow acquisition
// (setup input disabled for Variable type)
// INT_FS flow synthesis
// (output disabled for Variable type)

  
// INT_FC flow acquisition
// (setup input disabled for Variable type)
// INT_FC flow synthesis
// (output disabled for Variable type)

  
// INT_HS flow acquisition
// (setup input disabled for Variable type)
// INT_HS flow synthesis
// (output disabled for Variable type)

  
// INT_HC flow acquisition
// (setup input disabled for Variable type)
// INT_HC flow synthesis
// (output disabled for Variable type)

  
// SystemDate_W flow acquisition
// (setup input disabled for Variable type)
// SystemDate_W flow synthesis
// (output disabled for Variable type)

  
// SystemTime_W flow acquisition
// (setup input disabled for Variable type)
// SystemTime_W flow synthesis
// (output disabled for Variable type)

  
// PeakFactor flow acquisition
// (setup input disabled for Variable type)
// PeakFactor flow synthesis
// (output disabled for Variable type)

  
// PlateauFactor flow acquisition
// (setup input disabled for Variable type)
// PlateauFactor flow synthesis
// (output disabled for Variable type)

  
// PeepFactor flow acquisition
// (setup input disabled for Variable type)
// PeepFactor flow synthesis
// (output disabled for Variable type)

  
// FrequencyFactor flow acquisition
// (setup input disabled for Variable type)
// FrequencyFactor flow synthesis
// (output disabled for Variable type)

  
// TidalVolumenFactor flow acquisition
// (setup input disabled for Variable type)
// TidalVolumenFactor flow synthesis
// (output disabled for Variable type)

  
// MinimumVolumeFactor flow acquisition
// (setup input disabled for Variable type)
// MinimumVolumeFactor flow synthesis
// (output disabled for Variable type)

  
// TriggerFactor flow acquisition
// (setup input disabled for Variable type)
// TriggerFactor flow synthesis
// (output disabled for Variable type)

  
// BatteryLevelTrigger flow acquisition
// (setup input disabled for Variable type)
// BatteryLevelTrigger flow synthesis
// (output disabled for Variable type)

  
// PatientType flow acquisition
// (setup input disabled for Variable type)
// PatientType flow synthesis
// (output disabled for Variable type)

  
// PatientWeight flow acquisition
// (setup input disabled for Variable type)
// PatientWeight flow synthesis
// (output disabled for Variable type)

  
// PatientHeight flow acquisition
// (setup input disabled for Variable type)
// PatientHeight flow synthesis
// (output disabled for Variable type)

  
// TidalVolume flow acquisition
// (setup input disabled for Variable type)
// TidalVolume flow synthesis
// (output disabled for Variable type)

  
// PIDMode flow acquisition
// (setup input disabled for Variable type)
// PIDMode flow synthesis
// (output disabled for Variable type)

  
// PIDState flow acquisition
// (setup input disabled for Variable type)
// PIDState flow synthesis
// (output disabled for Variable type)

  
// PIDErrorBits flow acquisition
// (setup input disabled for Variable type)
// PIDErrorBits flow synthesis
// (output disabled for Variable type)

  
// Alarms flow acquisition
// (setup input disabled for Variable type)
// Alarms flow synthesis
// (output disabled for Variable type)

  
// Status flow acquisition
// (setup input disabled for Variable type)
// Status flow synthesis
// (output disabled for Variable type)

  
// POWER_StartMode flow acquisition
// (setup input disabled for Variable type)
// POWER_StartMode flow synthesis
// (output disabled for Variable type)

  
// POWER_StopMode flow acquisition
// (setup input disabled for Variable type)
// POWER_StopMode flow synthesis
// (output disabled for Variable type)

  
// HOME_Mode flow acquisition
// (setup input disabled for Variable type)
// HOME_Mode flow synthesis
// (output disabled for Variable type)

  
// SetWriteData flow acquisition
// (setup input disabled for BOOL type)
// SetWriteData flow synthesis
// (output disabled for BOOL type)

  
// SetTime flow acquisition
// (setup input disabled for BOOL type)
// SetTime flow synthesis
// (output disabled for BOOL type)

  
// Trigger flow acquisition
// (setup input disabled for BOOL type)
// Trigger flow synthesis
// (output disabled for BOOL type)

  
// PeakPBPlus flow acquisition
// (setup input disabled for BOOL type)
// PeakPBPlus flow synthesis
// (output disabled for BOOL type)

  
// PeakPBMinus flow acquisition
// (setup input disabled for BOOL type)
// PeakPBMinus flow synthesis
// (output disabled for BOOL type)

  
// PeepPBPlus flow acquisition
// (setup input disabled for BOOL type)
// PeepPBPlus flow synthesis
// (output disabled for BOOL type)

  
// PeepPBMinus flow acquisition
// (setup input disabled for BOOL type)
// PeepPBMinus flow synthesis
// (output disabled for BOOL type)

  
// FrPBPlus flow acquisition
// (setup input disabled for BOOL type)
// FrPBPlus flow synthesis
// (output disabled for BOOL type)

  
// FrPBMinus flow acquisition
// (setup input disabled for BOOL type)
// FrPBMinus flow synthesis
// (output disabled for BOOL type)

  
// RecruitmentOn flow acquisition
// (setup input disabled for BOOL type)
// RecruitmentOn flow synthesis
// (output disabled for BOOL type)

  
// RecruitmentOff flow acquisition
// (setup input disabled for BOOL type)
// RecruitmentOff flow synthesis
// (output disabled for BOOL type)

  
// RecruitmentMem flow acquisition
// (setup input disabled for BOOL type)
// RecruitmentMem flow synthesis
// (output disabled for BOOL type)

  
// RecruitmentRunning flow acquisition
// (setup input disabled for BOOL type)
// RecruitmentRunning flow synthesis
// (output disabled for BOOL type)

  
// RecruitmentEnding flow acquisition
// (setup input disabled for BOOL type)
// RecruitmentEnding flow synthesis
// (output disabled for BOOL type)

  
// RecruitmentEnd flow acquisition
// (setup input disabled for BOOL type)
// RecruitmentEnd flow synthesis
// (output disabled for BOOL type)

  
// ErrorAck flow acquisition
// (setup input disabled for BOOL type)
// ErrorAck flow synthesis
// (output disabled for BOOL type)

  
// Reset flow acquisition
// (setup input disabled for BOOL type)
// Reset flow synthesis
// (output disabled for BOOL type)

  
// PIDError flow acquisition
// (setup input disabled for BOOL type)
// PIDError flow synthesis
// (output disabled for BOOL type)

  
// PeakDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// PeakDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PeakDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// PeakDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PeakDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// PeakDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PeakDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// PeakDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PeakInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// PeakInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PeakInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// PeakInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PeakInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// PeakInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PeakInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// PeakInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PlateauDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// PlateauDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PlateauDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// PlateauDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PlateauDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// PlateauDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PlateauDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// PlateauDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PlateauInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// PlateauInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PlateauInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// PlateauInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PlateauInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// PlateauInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PlateauInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// PlateauInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PeepDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// PeepDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PeepDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// PeepDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PeepDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// PeepDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PeepDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// PeepDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PeepInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// PeepInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PeepInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// PeepInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PeepInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// PeepInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PeepInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// PeepInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// FrequencyDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// FrequencyDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// FrequencyDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// FrequencyDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// FrequencyDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// FrequencyDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// FrequencyDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// FrequencyDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// FrequencyInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// FrequencyInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// FrequencyInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// FrequencyInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// FrequencyInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// FrequencyInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// FrequencyInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// FrequencyInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// TidalVolumeDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// TidalVolumeDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// TidalVolumeDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// TidalVolumeDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// TidalVolumeDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// TidalVolumeDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// TidalVolumeDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// TidalVolumeDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// TidalVolumeInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// TidalVolumeInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// TidalVolumeInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// TidalVolumeInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// TidalVolumeInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// TidalVolumeInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// TidalVolumeInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// TidalVolumeInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// MinuteVolumeDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// MinuteVolumeDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// MinuteVolumeDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// MinuteVolumeDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// MinuteVolumeDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// MinuteVolumeDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// MinuteVolumeDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// MinuteVolumeDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// MinuteVolumeInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// MinuteVolumeInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// MinuteVolumeInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// MinuteVolumeInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// MinuteVolumeInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// MinuteVolumeInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// MinuteVolumeInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// MinuteVolumeInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// TriggerDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// TriggerDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// TriggerDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// TriggerDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// TriggerDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// TriggerDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// TriggerDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// TriggerDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// TriggerInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// TriggerInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// TriggerInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// TriggerInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// TriggerInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// TriggerInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// TriggerInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// TriggerInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// BatteryDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// BatteryDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// BatteryDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// BatteryDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// BatteryDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// BatteryDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// BatteryDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// BatteryDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// BatteryInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// BatteryInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// BatteryInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// BatteryInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// BatteryInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// BatteryInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// BatteryInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// BatteryInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// Enable flow acquisition
// (setup input disabled for BOOL type)
// Enable flow synthesis
// (output disabled for BOOL type)

  
// Busy flow acquisition
// (setup input disabled for BOOL type)
// Busy flow synthesis
// (output disabled for BOOL type)

  
// POWER_Enable flow acquisition
// (setup input disabled for BOOL type)
// POWER_Enable flow synthesis
// (output disabled for BOOL type)

  
// POWER_Status flow acquisition
// (setup input disabled for BOOL type)
// POWER_Status flow synthesis
// (output disabled for BOOL type)

  
// POWER_Error flow acquisition
// (setup input disabled for BOOL type)
// POWER_Error flow synthesis
// (output disabled for BOOL type)

  
// RESET_Execute flow acquisition
// (setup input disabled for BOOL type)
// RESET_Execute flow synthesis
// (output disabled for BOOL type)

  
// RESET_Done flow acquisition
// (setup input disabled for BOOL type)
// RESET_Done flow synthesis
// (output disabled for BOOL type)

  
// RESET_Error flow acquisition
// (setup input disabled for BOOL type)
// RESET_Error flow synthesis
// (output disabled for BOOL type)

  
// HOME_Execute flow acquisition
// (setup input disabled for BOOL type)
// HOME_Execute flow synthesis
// (output disabled for BOOL type)

  
// HOME_Done flow acquisition
// (setup input disabled for BOOL type)
// HOME_Done flow synthesis
// (output disabled for BOOL type)

  
// HOME_Error flow acquisition
// (setup input disabled for BOOL type)
// HOME_Error flow synthesis
// (output disabled for BOOL type)

  
// HALT_Execute flow acquisition
// (setup input disabled for BOOL type)
// HALT_Execute flow synthesis
// (output disabled for BOOL type)

  
// HALT_Done flow acquisition
// (setup input disabled for BOOL type)
// HALT_Done flow synthesis
// (output disabled for BOOL type)

  
// HALT_Error flow acquisition
// (setup input disabled for BOOL type)
// HALT_Error flow synthesis
// (output disabled for BOOL type)

  
// ABSOLUTE_Execute flow acquisition
// (setup input disabled for BOOL type)
// ABSOLUTE_Execute flow synthesis
// (output disabled for BOOL type)

  
// ABSOLUTE_Done flow acquisition
// (setup input disabled for BOOL type)
// ABSOLUTE_Done flow synthesis
// (output disabled for BOOL type)

  
// ABSOLUTE_Error flow acquisition
// (setup input disabled for BOOL type)
// ABSOLUTE_Error flow synthesis
// (output disabled for BOOL type)

  
// RELATIVE_Execute flow acquisition
// (setup input disabled for BOOL type)
// RELATIVE_Execute flow synthesis
// (output disabled for BOOL type)

  
// RELATIVE_Done flow acquisition
// (setup input disabled for BOOL type)
// RELATIVE_Done flow synthesis
// (output disabled for BOOL type)

  
// RELATIVE_Error flow acquisition
// (setup input disabled for BOOL type)
// RELATIVE_Error flow synthesis
// (output disabled for BOOL type)

  
// Velocity_Execute flow acquisition
// (setup input disabled for BOOL type)
// Velocity_Execute flow synthesis
// (output disabled for BOOL type)

  
// Velocity_Current flow acquisition
// (setup input disabled for BOOL type)
// Velocity_Current flow synthesis
// (output disabled for BOOL type)

  
// Velocity_invelocity flow acquisition
// (setup input disabled for BOOL type)
// Velocity_invelocity flow synthesis
// (output disabled for BOOL type)

  
// Velocity_Error flow acquisition
// (setup input disabled for BOOL type)
// Velocity_Error flow synthesis
// (output disabled for BOOL type)

  
// JOG_Forward flow acquisition
// (setup input disabled for BOOL type)
// JOG_Forward flow synthesis
// (output disabled for BOOL type)

  
// JOG_Backward flow acquisition
// (setup input disabled for BOOL type)
// JOG_Backward flow synthesis
// (output disabled for BOOL type)

  
// JOG_Invelocity flow acquisition
// (setup input disabled for BOOL type)
// JOG_Invelocity flow synthesis
// (output disabled for BOOL type)

  
// JOG_Error flow acquisition
// (setup input disabled for BOOL type)
// JOG_Error flow synthesis
// (output disabled for BOOL type)

  
// Permissions flow acquisition
// (setup input disabled for BOOL type)
// Permissions flow synthesis
// (output disabled for BOOL type)

  
// Operation_ON flow acquisition
// (setup input disabled for BOOL type)
// Operation_ON flow synthesis
// (output disabled for BOOL type)

  
// Emergency flow acquisition
// (setup input disabled for BOOL type)
// Emergency flow synthesis
// (output disabled for BOOL type)

  
// GeneralAck flow acquisition
// (setup input disabled for BOOL type)
// GeneralAck flow synthesis
// (output disabled for BOOL type)

  
// MotorAck flow acquisition
// (setup input disabled for BOOL type)
// MotorAck flow synthesis
// (output disabled for BOOL type)

  
// ProcessAck flow acquisition
// (setup input disabled for BOOL type)
// ProcessAck flow synthesis
// (output disabled for BOOL type)

  
// GeneralReset flow acquisition
// (setup input disabled for BOOL type)
// GeneralReset flow synthesis
// (output disabled for BOOL type)

  
// MotorReset flow acquisition
// (setup input disabled for BOOL type)
// MotorReset flow synthesis
// (output disabled for BOOL type)

  
// ProcessReset flow acquisition
// (setup input disabled for BOOL type)
// ProcessReset flow synthesis
// (output disabled for BOOL type)

  
// PeakValue flow acquisition
// (setup input disabled for UI_16 type)
// PeakValue flow synthesis
// (output disabled for UI_16 type)

  
// PlateauValue flow acquisition
// (setup input disabled for UI_16 type)
// PlateauValue flow synthesis
// (output disabled for UI_16 type)

  
// PeepValue flow acquisition
// (setup input disabled for UI_16 type)
// PeepValue flow synthesis
// (output disabled for UI_16 type)

  
// FrequencyValue flow acquisition
// (setup input disabled for UI_16 type)
// FrequencyValue flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumeValue flow acquisition
// (setup input disabled for UI_16 type)
// TidalVolumeValue flow synthesis
// (output disabled for UI_16 type)

  
// MinumValue flow acquisition
// (setup input disabled for UI_16 type)
// MinumValue flow synthesis
// (output disabled for UI_16 type)

  
// DrivingPreassure flow acquisition
// (setup input disabled for UI_16 type)
// DrivingPreassure flow synthesis
// (output disabled for UI_16 type)

  
// BatteryVoltage flow acquisition
// (setup input disabled for UI_16 type)
// BatteryVoltage flow synthesis
// (output disabled for UI_16 type)

  
// BatteryCapacity flow acquisition
// (setup input disabled for UI_16 type)
// BatteryCapacity flow synthesis
// (output disabled for UI_16 type)

  
// Pressure1 flow acquisition
// (setup input disabled for UI_16 type)
// Pressure1 flow synthesis
// (output disabled for UI_16 type)

  
// Pressure2 flow acquisition
// (setup input disabled for UI_16 type)
// Pressure2 flow synthesis
// (output disabled for UI_16 type)

  
// VomumeAccumulated flow acquisition
// (setup input disabled for UI_16 type)
// VomumeAccumulated flow synthesis
// (output disabled for UI_16 type)

  
// Flow flow acquisition
// (setup input disabled for UI_16 type)
// Flow flow synthesis
// (output disabled for UI_16 type)

  
// SystemDateRetVal flow acquisition
// (setup input disabled for UI_16 type)
// SystemDateRetVal flow synthesis
// (output disabled for UI_16 type)

  
// SystemDateOut flow acquisition
// (setup input disabled for DateTime type)
// SystemDateOut flow synthesis
// (output disabled for DateTime type)

  
// LocalDateRetVal flow acquisition
// (setup input disabled for UI_16 type)
// LocalDateRetVal flow synthesis
// (output disabled for UI_16 type)

  
// LocalDateOut flow acquisition
// (setup input disabled for DateTime type)
// LocalDateOut flow synthesis
// (output disabled for DateTime type)

  
// WriteDateTime flow acquisition
// (setup input disabled for DateTime type)
// WriteDateTime flow synthesis
// (output disabled for DateTime type)

  
// WriteDateOut flow acquisition
// (setup input disabled for DateTime type)
// WriteDateOut flow synthesis
// (output disabled for DateTime type)

  
// StatuSetTime flow acquisition
// (setup input disabled for DateTime type)
// StatuSetTime flow synthesis
// (output disabled for DateTime type)

  
// PeakMax flow acquisition
// (setup input disabled for UI_16 type)
// PeakMax flow synthesis
// (output disabled for UI_16 type)

  
// PeakSetpoint flow acquisition
// (setup input disabled for UI_16 type)
// PeakSetpoint flow synthesis
// (output disabled for UI_16 type)

  
// PeakMin flow acquisition
// (setup input disabled for UI_16 type)
// PeakMin flow synthesis
// (output disabled for UI_16 type)

  
// PlateauMax flow acquisition
// (setup input disabled for UI_16 type)
// PlateauMax flow synthesis
// (output disabled for UI_16 type)

  
// PlateauSetpoint flow acquisition
// (setup input disabled for UI_16 type)
// PlateauSetpoint flow synthesis
// (output disabled for UI_16 type)

  
// PlateauMin flow acquisition
// (setup input disabled for UI_16 type)
// PlateauMin flow synthesis
// (output disabled for UI_16 type)

  
// PeepMax flow acquisition
// (setup input disabled for UI_16 type)
// PeepMax flow synthesis
// (output disabled for UI_16 type)

  
// PeepSetpoint flow acquisition
// (setup input disabled for UI_16 type)
// PeepSetpoint flow synthesis
// (output disabled for UI_16 type)

  
// PeepMin flow acquisition
// (setup input disabled for UI_16 type)
// PeepMin flow synthesis
// (output disabled for UI_16 type)

  
// FrequencyMax flow acquisition
// (setup input disabled for UI_16 type)
// FrequencyMax flow synthesis
// (output disabled for UI_16 type)

  
// FrequencySetpoint flow acquisition
// (setup input disabled for UI_16 type)
// FrequencySetpoint flow synthesis
// (output disabled for UI_16 type)

  
// FrequencyMin flow acquisition
// (setup input disabled for UI_16 type)
// FrequencyMin flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumenMax flow acquisition
// (setup input disabled for UI_16 type)
// TidalVolumenMax flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumenSetpoint flow acquisition
// (setup input disabled for UI_16 type)
// TidalVolumenSetpoint flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumenMin flow acquisition
// (setup input disabled for UI_16 type)
// TidalVolumenMin flow synthesis
// (output disabled for UI_16 type)

  
// MinimumVolumeMax flow acquisition
// (setup input disabled for UI_16 type)
// MinimumVolumeMax flow synthesis
// (output disabled for UI_16 type)

  
// MinimumVolumeSetpoint flow acquisition
// (setup input disabled for UI_16 type)
// MinimumVolumeSetpoint flow synthesis
// (output disabled for UI_16 type)

  
// MinimumVolumeMin flow acquisition
// (setup input disabled for UI_16 type)
// MinimumVolumeMin flow synthesis
// (output disabled for UI_16 type)

  
// TriggerSetpoint flow acquisition
// (setup input disabled for UI_16 type)
// TriggerSetpoint flow synthesis
// (output disabled for UI_16 type)

  
// RecruitmentTimer flow acquisition
// (setup input disabled for TimeUI_16 type)
// RecruitmentTimer flow synthesis
// (output disabled for TimeUI_16 type)

  
// RecruitmentElap flow acquisition
// (setup input disabled for TimeUI_16 type)
// RecruitmentElap flow synthesis
// (output disabled for TimeUI_16 type)

  
// TimeStart flow acquisition
// (setup input disabled for DateTime type)
// TimeStart flow synthesis
// (output disabled for DateTime type)

  
// TimeEnd flow acquisition
// (setup input disabled for DateTime type)
// TimeEnd flow synthesis
// (output disabled for DateTime type)

  
// FlowSetpoint flow acquisition
// (setup input disabled for UI_16 type)
// FlowSetpoint flow synthesis
// (output disabled for UI_16 type)

  
// PIDOutReal flow acquisition
// (setup input disabled for FL_16 type)
// PIDOutReal flow synthesis
// (output disabled for FL_16 type)

  
// PIDOutInt flow acquisition
// (setup input disabled for UI_16 type)
// PIDOutInt flow synthesis
// (output disabled for UI_16 type)

  
// PIDOutPWM flow acquisition
// (setup input disabled for UI_16 type)
// PIDOutPWM flow synthesis
// (output disabled for UI_16 type)

  
// PIDOutMotor flow acquisition
// (setup input disabled for UI_16 type)
// PIDOutMotor flow synthesis
// (output disabled for UI_16 type)

  
// PeakSetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// PeakSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// PeakSetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// PeakSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// PeakSetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// PeakSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// PeakSetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// PeakSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// PeakSetpointHysteresisHI flow acquisition
// (setup input disabled for UI_16 type)
// PeakSetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type)

  
// PeakSetpointHysteresisLOW flow acquisition
// (setup input disabled for UI_16 type)
// PeakSetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type)

  
// PlateauSetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// PlateauSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// PlateauSetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// PlateauSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// PlateauSetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// PlateauSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// PlateaukSetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// PlateaukSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// PlateaukSetpointHysteresisHI flow acquisition
// (setup input disabled for UI_16 type)
// PlateaukSetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type)

  
// PlateaukSetpointHysteresisLOW flow acquisition
// (setup input disabled for UI_16 type)
// PlateaukSetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type)

  
// PeepSetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// PeepSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// PeepSetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// PeepSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// PeepSetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// PeepSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// PeepSetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// PeepSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// PeepSetpointHysteresisHI flow acquisition
// (setup input disabled for UI_16 type)
// PeepSetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type)

  
// PeepSetpointHysteresisLOW flow acquisition
// (setup input disabled for UI_16 type)
// PeepSetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type)

  
// FrequencySetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// FrequencySetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// FrequencySetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// FrequencySetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// FrequencySetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// FrequencySetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// FrequencySetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// FrequencySetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// FrequencySetpointHysteresisHI flow acquisition
// (setup input disabled for UI_16 type)
// FrequencySetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type)

  
// FrequencySetpointHysteresisLOW flow acquisition
// (setup input disabled for UI_16 type)
// FrequencySetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumeSetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// TidalVolumeSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumeSetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// TidalVolumeSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumeSetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// TidalVolumeSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumeSetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// TidalVolumeSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumeHystersisHI flow acquisition
// (setup input disabled for UI_16 type)
// TidalVolumeHystersisHI flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumeHystersisLOW flow acquisition
// (setup input disabled for UI_16 type)
// TidalVolumeHystersisLOW flow synthesis
// (output disabled for UI_16 type)

  
// MinuteVolumeSetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// MinuteVolumeSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// MinuteVolumeSetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// MinuteVolumeSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// MinuteVolumeSetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// MinuteVolumeSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// MinuteVolumeSetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// MinuteVolumeSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// MinuteVolumeHysteresisHI flow acquisition
// (setup input disabled for UI_16 type)
// MinuteVolumeHysteresisHI flow synthesis
// (output disabled for UI_16 type)

  
// MinuteVolumeHysteresisLOW flow acquisition
// (setup input disabled for UI_16 type)
// MinuteVolumeHysteresisLOW flow synthesis
// (output disabled for UI_16 type)

  
// TriggeVolumeSetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// TriggeVolumeSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// TriggerVolumeSetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// TriggerVolumeSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// TriggerVolumeSetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// TriggerVolumeSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// TriggerVolumeSetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// TriggerVolumeSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// TriggerVolumeHysteresisHI flow acquisition
// (setup input disabled for UI_16 type)
// TriggerVolumeHysteresisHI flow synthesis
// (output disabled for UI_16 type)

  
// TriggerVolumeHysteresisLOW flow acquisition
// (setup input disabled for UI_16 type)
// TriggerVolumeHysteresisLOW flow synthesis
// (output disabled for UI_16 type)

  
// BatterySetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// BatterySetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// BatterySetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// BatterySetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// BatterySetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// BatterySetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// BatterySetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// BatterySetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// BatterySetpointHysteresisHI flow acquisition
// (setup input disabled for UI_16 type)
// BatterySetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type)

  
// BatterySetpointHysteresisLOW flow acquisition
// (setup input disabled for UI_16 type)
// BatterySetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type)

  
// Frecuency flow acquisition
// (setup input disabled for UI_16 type)
// Frecuency flow synthesis
// (output disabled for UI_16 type)

  
// Duty_Cycle flow acquisition
// (setup input disabled for UI_16 type)
// Duty_Cycle flow synthesis
// (output disabled for UI_16 type)

  
// HOME_Position flow acquisition
// (setup input disabled for UI_16 type)
// HOME_Position flow synthesis
// (output disabled for UI_16 type)

  
// ABSOLUTE_Position flow acquisition
// (setup input disabled for UI_16 type)
// ABSOLUTE_Position flow synthesis
// (output disabled for UI_16 type)

  
// ABSOLUTE_Velocity flow acquisition
// (setup input disabled for UI_16 type)
// ABSOLUTE_Velocity flow synthesis
// (output disabled for UI_16 type)

  
// RELATIVE_Distance flow acquisition
// (setup input disabled for UI_16 type)
// RELATIVE_Distance flow synthesis
// (output disabled for UI_16 type)

  
// RELATIVE_Velocity flow acquisition
// (setup input disabled for UI_16 type)
// RELATIVE_Velocity flow synthesis
// (output disabled for UI_16 type)

  
// Velocity_Velocity flow acquisition
// (setup input disabled for UI_16 type)
// Velocity_Velocity flow synthesis
// (output disabled for UI_16 type)

  
// JOG_Velocity flow acquisition
// (setup input disabled for UI_16 type)
// JOG_Velocity flow synthesis
// (output disabled for UI_16 type)

  
// EmergencyStop flow acquisition
void setup_EmergencyStop(void){ 
pinMode(PORT_EmergencyStop,INPUT); 
};


// EmergencyStop flow synthesis
// (output disabled for DI type)

  
// TurnAxisHwHi flow acquisition
void setup_TurnAxisHwHi(void){ 
pinMode(PORT_TurnAxisHwHi, INPUT_PULLUP); 
};
// TurnAxisHwHi flow synthesis
// (output disabled for DI_pu type)

  
// BeaconLightRed flow acquisition
// (setup input disabled for DO type)
// BeaconLightRed flow synthesis
void setup_BeaconLightRed_output(void){ 
pinMode(PORT_BeaconLightRed, OUTPUT); 
};

  
// BeaconLightOrange flow acquisition
// (setup input disabled for DO type)
// BeaconLightOrange flow synthesis
void setup_BeaconLightOrange_output(void){ 
pinMode(PORT_BeaconLightOrange, OUTPUT); 
};

  
// TurnAxisPulse flow acquisition
// (setup input disabled for DO type)
// TurnAxisPulse flow synthesis
void setup_TurnAxisPulse_output(void){ 
pinMode(PORT_TurnAxisPulse, OUTPUT); 
};

  
// TurnAxisDir flow acquisition
// (setup input disabled for DO type)
// TurnAxisDir flow synthesis
void setup_TurnAxisDir_output(void){ 
pinMode(PORT_TurnAxisDir, OUTPUT); 
};

  
// TurnAxisEnable flow acquisition
// (setup input disabled for DO type)
// TurnAxisEnable flow synthesis
void setup_TurnAxisEnable_output(void){ 
pinMode(PORT_TurnAxisEnable, OUTPUT); 
};

  
// BeaconLightGreen flow acquisition
// (setup input disabled for DO type)
// BeaconLightGreen flow synthesis
void setup_BeaconLightGreen_output(void){ 
pinMode(PORT_BeaconLightGreen, OUTPUT); 
};

  
// Buzzer flow acquisition
// (setup input disabled for DO type)
// Buzzer flow synthesis
void setup_Buzzer_output(void){ 
pinMode(PORT_Buzzer, OUTPUT); 
};

  
// PeepValve flow acquisition
// (setup input disabled for DO type)
// PeepValve flow synthesis
void setup_PeepValve_output(void){ 
pinMode(PORT_PeepValve, OUTPUT); 
};

  
// TurnAxisHwLow flow acquisition
void setup_TurnAxisHwLow(void){ 
pinMode(PORT_TurnAxisHwLow, INPUT_PULLUP); 
};
// TurnAxisHwLow flow synthesis
// (output disabled for DI_pu type)

  
// ServoPend flow acquisition
void setup_ServoPend(void){ 
pinMode(PORT_ServoPend,INPUT); 
};


// ServoPend flow synthesis
// (output disabled for DI type)

  
// ServoAlarm flow acquisition
void setup_ServoAlarm(void){ 
pinMode(PORT_ServoAlarm, INPUT_PULLUP); 
};
// ServoAlarm flow synthesis
// (output disabled for DI_pu type)

  
// AtmosfericPressure flow acquisition
void setup_AtmosfericPressure(void){ 
pinMode(PORT_AtmosfericPressure,INPUT); 
};
// AtmosfericPressure flow synthesis
// (output disabled for ADC type)

  
// DiferentialPressure flow acquisition
void setup_DiferentialPressure(void){ 
pinMode(PORT_DiferentialPressure,INPUT); 
};
// DiferentialPressure flow synthesis
// (output disabled for ADC type)

  
// SerTX flow acquisition
// (setup input disabled for Bus type)
// SerTX flow synthesis
// (output disabled for Bus type)

  
// SerRX flow acquisition
// (setup input disabled for Bus type)
// SerRX flow synthesis
// (output disabled for Bus type)

  
// EmgcyButton flow acquisition
// (setup input disabled for TBD type)
// EmgcyButton flow synthesis
// (output disabled for TBD type)

  
// ApiVersion  flow acquisition
// (setup input disabled for UI_8 type)
// ApiVersion  flow synthesis
// (output disabled for UI_8 type)

  
// MachineUuid flow acquisition
// (setup input disabled for Variable type)
// MachineUuid flow synthesis
// (output disabled for Variable type)

  
// FirmwareVersion flow acquisition
// (setup input disabled for UI_16 type)
// FirmwareVersion flow synthesis
// (output disabled for UI_16 type)

  
// Uptime15mCounter flow acquisition
// (setup input disabled for UI_16 type)
// Uptime15mCounter flow synthesis
// (output disabled for UI_16 type)

  
// MutedAlarmSeconds flow acquisition
// (setup input disabled for UI_8 type)
// MutedAlarmSeconds flow synthesis
// (output disabled for UI_8 type)

  
// CycleRpmLast30s flow acquisition
// (setup input disabled for UI_16 type)
// CycleRpmLast30s flow synthesis
// (output disabled for UI_16 type)

  
// CycleIndications flow acquisition
// (setup input disabled for Variable type)
// CycleIndications flow synthesis
// (output disabled for Variable type)

  
// CycleDuration flow acquisition
// (setup input disabled for TimeUI_16 type)
// CycleDuration flow synthesis
// (output disabled for TimeUI_16 type)

  
// VentilationFlags flow acquisition
// (setup input disabled for Variable type)
// VentilationFlags flow synthesis
// (output disabled for Variable type)

  
// VolumeSetting flow acquisition
// (setup input disabled for UI_16 type)
// VolumeSetting flow synthesis
// (output disabled for UI_16 type)

  
// RpmSetting flow acquisition
// (setup input disabled for UI_16 type)
// RpmSetting flow synthesis
// (output disabled for UI_16 type)

  
// RampDegreesSetting flow acquisition
// (setup input disabled for UI_8 type)
// RampDegreesSetting flow synthesis
// (output disabled for UI_8 type)

  
// EiRatioSetting flow acquisition
// (setup input disabled for UI_8 type)
// EiRatioSetting flow synthesis
// (output disabled for UI_8 type)

  
// RecruitTimer flow acquisition
// (setup input disabled for UI_8 type)
// RecruitTimer flow synthesis
// (output disabled for UI_8 type)

  
// IncomingFrame flow acquisition
// (setup input disabled for Variable type)
// IncomingFrame flow synthesis
// (output disabled for Variable type)

  
// OutgoingFrame flow acquisition
// (setup input disabled for Variable type)
// OutgoingFrame flow synthesis
// (output disabled for Variable type)



// --- DRE flow acquisition and flow synthesis functions ---

  
// STATUS_LED flow acquisition
// (input disabled for LED type)
// STATUS_LED flow synthesis
// (output disabled for LED type)

  
// DO_STATUS_LED flow acquisition
// (input disabled for DO type)
// DO_STATUS_LED flow synthesis
void synthesize_DO_STATUS_LED(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_DO_STATUS_LED==TRUE) { 
digitalWrite(PORT_DO_STATUS_LED,diag.DO_STATUS_LED); 
} else { 
#endif 
digitalWrite(PORT_DO_STATUS_LED,dre.DO_STATUS_LED); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};

  
// dbgLedBlinkTimer flow acquisition
// (input disabled for Variable type)
// dbgLedBlinkTimer flow synthesis
// (output disabled for Variable type)

  
// DBG_LED flow acquisition
// (input disabled for Variable type)
// DBG_LED flow synthesis
// (output disabled for Variable type)

  
// SDA flow acquisition
// (input disabled for Bus type)
// SDA flow synthesis
// (output disabled for Bus type)

  
// SCL flow acquisition
// (input disabled for Bus type)
// SCL flow synthesis
// (output disabled for Bus type)

  
// ResetRelay flow acquisition
// (input disabled for DO type)
// ResetRelay flow synthesis
void synthesize_ResetRelay(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_ResetRelay==TRUE) { 
digitalWrite(PORT_ResetRelay,diag.ResetRelay); 
} else { 
#endif 
digitalWrite(PORT_ResetRelay,dre.ResetRelay); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};

  
// LCD_DB7 flow acquisition
// (input disabled for DO type)
// LCD_DB7 flow synthesis
void synthesize_LCD_DB7(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_LCD_DB7==TRUE) { 
digitalWrite(PORT_LCD_DB7,diag.LCD_DB7); 
} else { 
#endif 
digitalWrite(PORT_LCD_DB7,dre.LCD_DB7); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};

  
// LCD_DB6 flow acquisition
// (input disabled for DO type)
// LCD_DB6 flow synthesis
void synthesize_LCD_DB6(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_LCD_DB6==TRUE) { 
digitalWrite(PORT_LCD_DB6,diag.LCD_DB6); 
} else { 
#endif 
digitalWrite(PORT_LCD_DB6,dre.LCD_DB6); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};

  
// LCD_DB5 flow acquisition
// (input disabled for DO type)
// LCD_DB5 flow synthesis
void synthesize_LCD_DB5(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_LCD_DB5==TRUE) { 
digitalWrite(PORT_LCD_DB5,diag.LCD_DB5); 
} else { 
#endif 
digitalWrite(PORT_LCD_DB5,dre.LCD_DB5); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};

  
// LCD_DB4 flow acquisition
// (input disabled for DO type)
// LCD_DB4 flow synthesis
void synthesize_LCD_DB4(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_LCD_DB4==TRUE) { 
digitalWrite(PORT_LCD_DB4,diag.LCD_DB4); 
} else { 
#endif 
digitalWrite(PORT_LCD_DB4,dre.LCD_DB4); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};

  
// LCD_DB3 flow acquisition
// (input disabled for DO type)
// LCD_DB3 flow synthesis
void synthesize_LCD_DB3(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_LCD_DB3==TRUE) { 
digitalWrite(PORT_LCD_DB3,diag.LCD_DB3); 
} else { 
#endif 
digitalWrite(PORT_LCD_DB3,dre.LCD_DB3); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};

  
// LCD_DB2 flow acquisition
// (input disabled for DO type)
// LCD_DB2 flow synthesis
void synthesize_LCD_DB2(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_LCD_DB2==TRUE) { 
digitalWrite(PORT_LCD_DB2,diag.LCD_DB2); 
} else { 
#endif 
digitalWrite(PORT_LCD_DB2,dre.LCD_DB2); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};

  
// LCD_DB1 flow acquisition
// (input disabled for DO type)
// LCD_DB1 flow synthesis
void synthesize_LCD_DB1(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_LCD_DB1==TRUE) { 
digitalWrite(PORT_LCD_DB1,diag.LCD_DB1); 
} else { 
#endif 
digitalWrite(PORT_LCD_DB1,dre.LCD_DB1); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};

  
// LCD_DB0 flow acquisition
// (input disabled for DO type)
// LCD_DB0 flow synthesis
void synthesize_LCD_DB0(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_LCD_DB0==TRUE) { 
digitalWrite(PORT_LCD_DB0,diag.LCD_DB0); 
} else { 
#endif 
digitalWrite(PORT_LCD_DB0,dre.LCD_DB0); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};

  
// BME_CS2 flow acquisition
// (input disabled for Bus type)
// BME_CS2 flow synthesis
// (output disabled for Bus type)

  
// BME_MISO flow acquisition
// (input disabled for Bus type)
// BME_MISO flow synthesis
// (output disabled for Bus type)

  
// BME_MOSI flow acquisition
// (input disabled for Bus type)
// BME_MOSI flow synthesis
// (output disabled for Bus type)

  
// BME_SCK flow acquisition
// (input disabled for Bus type)
// BME_SCK flow synthesis
// (output disabled for Bus type)

  
// BME_CS1 flow acquisition
// (input disabled for Bus type)
// BME_CS1 flow synthesis
// (output disabled for Bus type)

  
// _5V flow acquisition
// (input disabled for Power type)
// _5V flow synthesis
// (output disabled for Power type)

  
// GND flow acquisition
// (input disabled for Power type)
// GND flow synthesis
// (output disabled for Power type)

  
// INT_FS flow acquisition
// (input disabled for Variable type)
// INT_FS flow synthesis
// (output disabled for Variable type)

  
// INT_FC flow acquisition
// (input disabled for Variable type)
// INT_FC flow synthesis
// (output disabled for Variable type)

  
// INT_HS flow acquisition
// (input disabled for Variable type)
// INT_HS flow synthesis
// (output disabled for Variable type)

  
// INT_HC flow acquisition
// (input disabled for Variable type)
// INT_HC flow synthesis
// (output disabled for Variable type)

  
// SystemDate_W flow acquisition
// (input disabled for Variable type)
// SystemDate_W flow synthesis
// (output disabled for Variable type)

  
// SystemTime_W flow acquisition
// (input disabled for Variable type)
// SystemTime_W flow synthesis
// (output disabled for Variable type)

  
// PeakFactor flow acquisition
// (input disabled for Variable type)
// PeakFactor flow synthesis
// (output disabled for Variable type)

  
// PlateauFactor flow acquisition
// (input disabled for Variable type)
// PlateauFactor flow synthesis
// (output disabled for Variable type)

  
// PeepFactor flow acquisition
// (input disabled for Variable type)
// PeepFactor flow synthesis
// (output disabled for Variable type)

  
// FrequencyFactor flow acquisition
// (input disabled for Variable type)
// FrequencyFactor flow synthesis
// (output disabled for Variable type)

  
// TidalVolumenFactor flow acquisition
// (input disabled for Variable type)
// TidalVolumenFactor flow synthesis
// (output disabled for Variable type)

  
// MinimumVolumeFactor flow acquisition
// (input disabled for Variable type)
// MinimumVolumeFactor flow synthesis
// (output disabled for Variable type)

  
// TriggerFactor flow acquisition
// (input disabled for Variable type)
// TriggerFactor flow synthesis
// (output disabled for Variable type)

  
// BatteryLevelTrigger flow acquisition
// (input disabled for Variable type)
// BatteryLevelTrigger flow synthesis
// (output disabled for Variable type)

  
// PatientType flow acquisition
// (input disabled for Variable type)
// PatientType flow synthesis
// (output disabled for Variable type)

  
// PatientWeight flow acquisition
// (input disabled for Variable type)
// PatientWeight flow synthesis
// (output disabled for Variable type)

  
// PatientHeight flow acquisition
// (input disabled for Variable type)
// PatientHeight flow synthesis
// (output disabled for Variable type)

  
// TidalVolume flow acquisition
// (input disabled for Variable type)
// TidalVolume flow synthesis
// (output disabled for Variable type)

  
// PIDMode flow acquisition
// (input disabled for Variable type)
// PIDMode flow synthesis
// (output disabled for Variable type)

  
// PIDState flow acquisition
// (input disabled for Variable type)
// PIDState flow synthesis
// (output disabled for Variable type)

  
// PIDErrorBits flow acquisition
// (input disabled for Variable type)
// PIDErrorBits flow synthesis
// (output disabled for Variable type)

  
// Alarms flow acquisition
// (input disabled for Variable type)
// Alarms flow synthesis
// (output disabled for Variable type)

  
// Status flow acquisition
// (input disabled for Variable type)
// Status flow synthesis
// (output disabled for Variable type)

  
// POWER_StartMode flow acquisition
// (input disabled for Variable type)
// POWER_StartMode flow synthesis
// (output disabled for Variable type)

  
// POWER_StopMode flow acquisition
// (input disabled for Variable type)
// POWER_StopMode flow synthesis
// (output disabled for Variable type)

  
// HOME_Mode flow acquisition
// (input disabled for Variable type)
// HOME_Mode flow synthesis
// (output disabled for Variable type)

  
// SetWriteData flow acquisition
// (input disabled for BOOL type)
// SetWriteData flow synthesis
// (output disabled for BOOL type)

  
// SetTime flow acquisition
// (input disabled for BOOL type)
// SetTime flow synthesis
// (output disabled for BOOL type)

  
// Trigger flow acquisition
// (input disabled for BOOL type)
// Trigger flow synthesis
// (output disabled for BOOL type)

  
// PeakPBPlus flow acquisition
// (input disabled for BOOL type)
// PeakPBPlus flow synthesis
// (output disabled for BOOL type)

  
// PeakPBMinus flow acquisition
// (input disabled for BOOL type)
// PeakPBMinus flow synthesis
// (output disabled for BOOL type)

  
// PeepPBPlus flow acquisition
// (input disabled for BOOL type)
// PeepPBPlus flow synthesis
// (output disabled for BOOL type)

  
// PeepPBMinus flow acquisition
// (input disabled for BOOL type)
// PeepPBMinus flow synthesis
// (output disabled for BOOL type)

  
// FrPBPlus flow acquisition
// (input disabled for BOOL type)
// FrPBPlus flow synthesis
// (output disabled for BOOL type)

  
// FrPBMinus flow acquisition
// (input disabled for BOOL type)
// FrPBMinus flow synthesis
// (output disabled for BOOL type)

  
// RecruitmentOn flow acquisition
// (input disabled for BOOL type)
// RecruitmentOn flow synthesis
// (output disabled for BOOL type)

  
// RecruitmentOff flow acquisition
// (input disabled for BOOL type)
// RecruitmentOff flow synthesis
// (output disabled for BOOL type)

  
// RecruitmentMem flow acquisition
// (input disabled for BOOL type)
// RecruitmentMem flow synthesis
// (output disabled for BOOL type)

  
// RecruitmentRunning flow acquisition
// (input disabled for BOOL type)
// RecruitmentRunning flow synthesis
// (output disabled for BOOL type)

  
// RecruitmentEnding flow acquisition
// (input disabled for BOOL type)
// RecruitmentEnding flow synthesis
// (output disabled for BOOL type)

  
// RecruitmentEnd flow acquisition
// (input disabled for BOOL type)
// RecruitmentEnd flow synthesis
// (output disabled for BOOL type)

  
// ErrorAck flow acquisition
// (input disabled for BOOL type)
// ErrorAck flow synthesis
// (output disabled for BOOL type)

  
// Reset flow acquisition
// (input disabled for BOOL type)
// Reset flow synthesis
// (output disabled for BOOL type)

  
// PIDError flow acquisition
// (input disabled for BOOL type)
// PIDError flow synthesis
// (output disabled for BOOL type)

  
// PeakDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// PeakDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PeakDisplayAlarmHI flow acquisition
// (input disabled for BOOL type)
// PeakDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PeakDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// PeakDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PeakDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type)
// PeakDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PeakInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// PeakInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PeakInterlockAlarmHI flow acquisition
// (input disabled for BOOL type)
// PeakInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PeakInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// PeakInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PeakInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type)
// PeakInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PlateauDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// PlateauDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PlateauDisplayAlarmHI flow acquisition
// (input disabled for BOOL type)
// PlateauDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PlateauDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// PlateauDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PlateauDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type)
// PlateauDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PlateauInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// PlateauInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PlateauInterlockAlarmHI flow acquisition
// (input disabled for BOOL type)
// PlateauInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PlateauInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// PlateauInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PlateauInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type)
// PlateauInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PeepDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// PeepDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PeepDisplayAlarmHI flow acquisition
// (input disabled for BOOL type)
// PeepDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PeepDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// PeepDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PeepDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type)
// PeepDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PeepInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// PeepInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PeepInterlockAlarmHI flow acquisition
// (input disabled for BOOL type)
// PeepInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PeepInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// PeepInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PeepInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type)
// PeepInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// FrequencyDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// FrequencyDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// FrequencyDisplayAlarmHI flow acquisition
// (input disabled for BOOL type)
// FrequencyDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// FrequencyDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// FrequencyDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// FrequencyDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type)
// FrequencyDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// FrequencyInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// FrequencyInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// FrequencyInterlockAlarmHI flow acquisition
// (input disabled for BOOL type)
// FrequencyInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// FrequencyInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// FrequencyInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// FrequencyInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type)
// FrequencyInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// TidalVolumeDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// TidalVolumeDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// TidalVolumeDisplayAlarmHI flow acquisition
// (input disabled for BOOL type)
// TidalVolumeDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// TidalVolumeDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// TidalVolumeDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// TidalVolumeDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type)
// TidalVolumeDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// TidalVolumeInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// TidalVolumeInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// TidalVolumeInterlockAlarmHI flow acquisition
// (input disabled for BOOL type)
// TidalVolumeInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// TidalVolumeInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// TidalVolumeInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// TidalVolumeInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type)
// TidalVolumeInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// MinuteVolumeDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// MinuteVolumeDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// MinuteVolumeDisplayAlarmHI flow acquisition
// (input disabled for BOOL type)
// MinuteVolumeDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// MinuteVolumeDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// MinuteVolumeDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// MinuteVolumeDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type)
// MinuteVolumeDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// MinuteVolumeInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// MinuteVolumeInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// MinuteVolumeInterlockAlarmHI flow acquisition
// (input disabled for BOOL type)
// MinuteVolumeInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// MinuteVolumeInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// MinuteVolumeInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// MinuteVolumeInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type)
// MinuteVolumeInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// TriggerDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// TriggerDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// TriggerDisplayAlarmHI flow acquisition
// (input disabled for BOOL type)
// TriggerDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// TriggerDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// TriggerDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// TriggerDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type)
// TriggerDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// TriggerInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// TriggerInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// TriggerInterlockAlarmHI flow acquisition
// (input disabled for BOOL type)
// TriggerInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// TriggerInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// TriggerInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// TriggerInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type)
// TriggerInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// BatteryDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// BatteryDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// BatteryDisplayAlarmHI flow acquisition
// (input disabled for BOOL type)
// BatteryDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// BatteryDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// BatteryDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// BatteryDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type)
// BatteryDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// BatteryInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// BatteryInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// BatteryInterlockAlarmHI flow acquisition
// (input disabled for BOOL type)
// BatteryInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// BatteryInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// BatteryInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// BatteryInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type)
// BatteryInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// Enable flow acquisition
// (input disabled for BOOL type)
// Enable flow synthesis
// (output disabled for BOOL type)

  
// Busy flow acquisition
// (input disabled for BOOL type)
// Busy flow synthesis
// (output disabled for BOOL type)

  
// POWER_Enable flow acquisition
// (input disabled for BOOL type)
// POWER_Enable flow synthesis
// (output disabled for BOOL type)

  
// POWER_Status flow acquisition
// (input disabled for BOOL type)
// POWER_Status flow synthesis
// (output disabled for BOOL type)

  
// POWER_Error flow acquisition
// (input disabled for BOOL type)
// POWER_Error flow synthesis
// (output disabled for BOOL type)

  
// RESET_Execute flow acquisition
// (input disabled for BOOL type)
// RESET_Execute flow synthesis
// (output disabled for BOOL type)

  
// RESET_Done flow acquisition
// (input disabled for BOOL type)
// RESET_Done flow synthesis
// (output disabled for BOOL type)

  
// RESET_Error flow acquisition
// (input disabled for BOOL type)
// RESET_Error flow synthesis
// (output disabled for BOOL type)

  
// HOME_Execute flow acquisition
// (input disabled for BOOL type)
// HOME_Execute flow synthesis
// (output disabled for BOOL type)

  
// HOME_Done flow acquisition
// (input disabled for BOOL type)
// HOME_Done flow synthesis
// (output disabled for BOOL type)

  
// HOME_Error flow acquisition
// (input disabled for BOOL type)
// HOME_Error flow synthesis
// (output disabled for BOOL type)

  
// HALT_Execute flow acquisition
// (input disabled for BOOL type)
// HALT_Execute flow synthesis
// (output disabled for BOOL type)

  
// HALT_Done flow acquisition
// (input disabled for BOOL type)
// HALT_Done flow synthesis
// (output disabled for BOOL type)

  
// HALT_Error flow acquisition
// (input disabled for BOOL type)
// HALT_Error flow synthesis
// (output disabled for BOOL type)

  
// ABSOLUTE_Execute flow acquisition
// (input disabled for BOOL type)
// ABSOLUTE_Execute flow synthesis
// (output disabled for BOOL type)

  
// ABSOLUTE_Done flow acquisition
// (input disabled for BOOL type)
// ABSOLUTE_Done flow synthesis
// (output disabled for BOOL type)

  
// ABSOLUTE_Error flow acquisition
// (input disabled for BOOL type)
// ABSOLUTE_Error flow synthesis
// (output disabled for BOOL type)

  
// RELATIVE_Execute flow acquisition
// (input disabled for BOOL type)
// RELATIVE_Execute flow synthesis
// (output disabled for BOOL type)

  
// RELATIVE_Done flow acquisition
// (input disabled for BOOL type)
// RELATIVE_Done flow synthesis
// (output disabled for BOOL type)

  
// RELATIVE_Error flow acquisition
// (input disabled for BOOL type)
// RELATIVE_Error flow synthesis
// (output disabled for BOOL type)

  
// Velocity_Execute flow acquisition
// (input disabled for BOOL type)
// Velocity_Execute flow synthesis
// (output disabled for BOOL type)

  
// Velocity_Current flow acquisition
// (input disabled for BOOL type)
// Velocity_Current flow synthesis
// (output disabled for BOOL type)

  
// Velocity_invelocity flow acquisition
// (input disabled for BOOL type)
// Velocity_invelocity flow synthesis
// (output disabled for BOOL type)

  
// Velocity_Error flow acquisition
// (input disabled for BOOL type)
// Velocity_Error flow synthesis
// (output disabled for BOOL type)

  
// JOG_Forward flow acquisition
// (input disabled for BOOL type)
// JOG_Forward flow synthesis
// (output disabled for BOOL type)

  
// JOG_Backward flow acquisition
// (input disabled for BOOL type)
// JOG_Backward flow synthesis
// (output disabled for BOOL type)

  
// JOG_Invelocity flow acquisition
// (input disabled for BOOL type)
// JOG_Invelocity flow synthesis
// (output disabled for BOOL type)

  
// JOG_Error flow acquisition
// (input disabled for BOOL type)
// JOG_Error flow synthesis
// (output disabled for BOOL type)

  
// Permissions flow acquisition
// (input disabled for BOOL type)
// Permissions flow synthesis
// (output disabled for BOOL type)

  
// Operation_ON flow acquisition
// (input disabled for BOOL type)
// Operation_ON flow synthesis
// (output disabled for BOOL type)

  
// Emergency flow acquisition
// (input disabled for BOOL type)
// Emergency flow synthesis
// (output disabled for BOOL type)

  
// GeneralAck flow acquisition
// (input disabled for BOOL type)
// GeneralAck flow synthesis
// (output disabled for BOOL type)

  
// MotorAck flow acquisition
// (input disabled for BOOL type)
// MotorAck flow synthesis
// (output disabled for BOOL type)

  
// ProcessAck flow acquisition
// (input disabled for BOOL type)
// ProcessAck flow synthesis
// (output disabled for BOOL type)

  
// GeneralReset flow acquisition
// (input disabled for BOOL type)
// GeneralReset flow synthesis
// (output disabled for BOOL type)

  
// MotorReset flow acquisition
// (input disabled for BOOL type)
// MotorReset flow synthesis
// (output disabled for BOOL type)

  
// ProcessReset flow acquisition
// (input disabled for BOOL type)
// ProcessReset flow synthesis
// (output disabled for BOOL type)

  
// PeakValue flow acquisition
// (input disabled for UI_16 type)
// PeakValue flow synthesis
// (output disabled for UI_16 type)

  
// PlateauValue flow acquisition
// (input disabled for UI_16 type)
// PlateauValue flow synthesis
// (output disabled for UI_16 type)

  
// PeepValue flow acquisition
// (input disabled for UI_16 type)
// PeepValue flow synthesis
// (output disabled for UI_16 type)

  
// FrequencyValue flow acquisition
// (input disabled for UI_16 type)
// FrequencyValue flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumeValue flow acquisition
// (input disabled for UI_16 type)
// TidalVolumeValue flow synthesis
// (output disabled for UI_16 type)

  
// MinumValue flow acquisition
// (input disabled for UI_16 type)
// MinumValue flow synthesis
// (output disabled for UI_16 type)

  
// DrivingPreassure flow acquisition
// (input disabled for UI_16 type)
// DrivingPreassure flow synthesis
// (output disabled for UI_16 type)

  
// BatteryVoltage flow acquisition
// (input disabled for UI_16 type)
// BatteryVoltage flow synthesis
// (output disabled for UI_16 type)

  
// BatteryCapacity flow acquisition
// (input disabled for UI_16 type)
// BatteryCapacity flow synthesis
// (output disabled for UI_16 type)

  
// Pressure1 flow acquisition
// (input disabled for UI_16 type)
// Pressure1 flow synthesis
// (output disabled for UI_16 type)

  
// Pressure2 flow acquisition
// (input disabled for UI_16 type)
// Pressure2 flow synthesis
// (output disabled for UI_16 type)

  
// VomumeAccumulated flow acquisition
// (input disabled for UI_16 type)
// VomumeAccumulated flow synthesis
// (output disabled for UI_16 type)

  
// Flow flow acquisition
// (input disabled for UI_16 type)
// Flow flow synthesis
// (output disabled for UI_16 type)

  
// SystemDateRetVal flow acquisition
// (input disabled for UI_16 type)
// SystemDateRetVal flow synthesis
// (output disabled for UI_16 type)

  
// SystemDateOut flow acquisition
// (input disabled for DateTime type)
// SystemDateOut flow synthesis
// (output disabled for DateTime type)

  
// LocalDateRetVal flow acquisition
// (input disabled for UI_16 type)
// LocalDateRetVal flow synthesis
// (output disabled for UI_16 type)

  
// LocalDateOut flow acquisition
// (input disabled for DateTime type)
// LocalDateOut flow synthesis
// (output disabled for DateTime type)

  
// WriteDateTime flow acquisition
// (input disabled for DateTime type)
// WriteDateTime flow synthesis
// (output disabled for DateTime type)

  
// WriteDateOut flow acquisition
// (input disabled for DateTime type)
// WriteDateOut flow synthesis
// (output disabled for DateTime type)

  
// StatuSetTime flow acquisition
// (input disabled for DateTime type)
// StatuSetTime flow synthesis
// (output disabled for DateTime type)

  
// PeakMax flow acquisition
// (input disabled for UI_16 type)
// PeakMax flow synthesis
// (output disabled for UI_16 type)

  
// PeakSetpoint flow acquisition
// (input disabled for UI_16 type)
// PeakSetpoint flow synthesis
// (output disabled for UI_16 type)

  
// PeakMin flow acquisition
// (input disabled for UI_16 type)
// PeakMin flow synthesis
// (output disabled for UI_16 type)

  
// PlateauMax flow acquisition
// (input disabled for UI_16 type)
// PlateauMax flow synthesis
// (output disabled for UI_16 type)

  
// PlateauSetpoint flow acquisition
// (input disabled for UI_16 type)
// PlateauSetpoint flow synthesis
// (output disabled for UI_16 type)

  
// PlateauMin flow acquisition
// (input disabled for UI_16 type)
// PlateauMin flow synthesis
// (output disabled for UI_16 type)

  
// PeepMax flow acquisition
// (input disabled for UI_16 type)
// PeepMax flow synthesis
// (output disabled for UI_16 type)

  
// PeepSetpoint flow acquisition
// (input disabled for UI_16 type)
// PeepSetpoint flow synthesis
// (output disabled for UI_16 type)

  
// PeepMin flow acquisition
// (input disabled for UI_16 type)
// PeepMin flow synthesis
// (output disabled for UI_16 type)

  
// FrequencyMax flow acquisition
// (input disabled for UI_16 type)
// FrequencyMax flow synthesis
// (output disabled for UI_16 type)

  
// FrequencySetpoint flow acquisition
// (input disabled for UI_16 type)
// FrequencySetpoint flow synthesis
// (output disabled for UI_16 type)

  
// FrequencyMin flow acquisition
// (input disabled for UI_16 type)
// FrequencyMin flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumenMax flow acquisition
// (input disabled for UI_16 type)
// TidalVolumenMax flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumenSetpoint flow acquisition
// (input disabled for UI_16 type)
// TidalVolumenSetpoint flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumenMin flow acquisition
// (input disabled for UI_16 type)
// TidalVolumenMin flow synthesis
// (output disabled for UI_16 type)

  
// MinimumVolumeMax flow acquisition
// (input disabled for UI_16 type)
// MinimumVolumeMax flow synthesis
// (output disabled for UI_16 type)

  
// MinimumVolumeSetpoint flow acquisition
// (input disabled for UI_16 type)
// MinimumVolumeSetpoint flow synthesis
// (output disabled for UI_16 type)

  
// MinimumVolumeMin flow acquisition
// (input disabled for UI_16 type)
// MinimumVolumeMin flow synthesis
// (output disabled for UI_16 type)

  
// TriggerSetpoint flow acquisition
// (input disabled for UI_16 type)
// TriggerSetpoint flow synthesis
// (output disabled for UI_16 type)

  
// RecruitmentTimer flow acquisition
// (input disabled for TimeUI_16 type)
// RecruitmentTimer flow synthesis
// (output disabled for TimeUI_16 type)

  
// RecruitmentElap flow acquisition
// (input disabled for TimeUI_16 type)
// RecruitmentElap flow synthesis
// (output disabled for TimeUI_16 type)

  
// TimeStart flow acquisition
// (input disabled for DateTime type)
// TimeStart flow synthesis
// (output disabled for DateTime type)

  
// TimeEnd flow acquisition
// (input disabled for DateTime type)
// TimeEnd flow synthesis
// (output disabled for DateTime type)

  
// FlowSetpoint flow acquisition
// (input disabled for UI_16 type)
// FlowSetpoint flow synthesis
// (output disabled for UI_16 type)

  
// PIDOutReal flow acquisition
// (input disabled for FL_16 type)
// PIDOutReal flow synthesis
// (output disabled for FL_16 type)

  
// PIDOutInt flow acquisition
// (input disabled for UI_16 type)
// PIDOutInt flow synthesis
// (output disabled for UI_16 type)

  
// PIDOutPWM flow acquisition
// (input disabled for UI_16 type)
// PIDOutPWM flow synthesis
// (output disabled for UI_16 type)

  
// PIDOutMotor flow acquisition
// (input disabled for UI_16 type)
// PIDOutMotor flow synthesis
// (output disabled for UI_16 type)

  
// PeakSetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type)
// PeakSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// PeakSetpointAlarmHI flow acquisition
// (input disabled for UI_16 type)
// PeakSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// PeakSetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// PeakSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// PeakSetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// PeakSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// PeakSetpointHysteresisHI flow acquisition
// (input disabled for UI_16 type)
// PeakSetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type)

  
// PeakSetpointHysteresisLOW flow acquisition
// (input disabled for UI_16 type)
// PeakSetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type)

  
// PlateauSetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type)
// PlateauSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// PlateauSetpointAlarmHI flow acquisition
// (input disabled for UI_16 type)
// PlateauSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// PlateauSetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// PlateauSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// PlateaukSetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// PlateaukSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// PlateaukSetpointHysteresisHI flow acquisition
// (input disabled for UI_16 type)
// PlateaukSetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type)

  
// PlateaukSetpointHysteresisLOW flow acquisition
// (input disabled for UI_16 type)
// PlateaukSetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type)

  
// PeepSetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type)
// PeepSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// PeepSetpointAlarmHI flow acquisition
// (input disabled for UI_16 type)
// PeepSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// PeepSetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// PeepSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// PeepSetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// PeepSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// PeepSetpointHysteresisHI flow acquisition
// (input disabled for UI_16 type)
// PeepSetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type)

  
// PeepSetpointHysteresisLOW flow acquisition
// (input disabled for UI_16 type)
// PeepSetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type)

  
// FrequencySetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type)
// FrequencySetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// FrequencySetpointAlarmHI flow acquisition
// (input disabled for UI_16 type)
// FrequencySetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// FrequencySetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// FrequencySetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// FrequencySetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// FrequencySetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// FrequencySetpointHysteresisHI flow acquisition
// (input disabled for UI_16 type)
// FrequencySetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type)

  
// FrequencySetpointHysteresisLOW flow acquisition
// (input disabled for UI_16 type)
// FrequencySetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumeSetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type)
// TidalVolumeSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumeSetpointAlarmHI flow acquisition
// (input disabled for UI_16 type)
// TidalVolumeSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumeSetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// TidalVolumeSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumeSetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// TidalVolumeSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumeHystersisHI flow acquisition
// (input disabled for UI_16 type)
// TidalVolumeHystersisHI flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumeHystersisLOW flow acquisition
// (input disabled for UI_16 type)
// TidalVolumeHystersisLOW flow synthesis
// (output disabled for UI_16 type)

  
// MinuteVolumeSetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type)
// MinuteVolumeSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// MinuteVolumeSetpointAlarmHI flow acquisition
// (input disabled for UI_16 type)
// MinuteVolumeSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// MinuteVolumeSetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// MinuteVolumeSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// MinuteVolumeSetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// MinuteVolumeSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// MinuteVolumeHysteresisHI flow acquisition
// (input disabled for UI_16 type)
// MinuteVolumeHysteresisHI flow synthesis
// (output disabled for UI_16 type)

  
// MinuteVolumeHysteresisLOW flow acquisition
// (input disabled for UI_16 type)
// MinuteVolumeHysteresisLOW flow synthesis
// (output disabled for UI_16 type)

  
// TriggeVolumeSetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type)
// TriggeVolumeSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// TriggerVolumeSetpointAlarmHI flow acquisition
// (input disabled for UI_16 type)
// TriggerVolumeSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// TriggerVolumeSetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// TriggerVolumeSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// TriggerVolumeSetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// TriggerVolumeSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// TriggerVolumeHysteresisHI flow acquisition
// (input disabled for UI_16 type)
// TriggerVolumeHysteresisHI flow synthesis
// (output disabled for UI_16 type)

  
// TriggerVolumeHysteresisLOW flow acquisition
// (input disabled for UI_16 type)
// TriggerVolumeHysteresisLOW flow synthesis
// (output disabled for UI_16 type)

  
// BatterySetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type)
// BatterySetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// BatterySetpointAlarmHI flow acquisition
// (input disabled for UI_16 type)
// BatterySetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// BatterySetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// BatterySetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// BatterySetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// BatterySetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// BatterySetpointHysteresisHI flow acquisition
// (input disabled for UI_16 type)
// BatterySetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type)

  
// BatterySetpointHysteresisLOW flow acquisition
// (input disabled for UI_16 type)
// BatterySetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type)

  
// Frecuency flow acquisition
// (input disabled for UI_16 type)
// Frecuency flow synthesis
// (output disabled for UI_16 type)

  
// Duty_Cycle flow acquisition
// (input disabled for UI_16 type)
// Duty_Cycle flow synthesis
// (output disabled for UI_16 type)

  
// HOME_Position flow acquisition
// (input disabled for UI_16 type)
// HOME_Position flow synthesis
// (output disabled for UI_16 type)

  
// ABSOLUTE_Position flow acquisition
// (input disabled for UI_16 type)
// ABSOLUTE_Position flow synthesis
// (output disabled for UI_16 type)

  
// ABSOLUTE_Velocity flow acquisition
// (input disabled for UI_16 type)
// ABSOLUTE_Velocity flow synthesis
// (output disabled for UI_16 type)

  
// RELATIVE_Distance flow acquisition
// (input disabled for UI_16 type)
// RELATIVE_Distance flow synthesis
// (output disabled for UI_16 type)

  
// RELATIVE_Velocity flow acquisition
// (input disabled for UI_16 type)
// RELATIVE_Velocity flow synthesis
// (output disabled for UI_16 type)

  
// Velocity_Velocity flow acquisition
// (input disabled for UI_16 type)
// Velocity_Velocity flow synthesis
// (output disabled for UI_16 type)

  
// JOG_Velocity flow acquisition
// (input disabled for UI_16 type)
// JOG_Velocity flow synthesis
// (output disabled for UI_16 type)

  
// EmergencyStop flow acquisition
BOOL acquire_EmergencyStop(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_EmergencyStop==TRUE) { 
return diag.EmergencyStop; 
} else { 
#endif 
return digitalRead(PORT_EmergencyStop); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};

// EmergencyStop flow synthesis
// (output disabled for DI type)

  
// TurnAxisHwHi flow acquisition
BOOL acquire_TurnAxisHwHi(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_TurnAxisHwHi==TRUE) { 
return diag.TurnAxisHwHi; 
} else { 
#endif 
return digitalRead(PORT_TurnAxisHwHi); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};
// TurnAxisHwHi flow synthesis
// (output disabled for DI_pu type)

  
// BeaconLightRed flow acquisition
// (input disabled for DO type)
// BeaconLightRed flow synthesis
void synthesize_BeaconLightRed(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_BeaconLightRed==TRUE) { 
digitalWrite(PORT_BeaconLightRed,diag.BeaconLightRed); 
} else { 
#endif 
digitalWrite(PORT_BeaconLightRed,dre.BeaconLightRed); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};

  
// BeaconLightOrange flow acquisition
// (input disabled for DO type)
// BeaconLightOrange flow synthesis
void synthesize_BeaconLightOrange(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_BeaconLightOrange==TRUE) { 
digitalWrite(PORT_BeaconLightOrange,diag.BeaconLightOrange); 
} else { 
#endif 
digitalWrite(PORT_BeaconLightOrange,dre.BeaconLightOrange); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};

  
// TurnAxisPulse flow acquisition
// (input disabled for DO type)
// TurnAxisPulse flow synthesis
void synthesize_TurnAxisPulse(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_TurnAxisPulse==TRUE) { 
digitalWrite(PORT_TurnAxisPulse,diag.TurnAxisPulse); 
} else { 
#endif 
digitalWrite(PORT_TurnAxisPulse,dre.TurnAxisPulse); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};

  
// TurnAxisDir flow acquisition
// (input disabled for DO type)
// TurnAxisDir flow synthesis
void synthesize_TurnAxisDir(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_TurnAxisDir==TRUE) { 
digitalWrite(PORT_TurnAxisDir,diag.TurnAxisDir); 
} else { 
#endif 
digitalWrite(PORT_TurnAxisDir,dre.TurnAxisDir); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};

  
// TurnAxisEnable flow acquisition
// (input disabled for DO type)
// TurnAxisEnable flow synthesis
void synthesize_TurnAxisEnable(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_TurnAxisEnable==TRUE) { 
digitalWrite(PORT_TurnAxisEnable,diag.TurnAxisEnable); 
} else { 
#endif 
digitalWrite(PORT_TurnAxisEnable,dre.TurnAxisEnable); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};

  
// BeaconLightGreen flow acquisition
// (input disabled for DO type)
// BeaconLightGreen flow synthesis
void synthesize_BeaconLightGreen(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_BeaconLightGreen==TRUE) { 
digitalWrite(PORT_BeaconLightGreen,diag.BeaconLightGreen); 
} else { 
#endif 
digitalWrite(PORT_BeaconLightGreen,dre.BeaconLightGreen); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};

  
// Buzzer flow acquisition
// (input disabled for DO type)
// Buzzer flow synthesis
void synthesize_Buzzer(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_Buzzer==TRUE) { 
digitalWrite(PORT_Buzzer,diag.Buzzer); 
} else { 
#endif 
digitalWrite(PORT_Buzzer,dre.Buzzer); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};

  
// PeepValve flow acquisition
// (input disabled for DO type)
// PeepValve flow synthesis
void synthesize_PeepValve(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_PeepValve==TRUE) { 
digitalWrite(PORT_PeepValve,diag.PeepValve); 
} else { 
#endif 
digitalWrite(PORT_PeepValve,dre.PeepValve); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};

  
// TurnAxisHwLow flow acquisition
BOOL acquire_TurnAxisHwLow(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_TurnAxisHwLow==TRUE) { 
return diag.TurnAxisHwLow; 
} else { 
#endif 
return digitalRead(PORT_TurnAxisHwLow); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};
// TurnAxisHwLow flow synthesis
// (output disabled for DI_pu type)

  
// ServoPend flow acquisition
BOOL acquire_ServoPend(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_ServoPend==TRUE) { 
return diag.ServoPend; 
} else { 
#endif 
return digitalRead(PORT_ServoPend); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};

// ServoPend flow synthesis
// (output disabled for DI type)

  
// ServoAlarm flow acquisition
BOOL acquire_ServoAlarm(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_ServoAlarm==TRUE) { 
return diag.ServoAlarm; 
} else { 
#endif 
return digitalRead(PORT_ServoAlarm); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};
// ServoAlarm flow synthesis
// (output disabled for DI_pu type)

  
// AtmosfericPressure flow acquisition
uint16_t acquire_AtmosfericPressure(void){ 
return analogRead(PORT_AtmosfericPressure); 
};
// AtmosfericPressure flow synthesis
// (output disabled for ADC type)

  
// DiferentialPressure flow acquisition
uint16_t acquire_DiferentialPressure(void){ 
return analogRead(PORT_DiferentialPressure); 
};
// DiferentialPressure flow synthesis
// (output disabled for ADC type)

  
// SerTX flow acquisition
// (input disabled for Bus type)
// SerTX flow synthesis
// (output disabled for Bus type)

  
// SerRX flow acquisition
// (input disabled for Bus type)
// SerRX flow synthesis
// (output disabled for Bus type)

  
// EmgcyButton flow acquisition
// (input disabled for TBD type)
// EmgcyButton flow synthesis
// (output disabled for TBD type)

  
// ApiVersion  flow acquisition
// (input disabled for UI_8 type)
// ApiVersion  flow synthesis
// (output disabled for UI_8 type)

  
// MachineUuid flow acquisition
// (input disabled for Variable type)
// MachineUuid flow synthesis
// (output disabled for Variable type)

  
// FirmwareVersion flow acquisition
// (input disabled for UI_16 type)
// FirmwareVersion flow synthesis
// (output disabled for UI_16 type)

  
// Uptime15mCounter flow acquisition
// (input disabled for UI_16 type)
// Uptime15mCounter flow synthesis
// (output disabled for UI_16 type)

  
// MutedAlarmSeconds flow acquisition
// (input disabled for UI_8 type)
// MutedAlarmSeconds flow synthesis
// (output disabled for UI_8 type)

  
// CycleRpmLast30s flow acquisition
// (input disabled for UI_16 type)
// CycleRpmLast30s flow synthesis
// (output disabled for UI_16 type)

  
// CycleIndications flow acquisition
// (input disabled for Variable type)
// CycleIndications flow synthesis
// (output disabled for Variable type)

  
// CycleDuration flow acquisition
// (input disabled for TimeUI_16 type)
// CycleDuration flow synthesis
// (output disabled for TimeUI_16 type)

  
// VentilationFlags flow acquisition
// (input disabled for Variable type)
// VentilationFlags flow synthesis
// (output disabled for Variable type)

  
// VolumeSetting flow acquisition
// (input disabled for UI_16 type)
// VolumeSetting flow synthesis
// (output disabled for UI_16 type)

  
// RpmSetting flow acquisition
// (input disabled for UI_16 type)
// RpmSetting flow synthesis
// (output disabled for UI_16 type)

  
// RampDegreesSetting flow acquisition
// (input disabled for UI_8 type)
// RampDegreesSetting flow synthesis
// (output disabled for UI_8 type)

  
// EiRatioSetting flow acquisition
// (input disabled for UI_8 type)
// EiRatioSetting flow synthesis
// (output disabled for UI_8 type)

  
// RecruitTimer flow acquisition
// (input disabled for UI_8 type)
// RecruitTimer flow synthesis
// (output disabled for UI_8 type)

  
// IncomingFrame flow acquisition
// (input disabled for Variable type)
// IncomingFrame flow synthesis
// (output disabled for Variable type)

  
// OutgoingFrame flow acquisition
// (input disabled for Variable type)
// OutgoingFrame flow synthesis
// (output disabled for Variable type)