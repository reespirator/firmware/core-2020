#ifndef __DRE_H
#define __DRE_H

#include "core_cfg.h"
#include "core_types.h"


typedef struct {
// LED -- Does not need declaration STATUS_LED;
BOOL DO_STATUS_LED;
t_dbgledblinktimer dbgLedBlinkTimer;
t_dbg_led DBG_LED;
// Bus -- Does not need declaration SDA;
// Bus -- Does not need declaration SCL;
BOOL ResetRelay;
BOOL LCD_DB7;
BOOL LCD_DB6;
BOOL LCD_DB5;
BOOL LCD_DB4;
BOOL LCD_DB3;
BOOL LCD_DB2;
BOOL LCD_DB1;
BOOL LCD_DB0;
// Bus -- Does not need declaration BME_CS2;
// Bus -- Does not need declaration BME_MISO;
// Bus -- Does not need declaration BME_MOSI;
// Bus -- Does not need declaration BME_SCK;
// Bus -- Does not need declaration BME_CS1;
// Power -- Does not need declaration _5V;
// Power -- Does not need declaration GND;
t_int_fs INT_FS;
t_int_fc INT_FC;
t_int_hs INT_HS;
t_int_hc INT_HC;
t_systemdate_w SystemDate_W;
t_systemtime_w SystemTime_W;
t_peakfactor PeakFactor;
t_plateaufactor PlateauFactor;
t_peepfactor PeepFactor;
t_frequencyfactor FrequencyFactor;
t_tidalvolumenfactor TidalVolumenFactor;
t_minimumvolumefactor MinimumVolumeFactor;
t_triggerfactor TriggerFactor;
t_batteryleveltrigger BatteryLevelTrigger;
t_patienttype PatientType;
t_patientweight PatientWeight;
t_patientheight PatientHeight;
t_tidalvolume TidalVolume;
t_pidmode PIDMode;
t_pidstate PIDState;
t_piderrorbits PIDErrorBits;
t_alarms Alarms;
t_status Status;
t_power_startmode POWER_StartMode;
t_power_stopmode POWER_StopMode;
t_home_mode HOME_Mode;
BOOL SetWriteData;
BOOL SetTime;
BOOL Trigger;
BOOL PeakPBPlus;
BOOL PeakPBMinus;
BOOL PeepPBPlus;
BOOL PeepPBMinus;
BOOL FrPBPlus;
BOOL FrPBMinus;
BOOL RecruitmentOn;
BOOL RecruitmentOff;
BOOL RecruitmentMem;
BOOL RecruitmentRunning;
BOOL RecruitmentEnding;
BOOL RecruitmentEnd;
BOOL ErrorAck;
BOOL Reset;
BOOL PIDError;
BOOL PeakDisplayPreAlarmHI;
BOOL PeakDisplayAlarmHI;
BOOL PeakDisplayPreAlarmLOW;
BOOL PeakDisplayAlarmLOW;
BOOL PeakInterlockPreAlarmHI;
BOOL PeakInterlockAlarmHI;
BOOL PeakInterlockPreAlarmLOW;
BOOL PeakInterlockAlarmLOW;
BOOL PlateauDisplayPreAlarmHI;
BOOL PlateauDisplayAlarmHI;
BOOL PlateauDisplayPreAlarmLOW;
BOOL PlateauDisplayAlarmLOW;
BOOL PlateauInterlockPreAlarmHI;
BOOL PlateauInterlockAlarmHI;
BOOL PlateauInterlockPreAlarmLOW;
BOOL PlateauInterlockAlarmLOW;
BOOL PeepDisplayPreAlarmHI;
BOOL PeepDisplayAlarmHI;
BOOL PeepDisplayPreAlarmLOW;
BOOL PeepDisplayAlarmLOW;
BOOL PeepInterlockPreAlarmHI;
BOOL PeepInterlockAlarmHI;
BOOL PeepInterlockPreAlarmLOW;
BOOL PeepInterlockAlarmLOW;
BOOL FrequencyDisplayPreAlarmHI;
BOOL FrequencyDisplayAlarmHI;
BOOL FrequencyDisplayPreAlarmLOW;
BOOL FrequencyDisplayAlarmLOW;
BOOL FrequencyInterlockPreAlarmHI;
BOOL FrequencyInterlockAlarmHI;
BOOL FrequencyInterlockPreAlarmLOW;
BOOL FrequencyInterlockAlarmLOW;
BOOL TidalVolumeDisplayPreAlarmHI;
BOOL TidalVolumeDisplayAlarmHI;
BOOL TidalVolumeDisplayPreAlarmLOW;
BOOL TidalVolumeDisplayAlarmLOW;
BOOL TidalVolumeInterlockPreAlarmHI;
BOOL TidalVolumeInterlockAlarmHI;
BOOL TidalVolumeInterlockPreAlarmLOW;
BOOL TidalVolumeInterlockAlarmLOW;
BOOL MinuteVolumeDisplayPreAlarmHI;
BOOL MinuteVolumeDisplayAlarmHI;
BOOL MinuteVolumeDisplayPreAlarmLOW;
BOOL MinuteVolumeDisplayAlarmLOW;
BOOL MinuteVolumeInterlockPreAlarmHI;
BOOL MinuteVolumeInterlockAlarmHI;
BOOL MinuteVolumeInterlockPreAlarmLOW;
BOOL MinuteVolumeInterlockAlarmLOW;
BOOL TriggerDisplayPreAlarmHI;
BOOL TriggerDisplayAlarmHI;
BOOL TriggerDisplayPreAlarmLOW;
BOOL TriggerDisplayAlarmLOW;
BOOL TriggerInterlockPreAlarmHI;
BOOL TriggerInterlockAlarmHI;
BOOL TriggerInterlockPreAlarmLOW;
BOOL TriggerInterlockAlarmLOW;
BOOL BatteryDisplayPreAlarmHI;
BOOL BatteryDisplayAlarmHI;
BOOL BatteryDisplayPreAlarmLOW;
BOOL BatteryDisplayAlarmLOW;
BOOL BatteryInterlockPreAlarmHI;
BOOL BatteryInterlockAlarmHI;
BOOL BatteryInterlockPreAlarmLOW;
BOOL BatteryInterlockAlarmLOW;
BOOL Enable;
BOOL Busy;
BOOL POWER_Enable;
BOOL POWER_Status;
BOOL POWER_Error;
BOOL RESET_Execute;
BOOL RESET_Done;
BOOL RESET_Error;
BOOL HOME_Execute;
BOOL HOME_Done;
BOOL HOME_Error;
BOOL HALT_Execute;
BOOL HALT_Done;
BOOL HALT_Error;
BOOL ABSOLUTE_Execute;
BOOL ABSOLUTE_Done;
BOOL ABSOLUTE_Error;
BOOL RELATIVE_Execute;
BOOL RELATIVE_Done;
BOOL RELATIVE_Error;
BOOL Velocity_Execute;
BOOL Velocity_Current;
BOOL Velocity_invelocity;
BOOL Velocity_Error;
BOOL JOG_Forward;
BOOL JOG_Backward;
BOOL JOG_Invelocity;
BOOL JOG_Error;
BOOL Permissions;
BOOL Operation_ON;
BOOL Emergency;
BOOL GeneralAck;
BOOL MotorAck;
BOOL ProcessAck;
BOOL GeneralReset;
BOOL MotorReset;
BOOL ProcessReset;
UI_16 PeakValue;
UI_16 PlateauValue;
UI_16 PeepValue;
UI_16 FrequencyValue;
UI_16 TidalVolumeValue;
UI_16 MinumValue;
UI_16 DrivingPreassure;
UI_16 BatteryVoltage;
UI_16 BatteryCapacity;
UI_16 Pressure1;
UI_16 Pressure2;
UI_16 VomumeAccumulated;
UI_16 Flow;
UI_16 SystemDateRetVal;
t_datetime SystemDateOut;
UI_16 LocalDateRetVal;
t_datetime LocalDateOut;
t_datetime WriteDateTime;
t_datetime WriteDateOut;
t_datetime StatuSetTime;
UI_16 PeakMax;
UI_16 PeakSetpoint;
UI_16 PeakMin;
UI_16 PlateauMax;
UI_16 PlateauSetpoint;
UI_16 PlateauMin;
UI_16 PeepMax;
UI_16 PeepSetpoint;
UI_16 PeepMin;
UI_16 FrequencyMax;
UI_16 FrequencySetpoint;
UI_16 FrequencyMin;
UI_16 TidalVolumenMax;
UI_16 TidalVolumenSetpoint;
UI_16 TidalVolumenMin;
UI_16 MinimumVolumeMax;
UI_16 MinimumVolumeSetpoint;
UI_16 MinimumVolumeMin;
UI_16 TriggerSetpoint;
UI_16 RecruitmentTimer;
UI_16 RecruitmentElap;
t_datetime TimeStart;
t_datetime TimeEnd;
UI_16 FlowSetpoint;
FL_16 PIDOutReal;
UI_16 PIDOutInt;
UI_16 PIDOutPWM;
UI_16 PIDOutMotor;
UI_16 PeakSetpointPreAlarmHI;
UI_16 PeakSetpointAlarmHI;
UI_16 PeakSetpointPreAlarmLOW;
UI_16 PeakSetpointAlarmLOW;
UI_16 PeakSetpointHysteresisHI;
UI_16 PeakSetpointHysteresisLOW;
UI_16 PlateauSetpointPreAlarmHI;
UI_16 PlateauSetpointAlarmHI;
UI_16 PlateauSetpointPreAlarmLOW;
UI_16 PlateaukSetpointAlarmLOW;
UI_16 PlateaukSetpointHysteresisHI;
UI_16 PlateaukSetpointHysteresisLOW;
UI_16 PeepSetpointPreAlarmHI;
UI_16 PeepSetpointAlarmHI;
UI_16 PeepSetpointPreAlarmLOW;
UI_16 PeepSetpointAlarmLOW;
UI_16 PeepSetpointHysteresisHI;
UI_16 PeepSetpointHysteresisLOW;
UI_16 FrequencySetpointPreAlarmHI;
UI_16 FrequencySetpointAlarmHI;
UI_16 FrequencySetpointPreAlarmLOW;
UI_16 FrequencySetpointAlarmLOW;
UI_16 FrequencySetpointHysteresisHI;
UI_16 FrequencySetpointHysteresisLOW;
UI_16 TidalVolumeSetpointPreAlarmHI;
UI_16 TidalVolumeSetpointAlarmHI;
UI_16 TidalVolumeSetpointPreAlarmLOW;
UI_16 TidalVolumeSetpointAlarmLOW;
UI_16 TidalVolumeHystersisHI;
UI_16 TidalVolumeHystersisLOW;
UI_16 MinuteVolumeSetpointPreAlarmHI;
UI_16 MinuteVolumeSetpointAlarmHI;
UI_16 MinuteVolumeSetpointPreAlarmLOW;
UI_16 MinuteVolumeSetpointAlarmLOW;
UI_16 MinuteVolumeHysteresisHI;
UI_16 MinuteVolumeHysteresisLOW;
UI_16 TriggeVolumeSetpointPreAlarmHI;
UI_16 TriggerVolumeSetpointAlarmHI;
UI_16 TriggerVolumeSetpointPreAlarmLOW;
UI_16 TriggerVolumeSetpointAlarmLOW;
UI_16 TriggerVolumeHysteresisHI;
UI_16 TriggerVolumeHysteresisLOW;
UI_16 BatterySetpointPreAlarmHI;
UI_16 BatterySetpointAlarmHI;
UI_16 BatterySetpointPreAlarmLOW;
UI_16 BatterySetpointAlarmLOW;
UI_16 BatterySetpointHysteresisHI;
UI_16 BatterySetpointHysteresisLOW;
UI_16 Frecuency;
UI_16 Duty_Cycle;
UI_16 HOME_Position;
UI_16 ABSOLUTE_Position;
UI_16 ABSOLUTE_Velocity;
UI_16 RELATIVE_Distance;
UI_16 RELATIVE_Velocity;
UI_16 Velocity_Velocity;
UI_16 JOG_Velocity;
BOOL EmergencyStop;
BOOL TurnAxisHwHi;
BOOL BeaconLightRed;
BOOL BeaconLightOrange;
BOOL TurnAxisPulse;
BOOL TurnAxisDir;
BOOL TurnAxisEnable;
BOOL BeaconLightGreen;
BOOL Buzzer;
BOOL PeepValve;
BOOL TurnAxisHwLow;
BOOL ServoPend;
BOOL ServoAlarm;
uint16_t AtmosfericPressure;
uint16_t DiferentialPressure;
// Bus -- Does not need declaration SerTX;
// Bus -- Does not need declaration SerRX;
// TBD -- Does not need declaration EmgcyButton;
UI_8 ApiVersion ;
t_machineuuid MachineUuid;
UI_16 FirmwareVersion;
UI_16 Uptime15mCounter;
UI_8 MutedAlarmSeconds;
UI_16 CycleRpmLast30s;
t_cycleindications CycleIndications;
UI_16 CycleDuration;
t_ventilationflags VentilationFlags;
UI_16 VolumeSetting;
UI_16 RpmSetting;
UI_8 RampDegreesSetting;
UI_8 EiRatioSetting;
UI_8 RecruitTimer;
t_incomingframe IncomingFrame;
t_outgoingframe OutgoingFrame;

} t_dre;

typedef struct {
// (null) No diag variables for STATUS_LED
BOOL enable_DO_STATUS_LED;
BOOL DO_STATUS_LED;
BOOL enable_dbgLedBlinkTimer;
t_dbgledblinktimer dbgLedBlinkTimer;
BOOL enable_DBG_LED;
t_dbg_led DBG_LED;
// (null) No diag variables for SDA
// (null) No diag variables for SCL
BOOL enable_ResetRelay;
BOOL ResetRelay;
BOOL enable_LCD_DB7;
BOOL LCD_DB7;
BOOL enable_LCD_DB6;
BOOL LCD_DB6;
BOOL enable_LCD_DB5;
BOOL LCD_DB5;
BOOL enable_LCD_DB4;
BOOL LCD_DB4;
BOOL enable_LCD_DB3;
BOOL LCD_DB3;
BOOL enable_LCD_DB2;
BOOL LCD_DB2;
BOOL enable_LCD_DB1;
BOOL LCD_DB1;
BOOL enable_LCD_DB0;
BOOL LCD_DB0;
// (null) No diag variables for BME_CS2
// (null) No diag variables for BME_MISO
// (null) No diag variables for BME_MOSI
// (null) No diag variables for BME_SCK
// (null) No diag variables for BME_CS1
// (null) No diag variables for _5V
// (null) No diag variables for GND
BOOL enable_INT_FS;
t_int_fs INT_FS;
BOOL enable_INT_FC;
t_int_fc INT_FC;
BOOL enable_INT_HS;
t_int_hs INT_HS;
BOOL enable_INT_HC;
t_int_hc INT_HC;
BOOL enable_SystemDate_W;
t_systemdate_w SystemDate_W;
BOOL enable_SystemTime_W;
t_systemtime_w SystemTime_W;
BOOL enable_PeakFactor;
t_peakfactor PeakFactor;
BOOL enable_PlateauFactor;
t_plateaufactor PlateauFactor;
BOOL enable_PeepFactor;
t_peepfactor PeepFactor;
BOOL enable_FrequencyFactor;
t_frequencyfactor FrequencyFactor;
BOOL enable_TidalVolumenFactor;
t_tidalvolumenfactor TidalVolumenFactor;
BOOL enable_MinimumVolumeFactor;
t_minimumvolumefactor MinimumVolumeFactor;
BOOL enable_TriggerFactor;
t_triggerfactor TriggerFactor;
BOOL enable_BatteryLevelTrigger;
t_batteryleveltrigger BatteryLevelTrigger;
BOOL enable_PatientType;
t_patienttype PatientType;
BOOL enable_PatientWeight;
t_patientweight PatientWeight;
BOOL enable_PatientHeight;
t_patientheight PatientHeight;
BOOL enable_TidalVolume;
t_tidalvolume TidalVolume;
BOOL enable_PIDMode;
t_pidmode PIDMode;
BOOL enable_PIDState;
t_pidstate PIDState;
BOOL enable_PIDErrorBits;
t_piderrorbits PIDErrorBits;
BOOL enable_Alarms;
t_alarms Alarms;
BOOL enable_Status;
t_status Status;
BOOL enable_POWER_StartMode;
t_power_startmode POWER_StartMode;
BOOL enable_POWER_StopMode;
t_power_stopmode POWER_StopMode;
BOOL enable_HOME_Mode;
t_home_mode HOME_Mode;
BOOL enable_SetWriteData;
BOOL SetWriteData;
BOOL enable_SetTime;
BOOL SetTime;
BOOL enable_Trigger;
BOOL Trigger;
BOOL enable_PeakPBPlus;
BOOL PeakPBPlus;
BOOL enable_PeakPBMinus;
BOOL PeakPBMinus;
BOOL enable_PeepPBPlus;
BOOL PeepPBPlus;
BOOL enable_PeepPBMinus;
BOOL PeepPBMinus;
BOOL enable_FrPBPlus;
BOOL FrPBPlus;
BOOL enable_FrPBMinus;
BOOL FrPBMinus;
BOOL enable_RecruitmentOn;
BOOL RecruitmentOn;
BOOL enable_RecruitmentOff;
BOOL RecruitmentOff;
BOOL enable_RecruitmentMem;
BOOL RecruitmentMem;
BOOL enable_RecruitmentRunning;
BOOL RecruitmentRunning;
BOOL enable_RecruitmentEnding;
BOOL RecruitmentEnding;
BOOL enable_RecruitmentEnd;
BOOL RecruitmentEnd;
BOOL enable_ErrorAck;
BOOL ErrorAck;
BOOL enable_Reset;
BOOL Reset;
BOOL enable_PIDError;
BOOL PIDError;
BOOL enable_PeakDisplayPreAlarmHI;
BOOL PeakDisplayPreAlarmHI;
BOOL enable_PeakDisplayAlarmHI;
BOOL PeakDisplayAlarmHI;
BOOL enable_PeakDisplayPreAlarmLOW;
BOOL PeakDisplayPreAlarmLOW;
BOOL enable_PeakDisplayAlarmLOW;
BOOL PeakDisplayAlarmLOW;
BOOL enable_PeakInterlockPreAlarmHI;
BOOL PeakInterlockPreAlarmHI;
BOOL enable_PeakInterlockAlarmHI;
BOOL PeakInterlockAlarmHI;
BOOL enable_PeakInterlockPreAlarmLOW;
BOOL PeakInterlockPreAlarmLOW;
BOOL enable_PeakInterlockAlarmLOW;
BOOL PeakInterlockAlarmLOW;
BOOL enable_PlateauDisplayPreAlarmHI;
BOOL PlateauDisplayPreAlarmHI;
BOOL enable_PlateauDisplayAlarmHI;
BOOL PlateauDisplayAlarmHI;
BOOL enable_PlateauDisplayPreAlarmLOW;
BOOL PlateauDisplayPreAlarmLOW;
BOOL enable_PlateauDisplayAlarmLOW;
BOOL PlateauDisplayAlarmLOW;
BOOL enable_PlateauInterlockPreAlarmHI;
BOOL PlateauInterlockPreAlarmHI;
BOOL enable_PlateauInterlockAlarmHI;
BOOL PlateauInterlockAlarmHI;
BOOL enable_PlateauInterlockPreAlarmLOW;
BOOL PlateauInterlockPreAlarmLOW;
BOOL enable_PlateauInterlockAlarmLOW;
BOOL PlateauInterlockAlarmLOW;
BOOL enable_PeepDisplayPreAlarmHI;
BOOL PeepDisplayPreAlarmHI;
BOOL enable_PeepDisplayAlarmHI;
BOOL PeepDisplayAlarmHI;
BOOL enable_PeepDisplayPreAlarmLOW;
BOOL PeepDisplayPreAlarmLOW;
BOOL enable_PeepDisplayAlarmLOW;
BOOL PeepDisplayAlarmLOW;
BOOL enable_PeepInterlockPreAlarmHI;
BOOL PeepInterlockPreAlarmHI;
BOOL enable_PeepInterlockAlarmHI;
BOOL PeepInterlockAlarmHI;
BOOL enable_PeepInterlockPreAlarmLOW;
BOOL PeepInterlockPreAlarmLOW;
BOOL enable_PeepInterlockAlarmLOW;
BOOL PeepInterlockAlarmLOW;
BOOL enable_FrequencyDisplayPreAlarmHI;
BOOL FrequencyDisplayPreAlarmHI;
BOOL enable_FrequencyDisplayAlarmHI;
BOOL FrequencyDisplayAlarmHI;
BOOL enable_FrequencyDisplayPreAlarmLOW;
BOOL FrequencyDisplayPreAlarmLOW;
BOOL enable_FrequencyDisplayAlarmLOW;
BOOL FrequencyDisplayAlarmLOW;
BOOL enable_FrequencyInterlockPreAlarmHI;
BOOL FrequencyInterlockPreAlarmHI;
BOOL enable_FrequencyInterlockAlarmHI;
BOOL FrequencyInterlockAlarmHI;
BOOL enable_FrequencyInterlockPreAlarmLOW;
BOOL FrequencyInterlockPreAlarmLOW;
BOOL enable_FrequencyInterlockAlarmLOW;
BOOL FrequencyInterlockAlarmLOW;
BOOL enable_TidalVolumeDisplayPreAlarmHI;
BOOL TidalVolumeDisplayPreAlarmHI;
BOOL enable_TidalVolumeDisplayAlarmHI;
BOOL TidalVolumeDisplayAlarmHI;
BOOL enable_TidalVolumeDisplayPreAlarmLOW;
BOOL TidalVolumeDisplayPreAlarmLOW;
BOOL enable_TidalVolumeDisplayAlarmLOW;
BOOL TidalVolumeDisplayAlarmLOW;
BOOL enable_TidalVolumeInterlockPreAlarmHI;
BOOL TidalVolumeInterlockPreAlarmHI;
BOOL enable_TidalVolumeInterlockAlarmHI;
BOOL TidalVolumeInterlockAlarmHI;
BOOL enable_TidalVolumeInterlockPreAlarmLOW;
BOOL TidalVolumeInterlockPreAlarmLOW;
BOOL enable_TidalVolumeInterlockAlarmLOW;
BOOL TidalVolumeInterlockAlarmLOW;
BOOL enable_MinuteVolumeDisplayPreAlarmHI;
BOOL MinuteVolumeDisplayPreAlarmHI;
BOOL enable_MinuteVolumeDisplayAlarmHI;
BOOL MinuteVolumeDisplayAlarmHI;
BOOL enable_MinuteVolumeDisplayPreAlarmLOW;
BOOL MinuteVolumeDisplayPreAlarmLOW;
BOOL enable_MinuteVolumeDisplayAlarmLOW;
BOOL MinuteVolumeDisplayAlarmLOW;
BOOL enable_MinuteVolumeInterlockPreAlarmHI;
BOOL MinuteVolumeInterlockPreAlarmHI;
BOOL enable_MinuteVolumeInterlockAlarmHI;
BOOL MinuteVolumeInterlockAlarmHI;
BOOL enable_MinuteVolumeInterlockPreAlarmLOW;
BOOL MinuteVolumeInterlockPreAlarmLOW;
BOOL enable_MinuteVolumeInterlockAlarmLOW;
BOOL MinuteVolumeInterlockAlarmLOW;
BOOL enable_TriggerDisplayPreAlarmHI;
BOOL TriggerDisplayPreAlarmHI;
BOOL enable_TriggerDisplayAlarmHI;
BOOL TriggerDisplayAlarmHI;
BOOL enable_TriggerDisplayPreAlarmLOW;
BOOL TriggerDisplayPreAlarmLOW;
BOOL enable_TriggerDisplayAlarmLOW;
BOOL TriggerDisplayAlarmLOW;
BOOL enable_TriggerInterlockPreAlarmHI;
BOOL TriggerInterlockPreAlarmHI;
BOOL enable_TriggerInterlockAlarmHI;
BOOL TriggerInterlockAlarmHI;
BOOL enable_TriggerInterlockPreAlarmLOW;
BOOL TriggerInterlockPreAlarmLOW;
BOOL enable_TriggerInterlockAlarmLOW;
BOOL TriggerInterlockAlarmLOW;
BOOL enable_BatteryDisplayPreAlarmHI;
BOOL BatteryDisplayPreAlarmHI;
BOOL enable_BatteryDisplayAlarmHI;
BOOL BatteryDisplayAlarmHI;
BOOL enable_BatteryDisplayPreAlarmLOW;
BOOL BatteryDisplayPreAlarmLOW;
BOOL enable_BatteryDisplayAlarmLOW;
BOOL BatteryDisplayAlarmLOW;
BOOL enable_BatteryInterlockPreAlarmHI;
BOOL BatteryInterlockPreAlarmHI;
BOOL enable_BatteryInterlockAlarmHI;
BOOL BatteryInterlockAlarmHI;
BOOL enable_BatteryInterlockPreAlarmLOW;
BOOL BatteryInterlockPreAlarmLOW;
BOOL enable_BatteryInterlockAlarmLOW;
BOOL BatteryInterlockAlarmLOW;
BOOL enable_Enable;
BOOL Enable;
BOOL enable_Busy;
BOOL Busy;
BOOL enable_POWER_Enable;
BOOL POWER_Enable;
BOOL enable_POWER_Status;
BOOL POWER_Status;
BOOL enable_POWER_Error;
BOOL POWER_Error;
BOOL enable_RESET_Execute;
BOOL RESET_Execute;
BOOL enable_RESET_Done;
BOOL RESET_Done;
BOOL enable_RESET_Error;
BOOL RESET_Error;
BOOL enable_HOME_Execute;
BOOL HOME_Execute;
BOOL enable_HOME_Done;
BOOL HOME_Done;
BOOL enable_HOME_Error;
BOOL HOME_Error;
BOOL enable_HALT_Execute;
BOOL HALT_Execute;
BOOL enable_HALT_Done;
BOOL HALT_Done;
BOOL enable_HALT_Error;
BOOL HALT_Error;
BOOL enable_ABSOLUTE_Execute;
BOOL ABSOLUTE_Execute;
BOOL enable_ABSOLUTE_Done;
BOOL ABSOLUTE_Done;
BOOL enable_ABSOLUTE_Error;
BOOL ABSOLUTE_Error;
BOOL enable_RELATIVE_Execute;
BOOL RELATIVE_Execute;
BOOL enable_RELATIVE_Done;
BOOL RELATIVE_Done;
BOOL enable_RELATIVE_Error;
BOOL RELATIVE_Error;
BOOL enable_Velocity_Execute;
BOOL Velocity_Execute;
BOOL enable_Velocity_Current;
BOOL Velocity_Current;
BOOL enable_Velocity_invelocity;
BOOL Velocity_invelocity;
BOOL enable_Velocity_Error;
BOOL Velocity_Error;
BOOL enable_JOG_Forward;
BOOL JOG_Forward;
BOOL enable_JOG_Backward;
BOOL JOG_Backward;
BOOL enable_JOG_Invelocity;
BOOL JOG_Invelocity;
BOOL enable_JOG_Error;
BOOL JOG_Error;
BOOL enable_Permissions;
BOOL Permissions;
BOOL enable_Operation_ON;
BOOL Operation_ON;
BOOL enable_Emergency;
BOOL Emergency;
BOOL enable_GeneralAck;
BOOL GeneralAck;
BOOL enable_MotorAck;
BOOL MotorAck;
BOOL enable_ProcessAck;
BOOL ProcessAck;
BOOL enable_GeneralReset;
BOOL GeneralReset;
BOOL enable_MotorReset;
BOOL MotorReset;
BOOL enable_ProcessReset;
BOOL ProcessReset;
BOOL enable_PeakValue;
UI_16 PeakValue;
BOOL enable_PlateauValue;
UI_16 PlateauValue;
BOOL enable_PeepValue;
UI_16 PeepValue;
BOOL enable_FrequencyValue;
UI_16 FrequencyValue;
BOOL enable_TidalVolumeValue;
UI_16 TidalVolumeValue;
BOOL enable_MinumValue;
UI_16 MinumValue;
BOOL enable_DrivingPreassure;
UI_16 DrivingPreassure;
BOOL enable_BatteryVoltage;
UI_16 BatteryVoltage;
BOOL enable_BatteryCapacity;
UI_16 BatteryCapacity;
BOOL enable_Pressure1;
UI_16 Pressure1;
BOOL enable_Pressure2;
UI_16 Pressure2;
BOOL enable_VomumeAccumulated;
UI_16 VomumeAccumulated;
BOOL enable_Flow;
UI_16 Flow;
BOOL enable_SystemDateRetVal;
UI_16 SystemDateRetVal;
BOOL enable_SystemDateOut;
t_datetime SystemDateOut;
BOOL enable_LocalDateRetVal;
UI_16 LocalDateRetVal;
BOOL enable_LocalDateOut;
t_datetime LocalDateOut;
BOOL enable_WriteDateTime;
t_datetime WriteDateTime;
BOOL enable_WriteDateOut;
t_datetime WriteDateOut;
BOOL enable_StatuSetTime;
t_datetime StatuSetTime;
BOOL enable_PeakMax;
UI_16 PeakMax;
BOOL enable_PeakSetpoint;
UI_16 PeakSetpoint;
BOOL enable_PeakMin;
UI_16 PeakMin;
BOOL enable_PlateauMax;
UI_16 PlateauMax;
BOOL enable_PlateauSetpoint;
UI_16 PlateauSetpoint;
BOOL enable_PlateauMin;
UI_16 PlateauMin;
BOOL enable_PeepMax;
UI_16 PeepMax;
BOOL enable_PeepSetpoint;
UI_16 PeepSetpoint;
BOOL enable_PeepMin;
UI_16 PeepMin;
BOOL enable_FrequencyMax;
UI_16 FrequencyMax;
BOOL enable_FrequencySetpoint;
UI_16 FrequencySetpoint;
BOOL enable_FrequencyMin;
UI_16 FrequencyMin;
BOOL enable_TidalVolumenMax;
UI_16 TidalVolumenMax;
BOOL enable_TidalVolumenSetpoint;
UI_16 TidalVolumenSetpoint;
BOOL enable_TidalVolumenMin;
UI_16 TidalVolumenMin;
BOOL enable_MinimumVolumeMax;
UI_16 MinimumVolumeMax;
BOOL enable_MinimumVolumeSetpoint;
UI_16 MinimumVolumeSetpoint;
BOOL enable_MinimumVolumeMin;
UI_16 MinimumVolumeMin;
BOOL enable_TriggerSetpoint;
UI_16 TriggerSetpoint;
BOOL enable_RecruitmentTimer;
UI_16 RecruitmentTimer;
BOOL enable_RecruitmentElap;
UI_16 RecruitmentElap;
BOOL enable_TimeStart;
t_datetime TimeStart;
BOOL enable_TimeEnd;
t_datetime TimeEnd;
BOOL enable_FlowSetpoint;
UI_16 FlowSetpoint;
BOOL enable_PIDOutReal;
FL_16 PIDOutReal;
BOOL enable_PIDOutInt;
UI_16 PIDOutInt;
BOOL enable_PIDOutPWM;
UI_16 PIDOutPWM;
BOOL enable_PIDOutMotor;
UI_16 PIDOutMotor;
BOOL enable_PeakSetpointPreAlarmHI;
UI_16 PeakSetpointPreAlarmHI;
BOOL enable_PeakSetpointAlarmHI;
UI_16 PeakSetpointAlarmHI;
BOOL enable_PeakSetpointPreAlarmLOW;
UI_16 PeakSetpointPreAlarmLOW;
BOOL enable_PeakSetpointAlarmLOW;
UI_16 PeakSetpointAlarmLOW;
BOOL enable_PeakSetpointHysteresisHI;
UI_16 PeakSetpointHysteresisHI;
BOOL enable_PeakSetpointHysteresisLOW;
UI_16 PeakSetpointHysteresisLOW;
BOOL enable_PlateauSetpointPreAlarmHI;
UI_16 PlateauSetpointPreAlarmHI;
BOOL enable_PlateauSetpointAlarmHI;
UI_16 PlateauSetpointAlarmHI;
BOOL enable_PlateauSetpointPreAlarmLOW;
UI_16 PlateauSetpointPreAlarmLOW;
BOOL enable_PlateaukSetpointAlarmLOW;
UI_16 PlateaukSetpointAlarmLOW;
BOOL enable_PlateaukSetpointHysteresisHI;
UI_16 PlateaukSetpointHysteresisHI;
BOOL enable_PlateaukSetpointHysteresisLOW;
UI_16 PlateaukSetpointHysteresisLOW;
BOOL enable_PeepSetpointPreAlarmHI;
UI_16 PeepSetpointPreAlarmHI;
BOOL enable_PeepSetpointAlarmHI;
UI_16 PeepSetpointAlarmHI;
BOOL enable_PeepSetpointPreAlarmLOW;
UI_16 PeepSetpointPreAlarmLOW;
BOOL enable_PeepSetpointAlarmLOW;
UI_16 PeepSetpointAlarmLOW;
BOOL enable_PeepSetpointHysteresisHI;
UI_16 PeepSetpointHysteresisHI;
BOOL enable_PeepSetpointHysteresisLOW;
UI_16 PeepSetpointHysteresisLOW;
BOOL enable_FrequencySetpointPreAlarmHI;
UI_16 FrequencySetpointPreAlarmHI;
BOOL enable_FrequencySetpointAlarmHI;
UI_16 FrequencySetpointAlarmHI;
BOOL enable_FrequencySetpointPreAlarmLOW;
UI_16 FrequencySetpointPreAlarmLOW;
BOOL enable_FrequencySetpointAlarmLOW;
UI_16 FrequencySetpointAlarmLOW;
BOOL enable_FrequencySetpointHysteresisHI;
UI_16 FrequencySetpointHysteresisHI;
BOOL enable_FrequencySetpointHysteresisLOW;
UI_16 FrequencySetpointHysteresisLOW;
BOOL enable_TidalVolumeSetpointPreAlarmHI;
UI_16 TidalVolumeSetpointPreAlarmHI;
BOOL enable_TidalVolumeSetpointAlarmHI;
UI_16 TidalVolumeSetpointAlarmHI;
BOOL enable_TidalVolumeSetpointPreAlarmLOW;
UI_16 TidalVolumeSetpointPreAlarmLOW;
BOOL enable_TidalVolumeSetpointAlarmLOW;
UI_16 TidalVolumeSetpointAlarmLOW;
BOOL enable_TidalVolumeHystersisHI;
UI_16 TidalVolumeHystersisHI;
BOOL enable_TidalVolumeHystersisLOW;
UI_16 TidalVolumeHystersisLOW;
BOOL enable_MinuteVolumeSetpointPreAlarmHI;
UI_16 MinuteVolumeSetpointPreAlarmHI;
BOOL enable_MinuteVolumeSetpointAlarmHI;
UI_16 MinuteVolumeSetpointAlarmHI;
BOOL enable_MinuteVolumeSetpointPreAlarmLOW;
UI_16 MinuteVolumeSetpointPreAlarmLOW;
BOOL enable_MinuteVolumeSetpointAlarmLOW;
UI_16 MinuteVolumeSetpointAlarmLOW;
BOOL enable_MinuteVolumeHysteresisHI;
UI_16 MinuteVolumeHysteresisHI;
BOOL enable_MinuteVolumeHysteresisLOW;
UI_16 MinuteVolumeHysteresisLOW;
BOOL enable_TriggeVolumeSetpointPreAlarmHI;
UI_16 TriggeVolumeSetpointPreAlarmHI;
BOOL enable_TriggerVolumeSetpointAlarmHI;
UI_16 TriggerVolumeSetpointAlarmHI;
BOOL enable_TriggerVolumeSetpointPreAlarmLOW;
UI_16 TriggerVolumeSetpointPreAlarmLOW;
BOOL enable_TriggerVolumeSetpointAlarmLOW;
UI_16 TriggerVolumeSetpointAlarmLOW;
BOOL enable_TriggerVolumeHysteresisHI;
UI_16 TriggerVolumeHysteresisHI;
BOOL enable_TriggerVolumeHysteresisLOW;
UI_16 TriggerVolumeHysteresisLOW;
BOOL enable_BatterySetpointPreAlarmHI;
UI_16 BatterySetpointPreAlarmHI;
BOOL enable_BatterySetpointAlarmHI;
UI_16 BatterySetpointAlarmHI;
BOOL enable_BatterySetpointPreAlarmLOW;
UI_16 BatterySetpointPreAlarmLOW;
BOOL enable_BatterySetpointAlarmLOW;
UI_16 BatterySetpointAlarmLOW;
BOOL enable_BatterySetpointHysteresisHI;
UI_16 BatterySetpointHysteresisHI;
BOOL enable_BatterySetpointHysteresisLOW;
UI_16 BatterySetpointHysteresisLOW;
BOOL enable_Frecuency;
UI_16 Frecuency;
BOOL enable_Duty_Cycle;
UI_16 Duty_Cycle;
BOOL enable_HOME_Position;
UI_16 HOME_Position;
BOOL enable_ABSOLUTE_Position;
UI_16 ABSOLUTE_Position;
BOOL enable_ABSOLUTE_Velocity;
UI_16 ABSOLUTE_Velocity;
BOOL enable_RELATIVE_Distance;
UI_16 RELATIVE_Distance;
BOOL enable_RELATIVE_Velocity;
UI_16 RELATIVE_Velocity;
BOOL enable_Velocity_Velocity;
UI_16 Velocity_Velocity;
BOOL enable_JOG_Velocity;
UI_16 JOG_Velocity;
BOOL enable_EmergencyStop;
BOOL EmergencyStop;
BOOL enable_TurnAxisHwHi;
BOOL TurnAxisHwHi;
BOOL enable_BeaconLightRed;
BOOL BeaconLightRed;
BOOL enable_BeaconLightOrange;
BOOL BeaconLightOrange;
BOOL enable_TurnAxisPulse;
BOOL TurnAxisPulse;
BOOL enable_TurnAxisDir;
BOOL TurnAxisDir;
BOOL enable_TurnAxisEnable;
BOOL TurnAxisEnable;
BOOL enable_BeaconLightGreen;
BOOL BeaconLightGreen;
BOOL enable_Buzzer;
BOOL Buzzer;
BOOL enable_PeepValve;
BOOL PeepValve;
BOOL enable_TurnAxisHwLow;
BOOL TurnAxisHwLow;
BOOL enable_ServoPend;
BOOL ServoPend;
BOOL enable_ServoAlarm;
BOOL ServoAlarm;
BOOL enable_AtmosfericPressure;
uint16_t AtmosfericPressure;
BOOL enable_DiferentialPressure;
uint16_t DiferentialPressure;
// (null) No diag variables for SerTX
// (null) No diag variables for SerRX
// (null) No diag variables for EmgcyButton
BOOL enable_ApiVersion ;
UI_8 ApiVersion ;
BOOL enable_MachineUuid;
t_machineuuid MachineUuid;
BOOL enable_FirmwareVersion;
UI_16 FirmwareVersion;
BOOL enable_Uptime15mCounter;
UI_16 Uptime15mCounter;
BOOL enable_MutedAlarmSeconds;
UI_8 MutedAlarmSeconds;
BOOL enable_CycleRpmLast30s;
UI_16 CycleRpmLast30s;
BOOL enable_CycleIndications;
t_cycleindications CycleIndications;
BOOL enable_CycleDuration;
UI_16 CycleDuration;
BOOL enable_VentilationFlags;
t_ventilationflags VentilationFlags;
BOOL enable_VolumeSetting;
UI_16 VolumeSetting;
BOOL enable_RpmSetting;
UI_16 RpmSetting;
BOOL enable_RampDegreesSetting;
UI_8 RampDegreesSetting;
BOOL enable_EiRatioSetting;
UI_8 EiRatioSetting;
BOOL enable_RecruitTimer;
UI_8 RecruitTimer;
BOOL enable_IncomingFrame;
t_incomingframe IncomingFrame;
BOOL enable_OutgoingFrame;
t_outgoingframe OutgoingFrame;

} t_diag;

// Initialization functions

  
// STATUS_LED flow acquisition
// (setup input disabled for LED type);
// STATUS_LED flow synthesis
// (output disabled for LED type);
  
// DO_STATUS_LED flow acquisition
// (setup input disabled for DO type);
// DO_STATUS_LED flow synthesis
void setup_DO_STATUS_LED_output(void);
  
// dbgLedBlinkTimer flow acquisition
// (setup input disabled for Variable type);
// dbgLedBlinkTimer flow synthesis
// (output disabled for Variable type);
  
// DBG_LED flow acquisition
// (setup input disabled for Variable type);
// DBG_LED flow synthesis
// (output disabled for Variable type);
  
// SDA flow acquisition
// (setup input disabled for Bus type);
// SDA flow synthesis
// (output disabled for Bus type);
  
// SCL flow acquisition
// (setup input disabled for Bus type);
// SCL flow synthesis
// (output disabled for Bus type);
  
// ResetRelay flow acquisition
// (setup input disabled for DO type);
// ResetRelay flow synthesis
void setup_ResetRelay_output(void);
  
// LCD_DB7 flow acquisition
// (setup input disabled for DO type);
// LCD_DB7 flow synthesis
void setup_LCD_DB7_output(void);
  
// LCD_DB6 flow acquisition
// (setup input disabled for DO type);
// LCD_DB6 flow synthesis
void setup_LCD_DB6_output(void);
  
// LCD_DB5 flow acquisition
// (setup input disabled for DO type);
// LCD_DB5 flow synthesis
void setup_LCD_DB5_output(void);
  
// LCD_DB4 flow acquisition
// (setup input disabled for DO type);
// LCD_DB4 flow synthesis
void setup_LCD_DB4_output(void);
  
// LCD_DB3 flow acquisition
// (setup input disabled for DO type);
// LCD_DB3 flow synthesis
void setup_LCD_DB3_output(void);
  
// LCD_DB2 flow acquisition
// (setup input disabled for DO type);
// LCD_DB2 flow synthesis
void setup_LCD_DB2_output(void);
  
// LCD_DB1 flow acquisition
// (setup input disabled for DO type);
// LCD_DB1 flow synthesis
void setup_LCD_DB1_output(void);
  
// LCD_DB0 flow acquisition
// (setup input disabled for DO type);
// LCD_DB0 flow synthesis
void setup_LCD_DB0_output(void);
  
// BME_CS2 flow acquisition
// (setup input disabled for Bus type);
// BME_CS2 flow synthesis
// (output disabled for Bus type);
  
// BME_MISO flow acquisition
// (setup input disabled for Bus type);
// BME_MISO flow synthesis
// (output disabled for Bus type);
  
// BME_MOSI flow acquisition
// (setup input disabled for Bus type);
// BME_MOSI flow synthesis
// (output disabled for Bus type);
  
// BME_SCK flow acquisition
// (setup input disabled for Bus type);
// BME_SCK flow synthesis
// (output disabled for Bus type);
  
// BME_CS1 flow acquisition
// (setup input disabled for Bus type);
// BME_CS1 flow synthesis
// (output disabled for Bus type);
  
// _5V flow acquisition
// (setup input disabled for Power type);
// _5V flow synthesis
// (output disabled for Power type);
  
// GND flow acquisition
// (setup input disabled for Power type);
// GND flow synthesis
// (output disabled for Power type);
  
// INT_FS flow acquisition
// (setup input disabled for Variable type);
// INT_FS flow synthesis
// (output disabled for Variable type);
  
// INT_FC flow acquisition
// (setup input disabled for Variable type);
// INT_FC flow synthesis
// (output disabled for Variable type);
  
// INT_HS flow acquisition
// (setup input disabled for Variable type);
// INT_HS flow synthesis
// (output disabled for Variable type);
  
// INT_HC flow acquisition
// (setup input disabled for Variable type);
// INT_HC flow synthesis
// (output disabled for Variable type);
  
// SystemDate_W flow acquisition
// (setup input disabled for Variable type);
// SystemDate_W flow synthesis
// (output disabled for Variable type);
  
// SystemTime_W flow acquisition
// (setup input disabled for Variable type);
// SystemTime_W flow synthesis
// (output disabled for Variable type);
  
// PeakFactor flow acquisition
// (setup input disabled for Variable type);
// PeakFactor flow synthesis
// (output disabled for Variable type);
  
// PlateauFactor flow acquisition
// (setup input disabled for Variable type);
// PlateauFactor flow synthesis
// (output disabled for Variable type);
  
// PeepFactor flow acquisition
// (setup input disabled for Variable type);
// PeepFactor flow synthesis
// (output disabled for Variable type);
  
// FrequencyFactor flow acquisition
// (setup input disabled for Variable type);
// FrequencyFactor flow synthesis
// (output disabled for Variable type);
  
// TidalVolumenFactor flow acquisition
// (setup input disabled for Variable type);
// TidalVolumenFactor flow synthesis
// (output disabled for Variable type);
  
// MinimumVolumeFactor flow acquisition
// (setup input disabled for Variable type);
// MinimumVolumeFactor flow synthesis
// (output disabled for Variable type);
  
// TriggerFactor flow acquisition
// (setup input disabled for Variable type);
// TriggerFactor flow synthesis
// (output disabled for Variable type);
  
// BatteryLevelTrigger flow acquisition
// (setup input disabled for Variable type);
// BatteryLevelTrigger flow synthesis
// (output disabled for Variable type);
  
// PatientType flow acquisition
// (setup input disabled for Variable type);
// PatientType flow synthesis
// (output disabled for Variable type);
  
// PatientWeight flow acquisition
// (setup input disabled for Variable type);
// PatientWeight flow synthesis
// (output disabled for Variable type);
  
// PatientHeight flow acquisition
// (setup input disabled for Variable type);
// PatientHeight flow synthesis
// (output disabled for Variable type);
  
// TidalVolume flow acquisition
// (setup input disabled for Variable type);
// TidalVolume flow synthesis
// (output disabled for Variable type);
  
// PIDMode flow acquisition
// (setup input disabled for Variable type);
// PIDMode flow synthesis
// (output disabled for Variable type);
  
// PIDState flow acquisition
// (setup input disabled for Variable type);
// PIDState flow synthesis
// (output disabled for Variable type);
  
// PIDErrorBits flow acquisition
// (setup input disabled for Variable type);
// PIDErrorBits flow synthesis
// (output disabled for Variable type);
  
// Alarms flow acquisition
// (setup input disabled for Variable type);
// Alarms flow synthesis
// (output disabled for Variable type);
  
// Status flow acquisition
// (setup input disabled for Variable type);
// Status flow synthesis
// (output disabled for Variable type);
  
// POWER_StartMode flow acquisition
// (setup input disabled for Variable type);
// POWER_StartMode flow synthesis
// (output disabled for Variable type);
  
// POWER_StopMode flow acquisition
// (setup input disabled for Variable type);
// POWER_StopMode flow synthesis
// (output disabled for Variable type);
  
// HOME_Mode flow acquisition
// (setup input disabled for Variable type);
// HOME_Mode flow synthesis
// (output disabled for Variable type);
  
// SetWriteData flow acquisition
// (setup input disabled for BOOL type);
// SetWriteData flow synthesis
// (output disabled for BOOL type);
  
// SetTime flow acquisition
// (setup input disabled for BOOL type);
// SetTime flow synthesis
// (output disabled for BOOL type);
  
// Trigger flow acquisition
// (setup input disabled for BOOL type);
// Trigger flow synthesis
// (output disabled for BOOL type);
  
// PeakPBPlus flow acquisition
// (setup input disabled for BOOL type);
// PeakPBPlus flow synthesis
// (output disabled for BOOL type);
  
// PeakPBMinus flow acquisition
// (setup input disabled for BOOL type);
// PeakPBMinus flow synthesis
// (output disabled for BOOL type);
  
// PeepPBPlus flow acquisition
// (setup input disabled for BOOL type);
// PeepPBPlus flow synthesis
// (output disabled for BOOL type);
  
// PeepPBMinus flow acquisition
// (setup input disabled for BOOL type);
// PeepPBMinus flow synthesis
// (output disabled for BOOL type);
  
// FrPBPlus flow acquisition
// (setup input disabled for BOOL type);
// FrPBPlus flow synthesis
// (output disabled for BOOL type);
  
// FrPBMinus flow acquisition
// (setup input disabled for BOOL type);
// FrPBMinus flow synthesis
// (output disabled for BOOL type);
  
// RecruitmentOn flow acquisition
// (setup input disabled for BOOL type);
// RecruitmentOn flow synthesis
// (output disabled for BOOL type);
  
// RecruitmentOff flow acquisition
// (setup input disabled for BOOL type);
// RecruitmentOff flow synthesis
// (output disabled for BOOL type);
  
// RecruitmentMem flow acquisition
// (setup input disabled for BOOL type);
// RecruitmentMem flow synthesis
// (output disabled for BOOL type);
  
// RecruitmentRunning flow acquisition
// (setup input disabled for BOOL type);
// RecruitmentRunning flow synthesis
// (output disabled for BOOL type);
  
// RecruitmentEnding flow acquisition
// (setup input disabled for BOOL type);
// RecruitmentEnding flow synthesis
// (output disabled for BOOL type);
  
// RecruitmentEnd flow acquisition
// (setup input disabled for BOOL type);
// RecruitmentEnd flow synthesis
// (output disabled for BOOL type);
  
// ErrorAck flow acquisition
// (setup input disabled for BOOL type);
// ErrorAck flow synthesis
// (output disabled for BOOL type);
  
// Reset flow acquisition
// (setup input disabled for BOOL type);
// Reset flow synthesis
// (output disabled for BOOL type);
  
// PIDError flow acquisition
// (setup input disabled for BOOL type);
// PIDError flow synthesis
// (output disabled for BOOL type);
  
// PeakDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// PeakDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PeakDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// PeakDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PeakDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// PeakDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PeakDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// PeakDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PeakInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// PeakInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PeakInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// PeakInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PeakInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// PeakInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PeakInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// PeakInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PlateauDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// PlateauDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PlateauDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// PlateauDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PlateauDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// PlateauDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PlateauDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// PlateauDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PlateauInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// PlateauInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PlateauInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// PlateauInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PlateauInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// PlateauInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PlateauInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// PlateauInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PeepDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// PeepDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PeepDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// PeepDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PeepDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// PeepDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PeepDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// PeepDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PeepInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// PeepInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PeepInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// PeepInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PeepInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// PeepInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PeepInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// PeepInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// FrequencyDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// FrequencyDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// FrequencyDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// FrequencyDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// FrequencyDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// FrequencyDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// FrequencyDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// FrequencyDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// FrequencyInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// FrequencyInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// FrequencyInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// FrequencyInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// FrequencyInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// FrequencyInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// FrequencyInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// FrequencyInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// TidalVolumeDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// TidalVolumeDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// TidalVolumeDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// TidalVolumeDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// TidalVolumeDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// TidalVolumeDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// TidalVolumeDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// TidalVolumeDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// TidalVolumeInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// TidalVolumeInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// TidalVolumeInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// TidalVolumeInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// TidalVolumeInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// TidalVolumeInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// TidalVolumeInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// TidalVolumeInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// MinuteVolumeDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// MinuteVolumeDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// MinuteVolumeDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// MinuteVolumeDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// MinuteVolumeDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// MinuteVolumeDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// MinuteVolumeDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// MinuteVolumeDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// MinuteVolumeInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// MinuteVolumeInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// MinuteVolumeInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// MinuteVolumeInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// MinuteVolumeInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// MinuteVolumeInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// MinuteVolumeInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// MinuteVolumeInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// TriggerDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// TriggerDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// TriggerDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// TriggerDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// TriggerDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// TriggerDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// TriggerDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// TriggerDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// TriggerInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// TriggerInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// TriggerInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// TriggerInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// TriggerInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// TriggerInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// TriggerInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// TriggerInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// BatteryDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// BatteryDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// BatteryDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// BatteryDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// BatteryDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// BatteryDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// BatteryDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// BatteryDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// BatteryInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// BatteryInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// BatteryInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// BatteryInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// BatteryInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// BatteryInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// BatteryInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// BatteryInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// Enable flow acquisition
// (setup input disabled for BOOL type);
// Enable flow synthesis
// (output disabled for BOOL type);
  
// Busy flow acquisition
// (setup input disabled for BOOL type);
// Busy flow synthesis
// (output disabled for BOOL type);
  
// POWER_Enable flow acquisition
// (setup input disabled for BOOL type);
// POWER_Enable flow synthesis
// (output disabled for BOOL type);
  
// POWER_Status flow acquisition
// (setup input disabled for BOOL type);
// POWER_Status flow synthesis
// (output disabled for BOOL type);
  
// POWER_Error flow acquisition
// (setup input disabled for BOOL type);
// POWER_Error flow synthesis
// (output disabled for BOOL type);
  
// RESET_Execute flow acquisition
// (setup input disabled for BOOL type);
// RESET_Execute flow synthesis
// (output disabled for BOOL type);
  
// RESET_Done flow acquisition
// (setup input disabled for BOOL type);
// RESET_Done flow synthesis
// (output disabled for BOOL type);
  
// RESET_Error flow acquisition
// (setup input disabled for BOOL type);
// RESET_Error flow synthesis
// (output disabled for BOOL type);
  
// HOME_Execute flow acquisition
// (setup input disabled for BOOL type);
// HOME_Execute flow synthesis
// (output disabled for BOOL type);
  
// HOME_Done flow acquisition
// (setup input disabled for BOOL type);
// HOME_Done flow synthesis
// (output disabled for BOOL type);
  
// HOME_Error flow acquisition
// (setup input disabled for BOOL type);
// HOME_Error flow synthesis
// (output disabled for BOOL type);
  
// HALT_Execute flow acquisition
// (setup input disabled for BOOL type);
// HALT_Execute flow synthesis
// (output disabled for BOOL type);
  
// HALT_Done flow acquisition
// (setup input disabled for BOOL type);
// HALT_Done flow synthesis
// (output disabled for BOOL type);
  
// HALT_Error flow acquisition
// (setup input disabled for BOOL type);
// HALT_Error flow synthesis
// (output disabled for BOOL type);
  
// ABSOLUTE_Execute flow acquisition
// (setup input disabled for BOOL type);
// ABSOLUTE_Execute flow synthesis
// (output disabled for BOOL type);
  
// ABSOLUTE_Done flow acquisition
// (setup input disabled for BOOL type);
// ABSOLUTE_Done flow synthesis
// (output disabled for BOOL type);
  
// ABSOLUTE_Error flow acquisition
// (setup input disabled for BOOL type);
// ABSOLUTE_Error flow synthesis
// (output disabled for BOOL type);
  
// RELATIVE_Execute flow acquisition
// (setup input disabled for BOOL type);
// RELATIVE_Execute flow synthesis
// (output disabled for BOOL type);
  
// RELATIVE_Done flow acquisition
// (setup input disabled for BOOL type);
// RELATIVE_Done flow synthesis
// (output disabled for BOOL type);
  
// RELATIVE_Error flow acquisition
// (setup input disabled for BOOL type);
// RELATIVE_Error flow synthesis
// (output disabled for BOOL type);
  
// Velocity_Execute flow acquisition
// (setup input disabled for BOOL type);
// Velocity_Execute flow synthesis
// (output disabled for BOOL type);
  
// Velocity_Current flow acquisition
// (setup input disabled for BOOL type);
// Velocity_Current flow synthesis
// (output disabled for BOOL type);
  
// Velocity_invelocity flow acquisition
// (setup input disabled for BOOL type);
// Velocity_invelocity flow synthesis
// (output disabled for BOOL type);
  
// Velocity_Error flow acquisition
// (setup input disabled for BOOL type);
// Velocity_Error flow synthesis
// (output disabled for BOOL type);
  
// JOG_Forward flow acquisition
// (setup input disabled for BOOL type);
// JOG_Forward flow synthesis
// (output disabled for BOOL type);
  
// JOG_Backward flow acquisition
// (setup input disabled for BOOL type);
// JOG_Backward flow synthesis
// (output disabled for BOOL type);
  
// JOG_Invelocity flow acquisition
// (setup input disabled for BOOL type);
// JOG_Invelocity flow synthesis
// (output disabled for BOOL type);
  
// JOG_Error flow acquisition
// (setup input disabled for BOOL type);
// JOG_Error flow synthesis
// (output disabled for BOOL type);
  
// Permissions flow acquisition
// (setup input disabled for BOOL type);
// Permissions flow synthesis
// (output disabled for BOOL type);
  
// Operation_ON flow acquisition
// (setup input disabled for BOOL type);
// Operation_ON flow synthesis
// (output disabled for BOOL type);
  
// Emergency flow acquisition
// (setup input disabled for BOOL type);
// Emergency flow synthesis
// (output disabled for BOOL type);
  
// GeneralAck flow acquisition
// (setup input disabled for BOOL type);
// GeneralAck flow synthesis
// (output disabled for BOOL type);
  
// MotorAck flow acquisition
// (setup input disabled for BOOL type);
// MotorAck flow synthesis
// (output disabled for BOOL type);
  
// ProcessAck flow acquisition
// (setup input disabled for BOOL type);
// ProcessAck flow synthesis
// (output disabled for BOOL type);
  
// GeneralReset flow acquisition
// (setup input disabled for BOOL type);
// GeneralReset flow synthesis
// (output disabled for BOOL type);
  
// MotorReset flow acquisition
// (setup input disabled for BOOL type);
// MotorReset flow synthesis
// (output disabled for BOOL type);
  
// ProcessReset flow acquisition
// (setup input disabled for BOOL type);
// ProcessReset flow synthesis
// (output disabled for BOOL type);
  
// PeakValue flow acquisition
// (setup input disabled for UI_16 type);
// PeakValue flow synthesis
// (output disabled for UI_16 type);
  
// PlateauValue flow acquisition
// (setup input disabled for UI_16 type);
// PlateauValue flow synthesis
// (output disabled for UI_16 type);
  
// PeepValue flow acquisition
// (setup input disabled for UI_16 type);
// PeepValue flow synthesis
// (output disabled for UI_16 type);
  
// FrequencyValue flow acquisition
// (setup input disabled for UI_16 type);
// FrequencyValue flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumeValue flow acquisition
// (setup input disabled for UI_16 type);
// TidalVolumeValue flow synthesis
// (output disabled for UI_16 type);
  
// MinumValue flow acquisition
// (setup input disabled for UI_16 type);
// MinumValue flow synthesis
// (output disabled for UI_16 type);
  
// DrivingPreassure flow acquisition
// (setup input disabled for UI_16 type);
// DrivingPreassure flow synthesis
// (output disabled for UI_16 type);
  
// BatteryVoltage flow acquisition
// (setup input disabled for UI_16 type);
// BatteryVoltage flow synthesis
// (output disabled for UI_16 type);
  
// BatteryCapacity flow acquisition
// (setup input disabled for UI_16 type);
// BatteryCapacity flow synthesis
// (output disabled for UI_16 type);
  
// Pressure1 flow acquisition
// (setup input disabled for UI_16 type);
// Pressure1 flow synthesis
// (output disabled for UI_16 type);
  
// Pressure2 flow acquisition
// (setup input disabled for UI_16 type);
// Pressure2 flow synthesis
// (output disabled for UI_16 type);
  
// VomumeAccumulated flow acquisition
// (setup input disabled for UI_16 type);
// VomumeAccumulated flow synthesis
// (output disabled for UI_16 type);
  
// Flow flow acquisition
// (setup input disabled for UI_16 type);
// Flow flow synthesis
// (output disabled for UI_16 type);
  
// SystemDateRetVal flow acquisition
// (setup input disabled for UI_16 type);
// SystemDateRetVal flow synthesis
// (output disabled for UI_16 type);
  
// SystemDateOut flow acquisition
// (setup input disabled for DateTime type);
// SystemDateOut flow synthesis
// (output disabled for DateTime type);
  
// LocalDateRetVal flow acquisition
// (setup input disabled for UI_16 type);
// LocalDateRetVal flow synthesis
// (output disabled for UI_16 type);
  
// LocalDateOut flow acquisition
// (setup input disabled for DateTime type);
// LocalDateOut flow synthesis
// (output disabled for DateTime type);
  
// WriteDateTime flow acquisition
// (setup input disabled for DateTime type);
// WriteDateTime flow synthesis
// (output disabled for DateTime type);
  
// WriteDateOut flow acquisition
// (setup input disabled for DateTime type);
// WriteDateOut flow synthesis
// (output disabled for DateTime type);
  
// StatuSetTime flow acquisition
// (setup input disabled for DateTime type);
// StatuSetTime flow synthesis
// (output disabled for DateTime type);
  
// PeakMax flow acquisition
// (setup input disabled for UI_16 type);
// PeakMax flow synthesis
// (output disabled for UI_16 type);
  
// PeakSetpoint flow acquisition
// (setup input disabled for UI_16 type);
// PeakSetpoint flow synthesis
// (output disabled for UI_16 type);
  
// PeakMin flow acquisition
// (setup input disabled for UI_16 type);
// PeakMin flow synthesis
// (output disabled for UI_16 type);
  
// PlateauMax flow acquisition
// (setup input disabled for UI_16 type);
// PlateauMax flow synthesis
// (output disabled for UI_16 type);
  
// PlateauSetpoint flow acquisition
// (setup input disabled for UI_16 type);
// PlateauSetpoint flow synthesis
// (output disabled for UI_16 type);
  
// PlateauMin flow acquisition
// (setup input disabled for UI_16 type);
// PlateauMin flow synthesis
// (output disabled for UI_16 type);
  
// PeepMax flow acquisition
// (setup input disabled for UI_16 type);
// PeepMax flow synthesis
// (output disabled for UI_16 type);
  
// PeepSetpoint flow acquisition
// (setup input disabled for UI_16 type);
// PeepSetpoint flow synthesis
// (output disabled for UI_16 type);
  
// PeepMin flow acquisition
// (setup input disabled for UI_16 type);
// PeepMin flow synthesis
// (output disabled for UI_16 type);
  
// FrequencyMax flow acquisition
// (setup input disabled for UI_16 type);
// FrequencyMax flow synthesis
// (output disabled for UI_16 type);
  
// FrequencySetpoint flow acquisition
// (setup input disabled for UI_16 type);
// FrequencySetpoint flow synthesis
// (output disabled for UI_16 type);
  
// FrequencyMin flow acquisition
// (setup input disabled for UI_16 type);
// FrequencyMin flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumenMax flow acquisition
// (setup input disabled for UI_16 type);
// TidalVolumenMax flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumenSetpoint flow acquisition
// (setup input disabled for UI_16 type);
// TidalVolumenSetpoint flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumenMin flow acquisition
// (setup input disabled for UI_16 type);
// TidalVolumenMin flow synthesis
// (output disabled for UI_16 type);
  
// MinimumVolumeMax flow acquisition
// (setup input disabled for UI_16 type);
// MinimumVolumeMax flow synthesis
// (output disabled for UI_16 type);
  
// MinimumVolumeSetpoint flow acquisition
// (setup input disabled for UI_16 type);
// MinimumVolumeSetpoint flow synthesis
// (output disabled for UI_16 type);
  
// MinimumVolumeMin flow acquisition
// (setup input disabled for UI_16 type);
// MinimumVolumeMin flow synthesis
// (output disabled for UI_16 type);
  
// TriggerSetpoint flow acquisition
// (setup input disabled for UI_16 type);
// TriggerSetpoint flow synthesis
// (output disabled for UI_16 type);
  
// RecruitmentTimer flow acquisition
// (setup input disabled for TimeUI_16 type);
// RecruitmentTimer flow synthesis
// (output disabled for TimeUI_16 type);
  
// RecruitmentElap flow acquisition
// (setup input disabled for TimeUI_16 type);
// RecruitmentElap flow synthesis
// (output disabled for TimeUI_16 type);
  
// TimeStart flow acquisition
// (setup input disabled for DateTime type);
// TimeStart flow synthesis
// (output disabled for DateTime type);
  
// TimeEnd flow acquisition
// (setup input disabled for DateTime type);
// TimeEnd flow synthesis
// (output disabled for DateTime type);
  
// FlowSetpoint flow acquisition
// (setup input disabled for UI_16 type);
// FlowSetpoint flow synthesis
// (output disabled for UI_16 type);
  
// PIDOutReal flow acquisition
// (setup input disabled for FL_16 type);
// PIDOutReal flow synthesis
// (output disabled for FL_16 type);
  
// PIDOutInt flow acquisition
// (setup input disabled for UI_16 type);
// PIDOutInt flow synthesis
// (output disabled for UI_16 type);
  
// PIDOutPWM flow acquisition
// (setup input disabled for UI_16 type);
// PIDOutPWM flow synthesis
// (output disabled for UI_16 type);
  
// PIDOutMotor flow acquisition
// (setup input disabled for UI_16 type);
// PIDOutMotor flow synthesis
// (output disabled for UI_16 type);
  
// PeakSetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// PeakSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// PeakSetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// PeakSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// PeakSetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// PeakSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// PeakSetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// PeakSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// PeakSetpointHysteresisHI flow acquisition
// (setup input disabled for UI_16 type);
// PeakSetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type);
  
// PeakSetpointHysteresisLOW flow acquisition
// (setup input disabled for UI_16 type);
// PeakSetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type);
  
// PlateauSetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// PlateauSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// PlateauSetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// PlateauSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// PlateauSetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// PlateauSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// PlateaukSetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// PlateaukSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// PlateaukSetpointHysteresisHI flow acquisition
// (setup input disabled for UI_16 type);
// PlateaukSetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type);
  
// PlateaukSetpointHysteresisLOW flow acquisition
// (setup input disabled for UI_16 type);
// PlateaukSetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type);
  
// PeepSetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// PeepSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// PeepSetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// PeepSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// PeepSetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// PeepSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// PeepSetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// PeepSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// PeepSetpointHysteresisHI flow acquisition
// (setup input disabled for UI_16 type);
// PeepSetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type);
  
// PeepSetpointHysteresisLOW flow acquisition
// (setup input disabled for UI_16 type);
// PeepSetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type);
  
// FrequencySetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// FrequencySetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// FrequencySetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// FrequencySetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// FrequencySetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// FrequencySetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// FrequencySetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// FrequencySetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// FrequencySetpointHysteresisHI flow acquisition
// (setup input disabled for UI_16 type);
// FrequencySetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type);
  
// FrequencySetpointHysteresisLOW flow acquisition
// (setup input disabled for UI_16 type);
// FrequencySetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumeSetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// TidalVolumeSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumeSetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// TidalVolumeSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumeSetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// TidalVolumeSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumeSetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// TidalVolumeSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumeHystersisHI flow acquisition
// (setup input disabled for UI_16 type);
// TidalVolumeHystersisHI flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumeHystersisLOW flow acquisition
// (setup input disabled for UI_16 type);
// TidalVolumeHystersisLOW flow synthesis
// (output disabled for UI_16 type);
  
// MinuteVolumeSetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// MinuteVolumeSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// MinuteVolumeSetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// MinuteVolumeSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// MinuteVolumeSetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// MinuteVolumeSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// MinuteVolumeSetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// MinuteVolumeSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// MinuteVolumeHysteresisHI flow acquisition
// (setup input disabled for UI_16 type);
// MinuteVolumeHysteresisHI flow synthesis
// (output disabled for UI_16 type);
  
// MinuteVolumeHysteresisLOW flow acquisition
// (setup input disabled for UI_16 type);
// MinuteVolumeHysteresisLOW flow synthesis
// (output disabled for UI_16 type);
  
// TriggeVolumeSetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// TriggeVolumeSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// TriggerVolumeSetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// TriggerVolumeSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// TriggerVolumeSetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// TriggerVolumeSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// TriggerVolumeSetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// TriggerVolumeSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// TriggerVolumeHysteresisHI flow acquisition
// (setup input disabled for UI_16 type);
// TriggerVolumeHysteresisHI flow synthesis
// (output disabled for UI_16 type);
  
// TriggerVolumeHysteresisLOW flow acquisition
// (setup input disabled for UI_16 type);
// TriggerVolumeHysteresisLOW flow synthesis
// (output disabled for UI_16 type);
  
// BatterySetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// BatterySetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// BatterySetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// BatterySetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// BatterySetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// BatterySetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// BatterySetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// BatterySetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// BatterySetpointHysteresisHI flow acquisition
// (setup input disabled for UI_16 type);
// BatterySetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type);
  
// BatterySetpointHysteresisLOW flow acquisition
// (setup input disabled for UI_16 type);
// BatterySetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type);
  
// Frecuency flow acquisition
// (setup input disabled for UI_16 type);
// Frecuency flow synthesis
// (output disabled for UI_16 type);
  
// Duty_Cycle flow acquisition
// (setup input disabled for UI_16 type);
// Duty_Cycle flow synthesis
// (output disabled for UI_16 type);
  
// HOME_Position flow acquisition
// (setup input disabled for UI_16 type);
// HOME_Position flow synthesis
// (output disabled for UI_16 type);
  
// ABSOLUTE_Position flow acquisition
// (setup input disabled for UI_16 type);
// ABSOLUTE_Position flow synthesis
// (output disabled for UI_16 type);
  
// ABSOLUTE_Velocity flow acquisition
// (setup input disabled for UI_16 type);
// ABSOLUTE_Velocity flow synthesis
// (output disabled for UI_16 type);
  
// RELATIVE_Distance flow acquisition
// (setup input disabled for UI_16 type);
// RELATIVE_Distance flow synthesis
// (output disabled for UI_16 type);
  
// RELATIVE_Velocity flow acquisition
// (setup input disabled for UI_16 type);
// RELATIVE_Velocity flow synthesis
// (output disabled for UI_16 type);
  
// Velocity_Velocity flow acquisition
// (setup input disabled for UI_16 type);
// Velocity_Velocity flow synthesis
// (output disabled for UI_16 type);
  
// JOG_Velocity flow acquisition
// (setup input disabled for UI_16 type);
// JOG_Velocity flow synthesis
// (output disabled for UI_16 type);
  
// EmergencyStop flow acquisition
void setup_EmergencyStop(void);
// EmergencyStop flow synthesis
// (output disabled for DI type);
  
// TurnAxisHwHi flow acquisition
void setup_TurnAxisHwHi(void);
// TurnAxisHwHi flow synthesis
// (output disabled for DI_pu type);
  
// BeaconLightRed flow acquisition
// (setup input disabled for DO type);
// BeaconLightRed flow synthesis
void setup_BeaconLightRed_output(void);
  
// BeaconLightOrange flow acquisition
// (setup input disabled for DO type);
// BeaconLightOrange flow synthesis
void setup_BeaconLightOrange_output(void);
  
// TurnAxisPulse flow acquisition
// (setup input disabled for DO type);
// TurnAxisPulse flow synthesis
void setup_TurnAxisPulse_output(void);
  
// TurnAxisDir flow acquisition
// (setup input disabled for DO type);
// TurnAxisDir flow synthesis
void setup_TurnAxisDir_output(void);
  
// TurnAxisEnable flow acquisition
// (setup input disabled for DO type);
// TurnAxisEnable flow synthesis
void setup_TurnAxisEnable_output(void);
  
// BeaconLightGreen flow acquisition
// (setup input disabled for DO type);
// BeaconLightGreen flow synthesis
void setup_BeaconLightGreen_output(void);
  
// Buzzer flow acquisition
// (setup input disabled for DO type);
// Buzzer flow synthesis
void setup_Buzzer_output(void);
  
// PeepValve flow acquisition
// (setup input disabled for DO type);
// PeepValve flow synthesis
void setup_PeepValve_output(void);
  
// TurnAxisHwLow flow acquisition
void setup_TurnAxisHwLow(void);
// TurnAxisHwLow flow synthesis
// (output disabled for DI_pu type);
  
// ServoPend flow acquisition
void setup_ServoPend(void);
// ServoPend flow synthesis
// (output disabled for DI type);
  
// ServoAlarm flow acquisition
void setup_ServoAlarm(void);
// ServoAlarm flow synthesis
// (output disabled for DI_pu type);
  
// AtmosfericPressure flow acquisition
void setup_AtmosfericPressure(void);
// AtmosfericPressure flow synthesis
// (output disabled for ADC type);
  
// DiferentialPressure flow acquisition
void setup_DiferentialPressure(void);
// DiferentialPressure flow synthesis
// (output disabled for ADC type);
  
// SerTX flow acquisition
// (setup input disabled for Bus type);
// SerTX flow synthesis
// (output disabled for Bus type);
  
// SerRX flow acquisition
// (setup input disabled for Bus type);
// SerRX flow synthesis
// (output disabled for Bus type);
  
// EmgcyButton flow acquisition
// (setup input disabled for TBD type);
// EmgcyButton flow synthesis
// (output disabled for TBD type);
  
// ApiVersion  flow acquisition
// (setup input disabled for UI_8 type);
// ApiVersion  flow synthesis
// (output disabled for UI_8 type);
  
// MachineUuid flow acquisition
// (setup input disabled for Variable type);
// MachineUuid flow synthesis
// (output disabled for Variable type);
  
// FirmwareVersion flow acquisition
// (setup input disabled for UI_16 type);
// FirmwareVersion flow synthesis
// (output disabled for UI_16 type);
  
// Uptime15mCounter flow acquisition
// (setup input disabled for UI_16 type);
// Uptime15mCounter flow synthesis
// (output disabled for UI_16 type);
  
// MutedAlarmSeconds flow acquisition
// (setup input disabled for UI_8 type);
// MutedAlarmSeconds flow synthesis
// (output disabled for UI_8 type);
  
// CycleRpmLast30s flow acquisition
// (setup input disabled for UI_16 type);
// CycleRpmLast30s flow synthesis
// (output disabled for UI_16 type);
  
// CycleIndications flow acquisition
// (setup input disabled for Variable type);
// CycleIndications flow synthesis
// (output disabled for Variable type);
  
// CycleDuration flow acquisition
// (setup input disabled for TimeUI_16 type);
// CycleDuration flow synthesis
// (output disabled for TimeUI_16 type);
  
// VentilationFlags flow acquisition
// (setup input disabled for Variable type);
// VentilationFlags flow synthesis
// (output disabled for Variable type);
  
// VolumeSetting flow acquisition
// (setup input disabled for UI_16 type);
// VolumeSetting flow synthesis
// (output disabled for UI_16 type);
  
// RpmSetting flow acquisition
// (setup input disabled for UI_16 type);
// RpmSetting flow synthesis
// (output disabled for UI_16 type);
  
// RampDegreesSetting flow acquisition
// (setup input disabled for UI_8 type);
// RampDegreesSetting flow synthesis
// (output disabled for UI_8 type);
  
// EiRatioSetting flow acquisition
// (setup input disabled for UI_8 type);
// EiRatioSetting flow synthesis
// (output disabled for UI_8 type);
  
// RecruitTimer flow acquisition
// (setup input disabled for UI_8 type);
// RecruitTimer flow synthesis
// (output disabled for UI_8 type);
  
// IncomingFrame flow acquisition
// (setup input disabled for Variable type);
// IncomingFrame flow synthesis
// (output disabled for Variable type);
  
// OutgoingFrame flow acquisition
// (setup input disabled for Variable type);
// OutgoingFrame flow synthesis
// (output disabled for Variable type);

// Input / Output functions

  
// STATUS_LED flow acquisition
// (input disabled for LED type);
// STATUS_LED flow synthesis
// (output disabled for LED type);
  
// DO_STATUS_LED flow acquisition
// (input disabled for DO type);
// DO_STATUS_LED flow synthesis
void synthesize_DO_STATUS_LED(void);
  
// dbgLedBlinkTimer flow acquisition
// (input disabled for Variable type);
// dbgLedBlinkTimer flow synthesis
// (output disabled for Variable type);
  
// DBG_LED flow acquisition
// (input disabled for Variable type);
// DBG_LED flow synthesis
// (output disabled for Variable type);
  
// SDA flow acquisition
// (input disabled for Bus type);
// SDA flow synthesis
// (output disabled for Bus type);
  
// SCL flow acquisition
// (input disabled for Bus type);
// SCL flow synthesis
// (output disabled for Bus type);
  
// ResetRelay flow acquisition
// (input disabled for DO type);
// ResetRelay flow synthesis
void synthesize_ResetRelay(void);
  
// LCD_DB7 flow acquisition
// (input disabled for DO type);
// LCD_DB7 flow synthesis
void synthesize_LCD_DB7(void);
  
// LCD_DB6 flow acquisition
// (input disabled for DO type);
// LCD_DB6 flow synthesis
void synthesize_LCD_DB6(void);
  
// LCD_DB5 flow acquisition
// (input disabled for DO type);
// LCD_DB5 flow synthesis
void synthesize_LCD_DB5(void);
  
// LCD_DB4 flow acquisition
// (input disabled for DO type);
// LCD_DB4 flow synthesis
void synthesize_LCD_DB4(void);
  
// LCD_DB3 flow acquisition
// (input disabled for DO type);
// LCD_DB3 flow synthesis
void synthesize_LCD_DB3(void);
  
// LCD_DB2 flow acquisition
// (input disabled for DO type);
// LCD_DB2 flow synthesis
void synthesize_LCD_DB2(void);
  
// LCD_DB1 flow acquisition
// (input disabled for DO type);
// LCD_DB1 flow synthesis
void synthesize_LCD_DB1(void);
  
// LCD_DB0 flow acquisition
// (input disabled for DO type);
// LCD_DB0 flow synthesis
void synthesize_LCD_DB0(void);
  
// BME_CS2 flow acquisition
// (input disabled for Bus type);
// BME_CS2 flow synthesis
// (output disabled for Bus type);
  
// BME_MISO flow acquisition
// (input disabled for Bus type);
// BME_MISO flow synthesis
// (output disabled for Bus type);
  
// BME_MOSI flow acquisition
// (input disabled for Bus type);
// BME_MOSI flow synthesis
// (output disabled for Bus type);
  
// BME_SCK flow acquisition
// (input disabled for Bus type);
// BME_SCK flow synthesis
// (output disabled for Bus type);
  
// BME_CS1 flow acquisition
// (input disabled for Bus type);
// BME_CS1 flow synthesis
// (output disabled for Bus type);
  
// _5V flow acquisition
// (input disabled for Power type);
// _5V flow synthesis
// (output disabled for Power type);
  
// GND flow acquisition
// (input disabled for Power type);
// GND flow synthesis
// (output disabled for Power type);
  
// INT_FS flow acquisition
// (input disabled for Variable type);
// INT_FS flow synthesis
// (output disabled for Variable type);
  
// INT_FC flow acquisition
// (input disabled for Variable type);
// INT_FC flow synthesis
// (output disabled for Variable type);
  
// INT_HS flow acquisition
// (input disabled for Variable type);
// INT_HS flow synthesis
// (output disabled for Variable type);
  
// INT_HC flow acquisition
// (input disabled for Variable type);
// INT_HC flow synthesis
// (output disabled for Variable type);
  
// SystemDate_W flow acquisition
// (input disabled for Variable type);
// SystemDate_W flow synthesis
// (output disabled for Variable type);
  
// SystemTime_W flow acquisition
// (input disabled for Variable type);
// SystemTime_W flow synthesis
// (output disabled for Variable type);
  
// PeakFactor flow acquisition
// (input disabled for Variable type);
// PeakFactor flow synthesis
// (output disabled for Variable type);
  
// PlateauFactor flow acquisition
// (input disabled for Variable type);
// PlateauFactor flow synthesis
// (output disabled for Variable type);
  
// PeepFactor flow acquisition
// (input disabled for Variable type);
// PeepFactor flow synthesis
// (output disabled for Variable type);
  
// FrequencyFactor flow acquisition
// (input disabled for Variable type);
// FrequencyFactor flow synthesis
// (output disabled for Variable type);
  
// TidalVolumenFactor flow acquisition
// (input disabled for Variable type);
// TidalVolumenFactor flow synthesis
// (output disabled for Variable type);
  
// MinimumVolumeFactor flow acquisition
// (input disabled for Variable type);
// MinimumVolumeFactor flow synthesis
// (output disabled for Variable type);
  
// TriggerFactor flow acquisition
// (input disabled for Variable type);
// TriggerFactor flow synthesis
// (output disabled for Variable type);
  
// BatteryLevelTrigger flow acquisition
// (input disabled for Variable type);
// BatteryLevelTrigger flow synthesis
// (output disabled for Variable type);
  
// PatientType flow acquisition
// (input disabled for Variable type);
// PatientType flow synthesis
// (output disabled for Variable type);
  
// PatientWeight flow acquisition
// (input disabled for Variable type);
// PatientWeight flow synthesis
// (output disabled for Variable type);
  
// PatientHeight flow acquisition
// (input disabled for Variable type);
// PatientHeight flow synthesis
// (output disabled for Variable type);
  
// TidalVolume flow acquisition
// (input disabled for Variable type);
// TidalVolume flow synthesis
// (output disabled for Variable type);
  
// PIDMode flow acquisition
// (input disabled for Variable type);
// PIDMode flow synthesis
// (output disabled for Variable type);
  
// PIDState flow acquisition
// (input disabled for Variable type);
// PIDState flow synthesis
// (output disabled for Variable type);
  
// PIDErrorBits flow acquisition
// (input disabled for Variable type);
// PIDErrorBits flow synthesis
// (output disabled for Variable type);
  
// Alarms flow acquisition
// (input disabled for Variable type);
// Alarms flow synthesis
// (output disabled for Variable type);
  
// Status flow acquisition
// (input disabled for Variable type);
// Status flow synthesis
// (output disabled for Variable type);
  
// POWER_StartMode flow acquisition
// (input disabled for Variable type);
// POWER_StartMode flow synthesis
// (output disabled for Variable type);
  
// POWER_StopMode flow acquisition
// (input disabled for Variable type);
// POWER_StopMode flow synthesis
// (output disabled for Variable type);
  
// HOME_Mode flow acquisition
// (input disabled for Variable type);
// HOME_Mode flow synthesis
// (output disabled for Variable type);
  
// SetWriteData flow acquisition
// (input disabled for BOOL type);
// SetWriteData flow synthesis
// (output disabled for BOOL type);
  
// SetTime flow acquisition
// (input disabled for BOOL type);
// SetTime flow synthesis
// (output disabled for BOOL type);
  
// Trigger flow acquisition
// (input disabled for BOOL type);
// Trigger flow synthesis
// (output disabled for BOOL type);
  
// PeakPBPlus flow acquisition
// (input disabled for BOOL type);
// PeakPBPlus flow synthesis
// (output disabled for BOOL type);
  
// PeakPBMinus flow acquisition
// (input disabled for BOOL type);
// PeakPBMinus flow synthesis
// (output disabled for BOOL type);
  
// PeepPBPlus flow acquisition
// (input disabled for BOOL type);
// PeepPBPlus flow synthesis
// (output disabled for BOOL type);
  
// PeepPBMinus flow acquisition
// (input disabled for BOOL type);
// PeepPBMinus flow synthesis
// (output disabled for BOOL type);
  
// FrPBPlus flow acquisition
// (input disabled for BOOL type);
// FrPBPlus flow synthesis
// (output disabled for BOOL type);
  
// FrPBMinus flow acquisition
// (input disabled for BOOL type);
// FrPBMinus flow synthesis
// (output disabled for BOOL type);
  
// RecruitmentOn flow acquisition
// (input disabled for BOOL type);
// RecruitmentOn flow synthesis
// (output disabled for BOOL type);
  
// RecruitmentOff flow acquisition
// (input disabled for BOOL type);
// RecruitmentOff flow synthesis
// (output disabled for BOOL type);
  
// RecruitmentMem flow acquisition
// (input disabled for BOOL type);
// RecruitmentMem flow synthesis
// (output disabled for BOOL type);
  
// RecruitmentRunning flow acquisition
// (input disabled for BOOL type);
// RecruitmentRunning flow synthesis
// (output disabled for BOOL type);
  
// RecruitmentEnding flow acquisition
// (input disabled for BOOL type);
// RecruitmentEnding flow synthesis
// (output disabled for BOOL type);
  
// RecruitmentEnd flow acquisition
// (input disabled for BOOL type);
// RecruitmentEnd flow synthesis
// (output disabled for BOOL type);
  
// ErrorAck flow acquisition
// (input disabled for BOOL type);
// ErrorAck flow synthesis
// (output disabled for BOOL type);
  
// Reset flow acquisition
// (input disabled for BOOL type);
// Reset flow synthesis
// (output disabled for BOOL type);
  
// PIDError flow acquisition
// (input disabled for BOOL type);
// PIDError flow synthesis
// (output disabled for BOOL type);
  
// PeakDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// PeakDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PeakDisplayAlarmHI flow acquisition
// (input disabled for BOOL type);
// PeakDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PeakDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// PeakDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PeakDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type);
// PeakDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PeakInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// PeakInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PeakInterlockAlarmHI flow acquisition
// (input disabled for BOOL type);
// PeakInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PeakInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// PeakInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PeakInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type);
// PeakInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PlateauDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// PlateauDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PlateauDisplayAlarmHI flow acquisition
// (input disabled for BOOL type);
// PlateauDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PlateauDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// PlateauDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PlateauDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type);
// PlateauDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PlateauInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// PlateauInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PlateauInterlockAlarmHI flow acquisition
// (input disabled for BOOL type);
// PlateauInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PlateauInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// PlateauInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PlateauInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type);
// PlateauInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PeepDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// PeepDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PeepDisplayAlarmHI flow acquisition
// (input disabled for BOOL type);
// PeepDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PeepDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// PeepDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PeepDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type);
// PeepDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PeepInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// PeepInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PeepInterlockAlarmHI flow acquisition
// (input disabled for BOOL type);
// PeepInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PeepInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// PeepInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PeepInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type);
// PeepInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// FrequencyDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// FrequencyDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// FrequencyDisplayAlarmHI flow acquisition
// (input disabled for BOOL type);
// FrequencyDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// FrequencyDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// FrequencyDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// FrequencyDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type);
// FrequencyDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// FrequencyInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// FrequencyInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// FrequencyInterlockAlarmHI flow acquisition
// (input disabled for BOOL type);
// FrequencyInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// FrequencyInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// FrequencyInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// FrequencyInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type);
// FrequencyInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// TidalVolumeDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// TidalVolumeDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// TidalVolumeDisplayAlarmHI flow acquisition
// (input disabled for BOOL type);
// TidalVolumeDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// TidalVolumeDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// TidalVolumeDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// TidalVolumeDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type);
// TidalVolumeDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// TidalVolumeInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// TidalVolumeInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// TidalVolumeInterlockAlarmHI flow acquisition
// (input disabled for BOOL type);
// TidalVolumeInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// TidalVolumeInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// TidalVolumeInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// TidalVolumeInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type);
// TidalVolumeInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// MinuteVolumeDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// MinuteVolumeDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// MinuteVolumeDisplayAlarmHI flow acquisition
// (input disabled for BOOL type);
// MinuteVolumeDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// MinuteVolumeDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// MinuteVolumeDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// MinuteVolumeDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type);
// MinuteVolumeDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// MinuteVolumeInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// MinuteVolumeInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// MinuteVolumeInterlockAlarmHI flow acquisition
// (input disabled for BOOL type);
// MinuteVolumeInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// MinuteVolumeInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// MinuteVolumeInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// MinuteVolumeInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type);
// MinuteVolumeInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// TriggerDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// TriggerDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// TriggerDisplayAlarmHI flow acquisition
// (input disabled for BOOL type);
// TriggerDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// TriggerDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// TriggerDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// TriggerDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type);
// TriggerDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// TriggerInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// TriggerInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// TriggerInterlockAlarmHI flow acquisition
// (input disabled for BOOL type);
// TriggerInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// TriggerInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// TriggerInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// TriggerInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type);
// TriggerInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// BatteryDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// BatteryDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// BatteryDisplayAlarmHI flow acquisition
// (input disabled for BOOL type);
// BatteryDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// BatteryDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// BatteryDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// BatteryDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type);
// BatteryDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// BatteryInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// BatteryInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// BatteryInterlockAlarmHI flow acquisition
// (input disabled for BOOL type);
// BatteryInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// BatteryInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// BatteryInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// BatteryInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type);
// BatteryInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// Enable flow acquisition
// (input disabled for BOOL type);
// Enable flow synthesis
// (output disabled for BOOL type);
  
// Busy flow acquisition
// (input disabled for BOOL type);
// Busy flow synthesis
// (output disabled for BOOL type);
  
// POWER_Enable flow acquisition
// (input disabled for BOOL type);
// POWER_Enable flow synthesis
// (output disabled for BOOL type);
  
// POWER_Status flow acquisition
// (input disabled for BOOL type);
// POWER_Status flow synthesis
// (output disabled for BOOL type);
  
// POWER_Error flow acquisition
// (input disabled for BOOL type);
// POWER_Error flow synthesis
// (output disabled for BOOL type);
  
// RESET_Execute flow acquisition
// (input disabled for BOOL type);
// RESET_Execute flow synthesis
// (output disabled for BOOL type);
  
// RESET_Done flow acquisition
// (input disabled for BOOL type);
// RESET_Done flow synthesis
// (output disabled for BOOL type);
  
// RESET_Error flow acquisition
// (input disabled for BOOL type);
// RESET_Error flow synthesis
// (output disabled for BOOL type);
  
// HOME_Execute flow acquisition
// (input disabled for BOOL type);
// HOME_Execute flow synthesis
// (output disabled for BOOL type);
  
// HOME_Done flow acquisition
// (input disabled for BOOL type);
// HOME_Done flow synthesis
// (output disabled for BOOL type);
  
// HOME_Error flow acquisition
// (input disabled for BOOL type);
// HOME_Error flow synthesis
// (output disabled for BOOL type);
  
// HALT_Execute flow acquisition
// (input disabled for BOOL type);
// HALT_Execute flow synthesis
// (output disabled for BOOL type);
  
// HALT_Done flow acquisition
// (input disabled for BOOL type);
// HALT_Done flow synthesis
// (output disabled for BOOL type);
  
// HALT_Error flow acquisition
// (input disabled for BOOL type);
// HALT_Error flow synthesis
// (output disabled for BOOL type);
  
// ABSOLUTE_Execute flow acquisition
// (input disabled for BOOL type);
// ABSOLUTE_Execute flow synthesis
// (output disabled for BOOL type);
  
// ABSOLUTE_Done flow acquisition
// (input disabled for BOOL type);
// ABSOLUTE_Done flow synthesis
// (output disabled for BOOL type);
  
// ABSOLUTE_Error flow acquisition
// (input disabled for BOOL type);
// ABSOLUTE_Error flow synthesis
// (output disabled for BOOL type);
  
// RELATIVE_Execute flow acquisition
// (input disabled for BOOL type);
// RELATIVE_Execute flow synthesis
// (output disabled for BOOL type);
  
// RELATIVE_Done flow acquisition
// (input disabled for BOOL type);
// RELATIVE_Done flow synthesis
// (output disabled for BOOL type);
  
// RELATIVE_Error flow acquisition
// (input disabled for BOOL type);
// RELATIVE_Error flow synthesis
// (output disabled for BOOL type);
  
// Velocity_Execute flow acquisition
// (input disabled for BOOL type);
// Velocity_Execute flow synthesis
// (output disabled for BOOL type);
  
// Velocity_Current flow acquisition
// (input disabled for BOOL type);
// Velocity_Current flow synthesis
// (output disabled for BOOL type);
  
// Velocity_invelocity flow acquisition
// (input disabled for BOOL type);
// Velocity_invelocity flow synthesis
// (output disabled for BOOL type);
  
// Velocity_Error flow acquisition
// (input disabled for BOOL type);
// Velocity_Error flow synthesis
// (output disabled for BOOL type);
  
// JOG_Forward flow acquisition
// (input disabled for BOOL type);
// JOG_Forward flow synthesis
// (output disabled for BOOL type);
  
// JOG_Backward flow acquisition
// (input disabled for BOOL type);
// JOG_Backward flow synthesis
// (output disabled for BOOL type);
  
// JOG_Invelocity flow acquisition
// (input disabled for BOOL type);
// JOG_Invelocity flow synthesis
// (output disabled for BOOL type);
  
// JOG_Error flow acquisition
// (input disabled for BOOL type);
// JOG_Error flow synthesis
// (output disabled for BOOL type);
  
// Permissions flow acquisition
// (input disabled for BOOL type);
// Permissions flow synthesis
// (output disabled for BOOL type);
  
// Operation_ON flow acquisition
// (input disabled for BOOL type);
// Operation_ON flow synthesis
// (output disabled for BOOL type);
  
// Emergency flow acquisition
// (input disabled for BOOL type);
// Emergency flow synthesis
// (output disabled for BOOL type);
  
// GeneralAck flow acquisition
// (input disabled for BOOL type);
// GeneralAck flow synthesis
// (output disabled for BOOL type);
  
// MotorAck flow acquisition
// (input disabled for BOOL type);
// MotorAck flow synthesis
// (output disabled for BOOL type);
  
// ProcessAck flow acquisition
// (input disabled for BOOL type);
// ProcessAck flow synthesis
// (output disabled for BOOL type);
  
// GeneralReset flow acquisition
// (input disabled for BOOL type);
// GeneralReset flow synthesis
// (output disabled for BOOL type);
  
// MotorReset flow acquisition
// (input disabled for BOOL type);
// MotorReset flow synthesis
// (output disabled for BOOL type);
  
// ProcessReset flow acquisition
// (input disabled for BOOL type);
// ProcessReset flow synthesis
// (output disabled for BOOL type);
  
// PeakValue flow acquisition
// (input disabled for UI_16 type);
// PeakValue flow synthesis
// (output disabled for UI_16 type);
  
// PlateauValue flow acquisition
// (input disabled for UI_16 type);
// PlateauValue flow synthesis
// (output disabled for UI_16 type);
  
// PeepValue flow acquisition
// (input disabled for UI_16 type);
// PeepValue flow synthesis
// (output disabled for UI_16 type);
  
// FrequencyValue flow acquisition
// (input disabled for UI_16 type);
// FrequencyValue flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumeValue flow acquisition
// (input disabled for UI_16 type);
// TidalVolumeValue flow synthesis
// (output disabled for UI_16 type);
  
// MinumValue flow acquisition
// (input disabled for UI_16 type);
// MinumValue flow synthesis
// (output disabled for UI_16 type);
  
// DrivingPreassure flow acquisition
// (input disabled for UI_16 type);
// DrivingPreassure flow synthesis
// (output disabled for UI_16 type);
  
// BatteryVoltage flow acquisition
// (input disabled for UI_16 type);
// BatteryVoltage flow synthesis
// (output disabled for UI_16 type);
  
// BatteryCapacity flow acquisition
// (input disabled for UI_16 type);
// BatteryCapacity flow synthesis
// (output disabled for UI_16 type);
  
// Pressure1 flow acquisition
// (input disabled for UI_16 type);
// Pressure1 flow synthesis
// (output disabled for UI_16 type);
  
// Pressure2 flow acquisition
// (input disabled for UI_16 type);
// Pressure2 flow synthesis
// (output disabled for UI_16 type);
  
// VomumeAccumulated flow acquisition
// (input disabled for UI_16 type);
// VomumeAccumulated flow synthesis
// (output disabled for UI_16 type);
  
// Flow flow acquisition
// (input disabled for UI_16 type);
// Flow flow synthesis
// (output disabled for UI_16 type);
  
// SystemDateRetVal flow acquisition
// (input disabled for UI_16 type);
// SystemDateRetVal flow synthesis
// (output disabled for UI_16 type);
  
// SystemDateOut flow acquisition
// (input disabled for DateTime type);
// SystemDateOut flow synthesis
// (output disabled for DateTime type);
  
// LocalDateRetVal flow acquisition
// (input disabled for UI_16 type);
// LocalDateRetVal flow synthesis
// (output disabled for UI_16 type);
  
// LocalDateOut flow acquisition
// (input disabled for DateTime type);
// LocalDateOut flow synthesis
// (output disabled for DateTime type);
  
// WriteDateTime flow acquisition
// (input disabled for DateTime type);
// WriteDateTime flow synthesis
// (output disabled for DateTime type);
  
// WriteDateOut flow acquisition
// (input disabled for DateTime type);
// WriteDateOut flow synthesis
// (output disabled for DateTime type);
  
// StatuSetTime flow acquisition
// (input disabled for DateTime type);
// StatuSetTime flow synthesis
// (output disabled for DateTime type);
  
// PeakMax flow acquisition
// (input disabled for UI_16 type);
// PeakMax flow synthesis
// (output disabled for UI_16 type);
  
// PeakSetpoint flow acquisition
// (input disabled for UI_16 type);
// PeakSetpoint flow synthesis
// (output disabled for UI_16 type);
  
// PeakMin flow acquisition
// (input disabled for UI_16 type);
// PeakMin flow synthesis
// (output disabled for UI_16 type);
  
// PlateauMax flow acquisition
// (input disabled for UI_16 type);
// PlateauMax flow synthesis
// (output disabled for UI_16 type);
  
// PlateauSetpoint flow acquisition
// (input disabled for UI_16 type);
// PlateauSetpoint flow synthesis
// (output disabled for UI_16 type);
  
// PlateauMin flow acquisition
// (input disabled for UI_16 type);
// PlateauMin flow synthesis
// (output disabled for UI_16 type);
  
// PeepMax flow acquisition
// (input disabled for UI_16 type);
// PeepMax flow synthesis
// (output disabled for UI_16 type);
  
// PeepSetpoint flow acquisition
// (input disabled for UI_16 type);
// PeepSetpoint flow synthesis
// (output disabled for UI_16 type);
  
// PeepMin flow acquisition
// (input disabled for UI_16 type);
// PeepMin flow synthesis
// (output disabled for UI_16 type);
  
// FrequencyMax flow acquisition
// (input disabled for UI_16 type);
// FrequencyMax flow synthesis
// (output disabled for UI_16 type);
  
// FrequencySetpoint flow acquisition
// (input disabled for UI_16 type);
// FrequencySetpoint flow synthesis
// (output disabled for UI_16 type);
  
// FrequencyMin flow acquisition
// (input disabled for UI_16 type);
// FrequencyMin flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumenMax flow acquisition
// (input disabled for UI_16 type);
// TidalVolumenMax flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumenSetpoint flow acquisition
// (input disabled for UI_16 type);
// TidalVolumenSetpoint flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumenMin flow acquisition
// (input disabled for UI_16 type);
// TidalVolumenMin flow synthesis
// (output disabled for UI_16 type);
  
// MinimumVolumeMax flow acquisition
// (input disabled for UI_16 type);
// MinimumVolumeMax flow synthesis
// (output disabled for UI_16 type);
  
// MinimumVolumeSetpoint flow acquisition
// (input disabled for UI_16 type);
// MinimumVolumeSetpoint flow synthesis
// (output disabled for UI_16 type);
  
// MinimumVolumeMin flow acquisition
// (input disabled for UI_16 type);
// MinimumVolumeMin flow synthesis
// (output disabled for UI_16 type);
  
// TriggerSetpoint flow acquisition
// (input disabled for UI_16 type);
// TriggerSetpoint flow synthesis
// (output disabled for UI_16 type);
  
// RecruitmentTimer flow acquisition
// (input disabled for TimeUI_16 type);
// RecruitmentTimer flow synthesis
// (output disabled for TimeUI_16 type);
  
// RecruitmentElap flow acquisition
// (input disabled for TimeUI_16 type);
// RecruitmentElap flow synthesis
// (output disabled for TimeUI_16 type);
  
// TimeStart flow acquisition
// (input disabled for DateTime type);
// TimeStart flow synthesis
// (output disabled for DateTime type);
  
// TimeEnd flow acquisition
// (input disabled for DateTime type);
// TimeEnd flow synthesis
// (output disabled for DateTime type);
  
// FlowSetpoint flow acquisition
// (input disabled for UI_16 type);
// FlowSetpoint flow synthesis
// (output disabled for UI_16 type);
  
// PIDOutReal flow acquisition
// (input disabled for FL_16 type);
// PIDOutReal flow synthesis
// (output disabled for FL_16 type);
  
// PIDOutInt flow acquisition
// (input disabled for UI_16 type);
// PIDOutInt flow synthesis
// (output disabled for UI_16 type);
  
// PIDOutPWM flow acquisition
// (input disabled for UI_16 type);
// PIDOutPWM flow synthesis
// (output disabled for UI_16 type);
  
// PIDOutMotor flow acquisition
// (input disabled for UI_16 type);
// PIDOutMotor flow synthesis
// (output disabled for UI_16 type);
  
// PeakSetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type);
// PeakSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// PeakSetpointAlarmHI flow acquisition
// (input disabled for UI_16 type);
// PeakSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// PeakSetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// PeakSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// PeakSetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// PeakSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// PeakSetpointHysteresisHI flow acquisition
// (input disabled for UI_16 type);
// PeakSetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type);
  
// PeakSetpointHysteresisLOW flow acquisition
// (input disabled for UI_16 type);
// PeakSetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type);
  
// PlateauSetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type);
// PlateauSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// PlateauSetpointAlarmHI flow acquisition
// (input disabled for UI_16 type);
// PlateauSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// PlateauSetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// PlateauSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// PlateaukSetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// PlateaukSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// PlateaukSetpointHysteresisHI flow acquisition
// (input disabled for UI_16 type);
// PlateaukSetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type);
  
// PlateaukSetpointHysteresisLOW flow acquisition
// (input disabled for UI_16 type);
// PlateaukSetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type);
  
// PeepSetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type);
// PeepSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// PeepSetpointAlarmHI flow acquisition
// (input disabled for UI_16 type);
// PeepSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// PeepSetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// PeepSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// PeepSetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// PeepSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// PeepSetpointHysteresisHI flow acquisition
// (input disabled for UI_16 type);
// PeepSetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type);
  
// PeepSetpointHysteresisLOW flow acquisition
// (input disabled for UI_16 type);
// PeepSetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type);
  
// FrequencySetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type);
// FrequencySetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// FrequencySetpointAlarmHI flow acquisition
// (input disabled for UI_16 type);
// FrequencySetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// FrequencySetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// FrequencySetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// FrequencySetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// FrequencySetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// FrequencySetpointHysteresisHI flow acquisition
// (input disabled for UI_16 type);
// FrequencySetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type);
  
// FrequencySetpointHysteresisLOW flow acquisition
// (input disabled for UI_16 type);
// FrequencySetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumeSetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type);
// TidalVolumeSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumeSetpointAlarmHI flow acquisition
// (input disabled for UI_16 type);
// TidalVolumeSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumeSetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// TidalVolumeSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumeSetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// TidalVolumeSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumeHystersisHI flow acquisition
// (input disabled for UI_16 type);
// TidalVolumeHystersisHI flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumeHystersisLOW flow acquisition
// (input disabled for UI_16 type);
// TidalVolumeHystersisLOW flow synthesis
// (output disabled for UI_16 type);
  
// MinuteVolumeSetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type);
// MinuteVolumeSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// MinuteVolumeSetpointAlarmHI flow acquisition
// (input disabled for UI_16 type);
// MinuteVolumeSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// MinuteVolumeSetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// MinuteVolumeSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// MinuteVolumeSetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// MinuteVolumeSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// MinuteVolumeHysteresisHI flow acquisition
// (input disabled for UI_16 type);
// MinuteVolumeHysteresisHI flow synthesis
// (output disabled for UI_16 type);
  
// MinuteVolumeHysteresisLOW flow acquisition
// (input disabled for UI_16 type);
// MinuteVolumeHysteresisLOW flow synthesis
// (output disabled for UI_16 type);
  
// TriggeVolumeSetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type);
// TriggeVolumeSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// TriggerVolumeSetpointAlarmHI flow acquisition
// (input disabled for UI_16 type);
// TriggerVolumeSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// TriggerVolumeSetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// TriggerVolumeSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// TriggerVolumeSetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// TriggerVolumeSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// TriggerVolumeHysteresisHI flow acquisition
// (input disabled for UI_16 type);
// TriggerVolumeHysteresisHI flow synthesis
// (output disabled for UI_16 type);
  
// TriggerVolumeHysteresisLOW flow acquisition
// (input disabled for UI_16 type);
// TriggerVolumeHysteresisLOW flow synthesis
// (output disabled for UI_16 type);
  
// BatterySetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type);
// BatterySetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// BatterySetpointAlarmHI flow acquisition
// (input disabled for UI_16 type);
// BatterySetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// BatterySetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// BatterySetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// BatterySetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// BatterySetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// BatterySetpointHysteresisHI flow acquisition
// (input disabled for UI_16 type);
// BatterySetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type);
  
// BatterySetpointHysteresisLOW flow acquisition
// (input disabled for UI_16 type);
// BatterySetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type);
  
// Frecuency flow acquisition
// (input disabled for UI_16 type);
// Frecuency flow synthesis
// (output disabled for UI_16 type);
  
// Duty_Cycle flow acquisition
// (input disabled for UI_16 type);
// Duty_Cycle flow synthesis
// (output disabled for UI_16 type);
  
// HOME_Position flow acquisition
// (input disabled for UI_16 type);
// HOME_Position flow synthesis
// (output disabled for UI_16 type);
  
// ABSOLUTE_Position flow acquisition
// (input disabled for UI_16 type);
// ABSOLUTE_Position flow synthesis
// (output disabled for UI_16 type);
  
// ABSOLUTE_Velocity flow acquisition
// (input disabled for UI_16 type);
// ABSOLUTE_Velocity flow synthesis
// (output disabled for UI_16 type);
  
// RELATIVE_Distance flow acquisition
// (input disabled for UI_16 type);
// RELATIVE_Distance flow synthesis
// (output disabled for UI_16 type);
  
// RELATIVE_Velocity flow acquisition
// (input disabled for UI_16 type);
// RELATIVE_Velocity flow synthesis
// (output disabled for UI_16 type);
  
// Velocity_Velocity flow acquisition
// (input disabled for UI_16 type);
// Velocity_Velocity flow synthesis
// (output disabled for UI_16 type);
  
// JOG_Velocity flow acquisition
// (input disabled for UI_16 type);
// JOG_Velocity flow synthesis
// (output disabled for UI_16 type);
  
// EmergencyStop flow acquisition
BOOL acquire_EmergencyStop(void);
// EmergencyStop flow synthesis
// (output disabled for DI type);
  
// TurnAxisHwHi flow acquisition
BOOL acquire_TurnAxisHwHi(void);
// TurnAxisHwHi flow synthesis
// (output disabled for DI_pu type);
  
// BeaconLightRed flow acquisition
// (input disabled for DO type);
// BeaconLightRed flow synthesis
void synthesize_BeaconLightRed(void);
  
// BeaconLightOrange flow acquisition
// (input disabled for DO type);
// BeaconLightOrange flow synthesis
void synthesize_BeaconLightOrange(void);
  
// TurnAxisPulse flow acquisition
// (input disabled for DO type);
// TurnAxisPulse flow synthesis
void synthesize_TurnAxisPulse(void);
  
// TurnAxisDir flow acquisition
// (input disabled for DO type);
// TurnAxisDir flow synthesis
void synthesize_TurnAxisDir(void);
  
// TurnAxisEnable flow acquisition
// (input disabled for DO type);
// TurnAxisEnable flow synthesis
void synthesize_TurnAxisEnable(void);
  
// BeaconLightGreen flow acquisition
// (input disabled for DO type);
// BeaconLightGreen flow synthesis
void synthesize_BeaconLightGreen(void);
  
// Buzzer flow acquisition
// (input disabled for DO type);
// Buzzer flow synthesis
void synthesize_Buzzer(void);
  
// PeepValve flow acquisition
// (input disabled for DO type);
// PeepValve flow synthesis
void synthesize_PeepValve(void);
  
// TurnAxisHwLow flow acquisition
BOOL acquire_TurnAxisHwLow(void);
// TurnAxisHwLow flow synthesis
// (output disabled for DI_pu type);
  
// ServoPend flow acquisition
BOOL acquire_ServoPend(void);
// ServoPend flow synthesis
// (output disabled for DI type);
  
// ServoAlarm flow acquisition
BOOL acquire_ServoAlarm(void);
// ServoAlarm flow synthesis
// (output disabled for DI_pu type);
  
// AtmosfericPressure flow acquisition
uint16_t acquire_AtmosfericPressure(void);
// AtmosfericPressure flow synthesis
// (output disabled for ADC type);
  
// DiferentialPressure flow acquisition
uint16_t acquire_DiferentialPressure(void);
// DiferentialPressure flow synthesis
// (output disabled for ADC type);
  
// SerTX flow acquisition
// (input disabled for Bus type);
// SerTX flow synthesis
// (output disabled for Bus type);
  
// SerRX flow acquisition
// (input disabled for Bus type);
// SerRX flow synthesis
// (output disabled for Bus type);
  
// EmgcyButton flow acquisition
// (input disabled for TBD type);
// EmgcyButton flow synthesis
// (output disabled for TBD type);
  
// ApiVersion  flow acquisition
// (input disabled for UI_8 type);
// ApiVersion  flow synthesis
// (output disabled for UI_8 type);
  
// MachineUuid flow acquisition
// (input disabled for Variable type);
// MachineUuid flow synthesis
// (output disabled for Variable type);
  
// FirmwareVersion flow acquisition
// (input disabled for UI_16 type);
// FirmwareVersion flow synthesis
// (output disabled for UI_16 type);
  
// Uptime15mCounter flow acquisition
// (input disabled for UI_16 type);
// Uptime15mCounter flow synthesis
// (output disabled for UI_16 type);
  
// MutedAlarmSeconds flow acquisition
// (input disabled for UI_8 type);
// MutedAlarmSeconds flow synthesis
// (output disabled for UI_8 type);
  
// CycleRpmLast30s flow acquisition
// (input disabled for UI_16 type);
// CycleRpmLast30s flow synthesis
// (output disabled for UI_16 type);
  
// CycleIndications flow acquisition
// (input disabled for Variable type);
// CycleIndications flow synthesis
// (output disabled for Variable type);
  
// CycleDuration flow acquisition
// (input disabled for TimeUI_16 type);
// CycleDuration flow synthesis
// (output disabled for TimeUI_16 type);
  
// VentilationFlags flow acquisition
// (input disabled for Variable type);
// VentilationFlags flow synthesis
// (output disabled for Variable type);
  
// VolumeSetting flow acquisition
// (input disabled for UI_16 type);
// VolumeSetting flow synthesis
// (output disabled for UI_16 type);
  
// RpmSetting flow acquisition
// (input disabled for UI_16 type);
// RpmSetting flow synthesis
// (output disabled for UI_16 type);
  
// RampDegreesSetting flow acquisition
// (input disabled for UI_8 type);
// RampDegreesSetting flow synthesis
// (output disabled for UI_8 type);
  
// EiRatioSetting flow acquisition
// (input disabled for UI_8 type);
// EiRatioSetting flow synthesis
// (output disabled for UI_8 type);
  
// RecruitTimer flow acquisition
// (input disabled for UI_8 type);
// RecruitTimer flow synthesis
// (output disabled for UI_8 type);
  
// IncomingFrame flow acquisition
// (input disabled for Variable type);
// IncomingFrame flow synthesis
// (output disabled for Variable type);
  
// OutgoingFrame flow acquisition
// (input disabled for Variable type);
// OutgoingFrame flow synthesis
// (output disabled for Variable type);

#endif /* _DRE_H */