/* ['Common headers' begin (DON'T REMOVE THIS LINE!)] */
#include "mvFSM.h"
/* ['Common headers' end (DON'T REMOVE THIS LINE!)] */

/* ['VentilationCycle' begin (DON'T REMOVE THIS LINE!)] */
void VentilationCycle(  )
{
    /* set initial state */
    static STATE_T state = ID_VENTILATIONCYCLE_INITIAL;

    switch( state )
    {
        /* State ID: ID_VENTILATIONCYCLE_INITIAL */
        case ID_VENTILATIONCYCLE_INITIAL:
        {
            /* Transition ID: ID_VENTILATIONCYCLE_TRANSITION_CONNECTION */
            state = ID_VENTILATIONCYCLE_EXSUFFLATION;
            break;
        }
        /* State ID: ID_VENTILATIONCYCLE_EXSUFFLATION */
        case ID_VENTILATIONCYCLE_EXSUFFLATION:
        {
            /* Transition ID: ID_VENTILATIONCYCLE_TRANSITION_CONNECTION */
            break;
        }
    }
}
/* ['VentilationCycle' end (DON'T REMOVE THIS LINE!)] */

/* ['VentilationSession' begin (DON'T REMOVE THIS LINE!)] */
void VentilationSession(  )
{
    /* set initial state */
    static STATE_T state = ID_VENTILATIONSESSION_INITIAL;

    switch( state )
    {
        /* State ID: ID_VENTILATIONSESSION_INITIAL */
        case ID_VENTILATIONSESSION_INITIAL:
        {
            /* Transition ID: ID_VENTILATIONSESSION_TRANSITION_CONNECTION */
            state = ID_VENTILATIONSESSION_HOMING;
            break;
        }
        /* State ID: ID_VENTILATIONSESSION_HOMING */
        case ID_VENTILATIONSESSION_HOMING:
        {
            /* Transition ID: ID_VENTILATIONSESSION_TRANSITION_CONNECTION */
            state = ID_VENTILATIONSESSION_VENTILATIONCYCLE;
            break;
        }
        /* State ID: ID_VENTILATIONSESSION_VENTILATIONCYCLE */
        case ID_VENTILATIONSESSION_VENTILATIONCYCLE:
        {
            /* call substate function */
            VentilationCycle(  );
            break;
        }
    }
}
/* ['VentilationSession' end (DON'T REMOVE THIS LINE!)] */

/* ['MechVentilation' begin (DON'T REMOVE THIS LINE!)] */
void MechVentilation(  )
{
    /* set initial state */
    static STATE_T state = ID_MECHVENTILATION_INITIAL;

    switch( state )
    {
        /* State ID: ID_MECHVENTILATION_INITIAL */
        case ID_MECHVENTILATION_INITIAL:
        {
            /* Transition ID: ID_MECHVENTILATION_TRANSITION_CONNECTION */
            state = ID_MECHVENTILATION_ERRORPRESENT;
            break;
        }
        /* State ID: ID_MECHVENTILATION_ERRORPRESENT */
        case ID_MECHVENTILATION_ERRORPRESENT:
        {
            if( edre.mvError )
            {
                /* Transition ID: ID_MECHVENTILATION_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::stopMV' begin] */
                edre.vsEnable = false;
                edre.mcEnable = false;
                edre.mdEnable = false;
                /* ['<global>::stopMV' end] */
                state = ID_MECHVENTILATION_ERROR;
            }
            else
            {
                /* Transition ID: ID_MECHVENTILATION_TRANSITION_CONNECTION */
                state = ID_MECHVENTILATION_READY;
            }
            break;
        }
        /* State ID: ID_MECHVENTILATION_ERROR */
        case ID_MECHVENTILATION_ERROR:
        {
            if( (!edre.mvError && edre.mvRehab) )
            {
                /* Transition ID: ID_MECHVENTILATION_TRANSITION_CONNECTION */
                state = ID_MECHVENTILATION_READY;
            }
            break;
        }
        /* State ID: ID_MECHVENTILATION_READY */
        case ID_MECHVENTILATION_READY:
        {
            if( edre.mvError )
            {
                /* Transition ID: ID_MECHVENTILATION_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::stopMV' begin] */
                edre.vsEnable = false;
                edre.mcEnable = false;
                edre.mdEnable = false;
                /* ['<global>::stopMV' end] */
                state = ID_MECHVENTILATION_ERROR;
            }
            else if( edre.mvEnable )
            {
                /* Transition ID: ID_MECHVENTILATION_TRANSITION_CONNECTION */
                state = ID_MECHVENTILATION_VENTILATIONSESSION;
            }
            break;
        }
        /* State ID: ID_MECHVENTILATION_VENTILATIONSESSION */
        case ID_MECHVENTILATION_VENTILATIONSESSION:
        {
            /* call substate function */
            VentilationSession(  );
            if( edre.mvError )
            {
                /* Transition ID: ID_MECHVENTILATION_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::stopMV' begin] */
                edre.vsEnable = false;
                edre.mcEnable = false;
                edre.mdEnable = false;
                /* ['<global>::stopMV' end] */
                state = ID_MECHVENTILATION_ERROR;
            }
            else if( edre.mvEnable )
            {
                /* Transition ID: ID_MECHVENTILATION_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::stopMV' begin] */
                edre.vsEnable = false;
                edre.mcEnable = false;
                edre.mdEnable = false;
                /* ['<global>::stopMV' end] */
                state = ID_MECHVENTILATION_READY;
            }
            break;
        }
    }
}
/* ['MechVentilation' end (DON'T REMOVE THIS LINE!)] */
