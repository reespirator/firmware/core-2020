/* ['Common headers' begin (DON'T REMOVE THIS LINE!)] */
#include "mvFSM_CI.h"
/* ['Common headers' end (DON'T REMOVE THIS LINE!)] */

/* ['Common definitions for 'Code items generator'' begin (DON'T REMOVE THIS LINE!)] */
/* Generic code items' definitions */
BOOL DO_STATUS_LED;
t_dbgledblinktimer dbgLedBlinkTimer;
t_dbg_led DBG_LED;
BOOL ResetRelay;
BOOL LCD_DB7;
BOOL LCD_DB6;
BOOL LCD_DB5;
BOOL LCD_DB4;
BOOL LCD_DB3;
BOOL LCD_DB2;
BOOL LCD_DB1;
BOOL LCD_DB0;
t_int_fs INT_FS;
t_int_fc INT_FC;
t_int_hs INT_HS;
t_int_hc INT_HC;
t_systemdate_w SystemDate_W;
t_systemtime_w SystemTime_W;
t_peakfactor PeakFactor;
t_plateaufactor PlateauFactor;
t_peepfactor PeepFactor;
t_frequencyfactor FrequencyFactor;
t_tidalvolumenfactor TidalVolumenFactor;
t_minimumvolumefactor MinimumVolumeFactor;
t_triggerfactor TriggerFactor;
t_batteryleveltrigger BatteryLevelTrigger;
t_patienttype PatientType;
t_patientweight PatientWeight;
t_patientheight PatientHeight;
t_tidalvolume TidalVolume;
t_pidmode PIDMode;
t_pidstate PIDState;
t_piderrorbits PIDErrorBits;
t_alarms Alarms;
t_status Status;
t_power_startmode POWER_StartMode;
t_power_stopmode POWER_StopMode;
t_home_mode HOME_Mode;
BOOL SetWriteData;
BOOL SetTime;
BOOL Trigger;
BOOL PeakPBPlus;
BOOL PeakPBMinus;
BOOL PeepPBPlus;
BOOL PeepPBMinus;
BOOL FrPBPlus;
BOOL FrPBMinus;
BOOL RecruitmentOn;
BOOL RecruitmentOff;
BOOL RecruitmentMem;
BOOL RecruitmentRunning;
BOOL RecruitmentEnding;
BOOL RecruitmentEnd;
BOOL ErrorAck;
BOOL Reset;
BOOL PIDError;
BOOL PeakDisplayPreAlarmHI;
BOOL PeakDisplayAlarmHI;
BOOL PeakDisplayPreAlarmLOW;
BOOL PeakDisplayAlarmLOW;
BOOL PeakInterlockPreAlarmHI;
BOOL PeakInterlockAlarmHI;
BOOL PeakInterlockPreAlarmLOW;
BOOL PeakInterlockAlarmLOW;
BOOL PlateauDisplayPreAlarmHI;
BOOL PlateauDisplayAlarmHI;
BOOL PlateauDisplayPreAlarmLOW;
BOOL PlateauDisplayAlarmLOW;
BOOL PlateauInterlockPreAlarmHI;
BOOL PlateauInterlockAlarmHI;
BOOL PlateauInterlockPreAlarmLOW;
BOOL PlateauInterlockAlarmLOW;
BOOL PeepDisplayPreAlarmHI;
BOOL PeepDisplayAlarmHI;
BOOL PeepDisplayPreAlarmLOW;
BOOL PeepDisplayAlarmLOW;
BOOL PeepInterlockPreAlarmHI;
BOOL PeepInterlockAlarmHI;
BOOL PeepInterlockPreAlarmLOW;
BOOL PeepInterlockAlarmLOW;
BOOL FrequencyDisplayPreAlarmHI;
BOOL FrequencyDisplayAlarmHI;
BOOL FrequencyDisplayPreAlarmLOW;
BOOL FrequencyDisplayAlarmLOW;
BOOL FrequencyInterlockPreAlarmHI;
BOOL FrequencyInterlockAlarmHI;
BOOL FrequencyInterlockPreAlarmLOW;
BOOL FrequencyInterlockAlarmLOW;
BOOL TidalVolumeDisplayPreAlarmHI;
BOOL TidalVolumeDisplayAlarmHI;
BOOL TidalVolumeDisplayPreAlarmLOW;
BOOL TidalVolumeDisplayAlarmLOW;
BOOL TidalVolumeInterlockPreAlarmHI;
BOOL TidalVolumeInterlockAlarmHI;
BOOL TidalVolumeInterlockPreAlarmLOW;
BOOL TidalVolumeInterlockAlarmLOW;
BOOL MinuteVolumeDisplayPreAlarmHI;
BOOL MinuteVolumeDisplayAlarmHI;
BOOL MinuteVolumeDisplayPreAlarmLOW;
BOOL MinuteVolumeDisplayAlarmLOW;
BOOL MinuteVolumeInterlockPreAlarmHI;
BOOL MinuteVolumeInterlockAlarmHI;
BOOL MinuteVolumeInterlockPreAlarmLOW;
BOOL MinuteVolumeInterlockAlarmLOW;
BOOL TriggerDisplayPreAlarmHI;
BOOL TriggerDisplayAlarmHI;
BOOL TriggerDisplayPreAlarmLOW;
BOOL TriggerDisplayAlarmLOW;
BOOL TriggerInterlockPreAlarmHI;
BOOL TriggerInterlockAlarmHI;
BOOL TriggerInterlockPreAlarmLOW;
BOOL TriggerInterlockAlarmLOW;
BOOL BatteryDisplayPreAlarmHI;
BOOL BatteryDisplayAlarmHI;
BOOL BatteryDisplayPreAlarmLOW;
BOOL BatteryDisplayAlarmLOW;
BOOL BatteryInterlockPreAlarmHI;
BOOL BatteryInterlockAlarmHI;
BOOL BatteryInterlockPreAlarmLOW;
BOOL BatteryInterlockAlarmLOW;
BOOL Enable;
BOOL Busy;
BOOL POWER_Enable;
BOOL POWER_Status;
BOOL POWER_Error;
BOOL RESET_Execute;
BOOL RESET_Done;
BOOL RESET_Error;
BOOL HOME_Execute;
BOOL HOME_Done;
BOOL HOME_Error;
BOOL HALT_Execute;
BOOL HALT_Done;
BOOL HALT_Error;
BOOL ABSOLUTE_Execute;
BOOL ABSOLUTE_Done;
BOOL ABSOLUTE_Error;
BOOL RELATIVE_Execute;
BOOL RELATIVE_Done;
BOOL RELATIVE_Error;
BOOL Velocity_Execute;
BOOL Velocity_Current;
BOOL Velocity_invelocity;
BOOL Velocity_Error;
BOOL JOG_Forward;
BOOL JOG_Backward;
BOOL JOG_Invelocity;
BOOL JOG_Error;
BOOL Permissions;
BOOL Operation_ON;
BOOL Emergency;
BOOL GeneralAck;
BOOL MotorAck;
BOOL ProcessAck;
BOOL GeneralReset;
BOOL MotorReset;
BOOL ProcessReset;
UI_16 PeakValue;
UI_16 PlateauValue;
UI_16 PeepValue;
UI_16 FrequencyValue;
UI_16 TidalVolumeValue;
UI_16 MinumValue;
UI_16 DrivingPreassure;
UI_16 BatteryVoltage;
UI_16 BatteryCapacity;
UI_16 Pressure1;
UI_16 Pressure2;
UI_16 VomumeAccumulated;
UI_16 Flow;
UI_16 SystemDateRetVal;
t_datetime SystemDateOut;
UI_16 LocalDateRetVal;
t_datetime LocalDateOut;
t_datetime WriteDateTime;
t_datetime WriteDateOut;
t_datetime StatuSetTime;
UI_16 PeakMax;
UI_16 PeakSetpoint;
UI_16 PeakMin;
UI_16 PlateauMax;
UI_16 PlateauSetpoint;
UI_16 PlateauMin;
UI_16 PeepMax;
UI_16 PeepSetpoint;
UI_16 PeepMin;
UI_16 FrequencyMax;
UI_16 FrequencySetpoint;
UI_16 FrequencyMin;
UI_16 TidalVolumenMax;
UI_16 TidalVolumenSetpoint;
UI_16 TidalVolumenMin;
UI_16 MinimumVolumeMax;
UI_16 MinimumVolumeSetpoint;
UI_16 MinimumVolumeMin;
UI_16 TriggerSetpoint;
UI_16 RecruitmentTimer;
UI_16 RecruitmentElap;
t_datetime TimeStart;
t_datetime TimeEnd;
UI_16 FlowSetpoint;
FL_16 PIDOutReal;
UI_16 PIDOutInt;
UI_16 PIDOutPWM;
UI_16 PIDOutMotor;
UI_16 PeakSetpointPreAlarmHI;
UI_16 PeakSetpointAlarmHI;
UI_16 PeakSetpointPreAlarmLOW;
UI_16 PeakSetpointAlarmLOW;
UI_16 PeakSetpointHysteresisHI;
UI_16 PeakSetpointHysteresisLOW;
UI_16 PlateauSetpointPreAlarmHI;
UI_16 PlateauSetpointAlarmHI;
UI_16 PlateauSetpointPreAlarmLOW;
UI_16 PlateaukSetpointAlarmLOW;
UI_16 PlateaukSetpointHysteresisHI;
UI_16 PlateaukSetpointHysteresisLOW;
UI_16 PeepSetpointPreAlarmHI;
UI_16 PeepSetpointAlarmHI;
UI_16 PeepSetpointPreAlarmLOW;
UI_16 PeepSetpointAlarmLOW;
UI_16 PeepSetpointHysteresisHI;
UI_16 PeepSetpointHysteresisLOW;
UI_16 FrequencySetpointPreAlarmHI;
UI_16 FrequencySetpointAlarmHI;
UI_16 FrequencySetpointPreAlarmLOW;
UI_16 FrequencySetpointAlarmLOW;
UI_16 FrequencySetpointHysteresisHI;
UI_16 FrequencySetpointHysteresisLOW;
UI_16 TidalVolumeSetpointPreAlarmHI;
UI_16 TidalVolumeSetpointAlarmHI;
UI_16 TidalVolumeSetpointPreAlarmLOW;
UI_16 TidalVolumeSetpointAlarmLOW;
UI_16 TidalVolumeHystersisHI;
UI_16 TidalVolumeHystersisLOW;
UI_16 MinuteVolumeSetpointPreAlarmHI;
UI_16 MinuteVolumeSetpointAlarmHI;
UI_16 MinuteVolumeSetpointPreAlarmLOW;
UI_16 MinuteVolumeSetpointAlarmLOW;
UI_16 MinuteVolumeHysteresisHI;
UI_16 MinuteVolumeHysteresisLOW;
UI_16 TriggeVolumeSetpointPreAlarmHI;
UI_16 TriggerVolumeSetpointAlarmHI;
UI_16 TriggerVolumeSetpointPreAlarmLOW;
UI_16 TriggerVolumeSetpointAlarmLOW;
UI_16 TriggerVolumeHysteresisHI;
UI_16 TriggerVolumeHysteresisLOW;
UI_16 BatterySetpointPreAlarmHI;
UI_16 BatterySetpointAlarmHI;
UI_16 BatterySetpointPreAlarmLOW;
UI_16 BatterySetpointAlarmLOW;
UI_16 BatterySetpointHysteresisHI;
UI_16 BatterySetpointHysteresisLOW;
UI_16 Frecuency;
UI_16 Duty_Cycle;
UI_16 HOME_Position;
UI_16 ABSOLUTE_Position;
UI_16 ABSOLUTE_Velocity;
UI_16 RELATIVE_Distance;
UI_16 RELATIVE_Velocity;
UI_16 Velocity_Velocity;
UI_16 JOG_Velocity;
BOOL EmergencyStop;
BOOL TurnAxisHwHi;
BOOL BeaconLightRed;
BOOL BeaconLightOrange;
BOOL TurnAxisPulse;
BOOL TurnAxisDir;
BOOL TurnAxisEnable;
BOOL BeaconLightGreen;
BOOL Buzzer;
BOOL PeepValve;
BOOL TurnAxisHwLow;
BOOL ServoPend;
BOOL ServoAlarm;
UI_16 AtmosfericPressure;
UI_16 DiferentialPressure;
UI_8 ApiVersion_;
t_machineuuid MachineUuid;
UI_16 FirmwareVersion;
UI_16 Uptime15mCounter;
UI_8 MutedAlarmSeconds;
UI_16 CycleRpmLast30s;
t_cycleindications CycleIndications;
UI_16 CycleDuration;
t_ventilationflags VentilationFlags;
UI_16 VolumeSetting;
UI_16 RpmSetting;
UI_8 RampDegreesSetting;
UI_8 EiRatioSetting;
UI_8 RecruitTimer;
t_incomingframe IncomingFrame;
t_outgoingframe OutgoingFrame;
/* ['Common definitions for 'Code items generator'' end (DON'T REMOVE THIS LINE!)] */

/* ['Common definitions for 'Hierarchical State Chart generator'' begin (DON'T REMOVE THIS LINE!)] */
/* Code items' definitions */
/* ['Common definitions for 'Hierarchical State Chart generator'' end (DON'T REMOVE THIS LINE!)] */
