/* ['Common headers' begin (DON'T REMOVE THIS LINE!)] */
#include "mvFSM_CI.h"
/* ['Common headers' end (DON'T REMOVE THIS LINE!)] */

/* ['VentilationCycle' begin (DON'T REMOVE THIS LINE!)] */
/* State IDs */
#define ID_VENTILATIONCYCLE_INITIAL 0
#define ID_VENTILATIONCYCLE_EXSUFFLATION 1

void VentilationCycle(  );
/* ['VentilationCycle' end (DON'T REMOVE THIS LINE!)] */

/* ['VentilationSession' begin (DON'T REMOVE THIS LINE!)] */
/* State IDs */
#define ID_VENTILATIONSESSION_INITIAL 2
#define ID_VENTILATIONSESSION_HOMING 3
#define ID_VENTILATIONSESSION_VENTILATIONCYCLE 4

void VentilationSession(  );
/* ['VentilationSession' end (DON'T REMOVE THIS LINE!)] */

/* ['MechVentilation' begin (DON'T REMOVE THIS LINE!)] */
/* State IDs */
#define ID_MECHVENTILATION_INITIAL 5
#define ID_MECHVENTILATION_READY 6
#define ID_MECHVENTILATION_ERROR 7
#define ID_MECHVENTILATION_VENTILATIONSESSION 8
#define ID_MECHVENTILATION_ERRORPRESENT 9

void MechVentilation(  );
/* ['MechVentilation' end (DON'T REMOVE THIS LINE!)] */
