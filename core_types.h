#ifndef _REES_CORE_TYPES_H
#define _REES_CORE_TYPES_H

#include <stdint.h>

typedef bool t_tbd;

typedef bool BOOL;
typedef uint16_t t_dbgledblinktimer;
typedef bool t_dbg_led;
typedef uint16_t UI_16;
typedef float FL_16;

typedef t_tbd t_int_fs;
typedef t_tbd t_int_fc;
typedef t_tbd t_int_hs;
typedef t_tbd t_int_hc;
typedef t_tbd t_systemdate_w;
typedef t_tbd t_systemtime_w;
typedef t_tbd t_peakfactor;
typedef t_tbd t_plateaufactor;
typedef t_tbd t_peepfactor;
typedef t_tbd t_frequencyfactor;
typedef t_tbd t_tidalvolumenfactor;
typedef t_tbd t_minimumvolumefactor;
typedef t_tbd t_triggerfactor;
typedef t_tbd t_batteryleveltrigger;
typedef t_tbd t_patienttype;
typedef t_tbd t_patientweight;
typedef t_tbd t_patientheight;
typedef t_tbd t_tidalvolume;
typedef t_tbd t_pidmode;
typedef t_tbd t_pidstate;
typedef t_tbd t_piderrorbits;
typedef t_tbd t_alarms;
typedef t_tbd t_status;
typedef t_tbd t_power_startmode;
typedef t_tbd t_power_stopmode;
typedef t_tbd t_home_mode;
typedef t_tbd t_datetime;


#define TRUE (true)

typedef uint8_t UI_8;
typedef t_tbd t_machineuuid;
typedef t_tbd t_cycleindications;
typedef t_tbd t_ventilationflags;
typedef t_tbd t_incomingframe;
typedef t_tbd t_outgoingframe;


#endif
