/* ---- This file describes all the pin assignments of the microcontroller --- */

#ifndef _CORE_PINOUT_H
#define _CORE_PINOUT_H

#include <Arduino.h>

#ifdef ARDUINO_ESP8266_WEMOS_D1MINI
//#error "TODO: Definir pines para el ESP8266_WEMOS_D1MINI"
#define PORT_DO_STATUS_LED LED_BUILTIN

#elif ARDUINO_ESP8266_ESP01
#error "TODO: Definir pines para el ARDUINO_ESP8266_ESP01"
#define PORT_DO_STATUS_LED LED_BUILTIN

#elif ARDUINO_ESP8266_NODEMCU
#error "TODO: Definir pines para el ARDUINO_ESP8266_NODEMCU"
#define PORT_DO_STATUS_LED LED_BUILTIN

#elif ARDUINO_ESP32_DEV
#error "TODO: Definir pines para la ARDUINO_AVR_NANO"
#define PORT_DO_STATUS_LED LED_BUILTIN

#elif ARDUINO_TEENSY31
#error "TODO: Definir pines para la TEENSY31"
#define PORT_DO_STATUS_LED LED_BUILTIN

#elif ARDUINO_AVR_NANO
#error "TODO: Definir pines para la ARDUINO_AVR_NANO"
#define PORT_DO_STATUS_LED LED_BUILTIN

#elif ARDUINO_AVR_MEGA2560
//#error "TODO: Definir pines para la ARDUINO_AVR_MEGA2560"
#define PORT_DO_STATUS_LED LED_BUILTIN
#define PORT_TurnAxisHwHi 2
#define PORT_ServoAlarm 3
#define PORT_BeaconLightRed 4
#define PORT_BeaconLightOrange 5
#define PORT_TurnAxisPulse 6
#define PORT_TurnAxisDir 7
#define PORT_TurnAxisEnable 8
#define PORT_BeaconLightGreen 9
#define PORT_EmergencyStop 10
#define PORT_Buzzer 11
#define PORT_TurnAxisHwLow 13
#define PORT_ServoPend 14
#define PORT_ResetRelay 25
#define PORT_PeepValve 39
#define PORT_AtmosfericPressure A0
#define PORT_DiferentialPressure A1
#define PORT_LCD_DB7 30
#define PORT_LCD_DB6 31
#define PORT_LCD_DB5 32
#define PORT_LCD_DB4 33
#define PORT_LCD_DB3 34
#define PORT_LCD_DB2 35
#define PORT_LCD_DB1 36
#define PORT_LCD_DB0 37
#elif ARDUINO_AVR_UNO
//#error "TODO: Definir pines para la ARDUINO_AVR_MEGA2560"
#define PORT_DO_STATUS_LED LED_BUILTIN
#define PORT_ResetRelay 0
#define PORT_ServoPend 1
#define PORT_TurnAxisHwHi 2
#define PORT_ServoAlarm 3
#define PORT_BeaconLightRed 4
#define PORT_BeaconLightOrange 5
#define PORT_TurnAxisPulse 6
#define PORT_TurnAxisDir 7
#define PORT_TurnAxisEnable 8
#define PORT_BeaconLightGreen 9
#define PORT_EmergencyStop 10
#define PORT_Buzzer 11
#define PORT_TurnAxisHwLow 12
#define PORT_PeepValve 14
#define PORT_AtmosfericPressure A0
#define PORT_DiferentialPressure A1
#define PORT_LCD_DB7 -1
#define PORT_LCD_DB6 -1
#define PORT_LCD_DB5 -1
#define PORT_LCD_DB4 -1
#define PORT_LCD_DB3 -1
#define PORT_LCD_DB2 -1
#define PORT_LCD_DB1 -1
#define PORT_LCD_DB0 -1
#else
#error "No microcontroller defined"
#endif

#endif // _PRJ_PINOUT_H