/* ['Common headers' begin (DON'T REMOVE THIS LINE!)] */
#include <stdio.h>
#include <stdlib.h>
/* ['Common headers' end (DON'T REMOVE THIS LINE!)] */
#include "../DRE.h"

extern t_dre dre;
extern t_diag diag;
/* ['Common declarations for 'Code items generator'' begin (DON'T REMOVE THIS LINE!)] */
/* Generic code items' declarations */
extern BOOL DO_STATUS_LED;
extern t_dbgledblinktimer dbgLedBlinkTimer;
extern t_dbg_led DBG_LED;
/* ['Common declarations for 'Code items generator'' end (DON'T REMOVE THIS LINE!)] */

/* ['Common declarations for 'Hierarchical State Chart generator'' begin (DON'T REMOVE THIS LINE!)] */
/* State ID data type */
typedef unsigned long STATE_T;

/* Declaration of code items used in state charts */
/* ['Common declarations for 'Hierarchical State Chart generator'' end (DON'T REMOVE THIS LINE!)] */
