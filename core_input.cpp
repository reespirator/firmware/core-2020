#ifndef _CORE_INPUT_H
#define _CORE_INPUT_H

#include "DRE.h"

void core_input_init(){
    // Setup of the inputs
    setup_EmergencyStop();
    setup_ServoAlarm();
    setup_TurnAxisHwHi();
    setup_TurnAxisHwLow();
    setup_ServoPend();
    setup_AtmosfericPressure();
    setup_DiferentialPressure();
}
void core_input(){
    // Executes the inputs
    acquire_EmergencyStop();
    acquire_ServoAlarm();
    acquire_TurnAxisHwHi();
    acquire_TurnAxisHwLow();
    acquire_ServoPend();
    acquire_AtmosfericPressure();
    acquire_DiferentialPressure();
}

#endif