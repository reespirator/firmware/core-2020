/* This header file configures the main parameters of the application.
If variant exists, the specific parameters shall be defined
in core_variants.h file, not here.  The active variant is also selected
in core_variants.h file.
*/
#ifndef _CORE_CFG_H
#define _CORE_CFG_H

/* Include variant specific configuration and active variant selection */
#include "core_variants.h"

////////  Configuration of functionalities ///////////

#define CTE_CYCLE_TIME_IN_MS 5
#define CFG_DBGLED_BLINK_TIMER_DUTY (1000/CTE_CYCLE_TIME_IN_MS)
#define CFG_DBGLED_BLINK_TIMER_PERIOD (2000/CTE_CYCLE_TIME_IN_MS)
/** 
 * Special features or layers that can be disabled
 * (we include here examples, but the draft do not include 
 * activable features by now)
 * */

#define CFG_USE_MQTT
#define CFG_USE_WIFI
#define CFG_USE_LCD

/* The DEBUG_LED_BLINK uses the _DIAG_ACTIVE mechanisms */
#ifdef DEBUG_LED_BLINK
#define _DIAG_ACTIVE
#endif

#endif /* _CORE_CFG_H */
