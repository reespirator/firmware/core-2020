#include "core_cfg.h"
#include "reesFSM/reesFSM.h"
#include "core_input.h"
#include "core_output.h"
#include "DRE_init.h"
#include "mvFSM/mvFSM.h"

void main_FSMs(){
    StatusLED();
#ifdef DEBUG_LED_BLINK
    /* If we need the LED blinking, we will execute the blinking FSM
       and them we will use the diagnostics bypass to have inject
       the DBG_LED as the diagnostics value for DO_STATUS_LED
       so the synthesize value will use the injected value */
    DbgLEDBlink();
    diag.DO_STATUS_LED = dre.DBG_LED;
    diag.enable_DO_STATUS_LED = true;
#endif
    MechVentilation();
}

void rees_core_init(){
    DRE_init();
    Diag_init();
    core_input_init();
    core_output_init();
}

void rees_core(){
    core_output();
    core_input();
    main_FSMs();
}