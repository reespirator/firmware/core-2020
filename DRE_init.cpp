#ifndef _DRE_INIT_H
#define _DRE_INIT_H

#include "DRE.h"
#include "extra_DRE.h"

extern t_dre dre;
extern t_diag diag;
t_extra_DRE edre;


void DRE_init(){
    dre.DO_STATUS_LED = false;
    dre.dbgLedBlinkTimer = 0;
    dre.DBG_LED = false;
    dre.ResetRelay = false;
    dre.EmergencyStop = false;
    dre.TurnAxisHwHi = false;
    dre.BeaconLightRed = false;
    dre.BeaconLightOrange = false;
    dre.TurnAxisPulse = false;
    dre.TurnAxisDir = false;
    dre.TurnAxisEnable = false;
    dre.BeaconLightGreen = false;
    dre.Buzzer = false;
    dre.PeepValve = false;
    dre.TurnAxisHwLow = false;
    dre.ServoPend = false;
    dre.ServoAlarm = false;
    dre.AtmosfericPressure = 0;
    dre.DiferentialPressure = 0;
#ifdef CFG_USE_LCD
    dre.LCD_DB7 = false;
    dre.LCD_DB6 = false;
    dre.LCD_DB5 = false;
    dre.LCD_DB4 = false;
    dre.LCD_DB3 = false;
    dre.LCD_DB2 = false;
    dre.LCD_DB1 = false;
    dre.LCD_DB0 = false;
#endif
    edre.mvEnable = true;
    edre.vsEnable = true;    
    edre.mcEnable = true;
    edre.mdEnable = true;
    edre.mvError = false;
    edre.mvRehab = false;
}

void Diag_init(){
    diag.enable_DO_STATUS_LED = false;
    diag.enable_dbgLedBlinkTimer = false;
    diag.enable_DBG_LED = false;
    diag.enable_ResetRelay = false;
    diag.enable_EmergencyStop = false;
    diag.enable_TurnAxisHwHi = false;
    diag.enable_BeaconLightRed = false;
    diag.enable_BeaconLightOrange = false;
    diag.enable_TurnAxisPulse = false;
    diag.enable_TurnAxisDir = false;
    diag.enable_TurnAxisEnable = false;
    diag.enable_BeaconLightGreen = false;
    diag.enable_Buzzer = false;
    diag.enable_PeepValve = false;
    diag.enable_TurnAxisHwLow = false;
    diag.enable_ServoPend = false;
    diag.enable_ServoAlarm = false;
    diag.enable_AtmosfericPressure = false;
    diag.enable_DiferentialPressure = false;
#ifdef CFG_USE_LCD
    diag.enable_LCD_DB7 = false;
    diag.enable_LCD_DB6 = false;
    diag.enable_LCD_DB5 = false;
    diag.enable_LCD_DB4 = false;
    diag.enable_LCD_DB3 = false;
    diag.enable_LCD_DB2 = false;
    diag.enable_LCD_DB1 = false;
    diag.enable_LCD_DB0 = false;
#endif
}

#endif